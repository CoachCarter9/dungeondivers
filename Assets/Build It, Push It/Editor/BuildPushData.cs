﻿using System;
using UnityEngine;

namespace KatalystKreations.BuildPush {

    [Flags]
    public enum TargetPlatform {
        Windows = 1,
        Windows64 = 2,
        OSX = 4,
        OSX64 = 8,
        OSXUniversal = 16,
        Linux = 32,
        Linux64 = 64,
        LinuxUniversal = 128,
        Android = 256,
        WebGL = 512,
    }

    //[CreateAssetMenu(fileName = "Data", menuName = "Build it, Push it/Create Data", order = 1)]
    [System.Serializable]
    public class BuildPushData : ScriptableObject {
        public string buildPath;
        public Version versionNumber;
        public bool autoIncrementVersion;
        public bool customVersion;
        public string customVersionString;
        public string userName;
        public string gameName;
        public string channelName;
        public string butlerFilePath;
        public bool verboseMode;

        public bool hotfixToggle;
        public bool patchToggle;
        public bool contentToggle;
        public bool expansionToggle;

        public bool windows;
        public bool windows64;
        public bool OSX;
        public bool OSX64;
        public bool OSXUniversal;
        public bool linux;
        public bool linux64;
        public bool linuxUniversal;
        public bool android;
        public bool webGL;
    }

    [System.Serializable]
    public struct Version {
        public int expansion;
        public int content;
        public int patch;
        public int hotfix;

        public string ReturnVersion() {
            return expansion + "." + content + "." + patch + "." + hotfix;
        }
    }
}