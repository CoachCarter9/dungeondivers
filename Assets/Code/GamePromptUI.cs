﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePromptUI : MonoBehaviour
{
    [SerializeField]
    private Text textDisplay;

    // Use this for initialization
    private void Start()
    {
        GamePromptManager.messagePrompt = this;
    }

    public void DisplayMessage(string message)
    {
        textDisplay.text = message;
    }
}