﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;

namespace DungeonDivers
{
    public class MPGameController : GameController
    {
        private OtherPlayersTurnState otherPlayerTurnState;

        private PhotonView photonView;

        private int turnCounter = 0;

        private void Start()
        {
            photonView = GetComponent<PhotonView>();
            //CreateNewPlayer(Player.PlayerColor.Red, PhotonNetwork.player);
            Debug.Log("Connected to Photon: " + PhotonNetwork.connected);
            if (PhotonNetwork.connected)
            {
                CreateNewPlayer(Player.PlayerColor.Red, PhotonNetwork.player);
                StartGame();
            }
            if (PhotonNetwork.isMasterClient == false)
            {
                ScriptableObject.CreateInstance<DDFloatText>();
                //CreateNewPlayer(Player.PlayerColor.Red, PhotonNetwork.player);
            }
            //Report your stats to everyone else
        }

        protected override void InitPlayers()
        {
            base.InitPlayers();

            for (int i = 0; i < PlayerManager.Instance.Players.Count; i++)
            {
                Debug.Log(string.Format("{0} - Player Name: {1}", i, PlayerManager.Instance.Players[i].Name));
            }
        }

        private void CreateNewPlayer(Player.PlayerColor playerColor, PhotonPlayer pPlayer)
        {
            Debug.Log("Creating photon player");
            string prefabToLoad = "DungeonDiverMP";
            Player newPlayer = new Player(pPlayer.NickName, playerColor, PlayerManager.Instance.AutoGenerateStats());
            PlayerManager.Instance.AddPlayer(newPlayer);
            GameObject photonGO = PhotonNetwork.Instantiate(prefabToLoad, new Vector3(999, 999, 999), Quaternion.identity, 0);
            Debug.Log("Creating DD: " + photonGO);
            newPlayer.CharacterModel = photonGO;
            photonGO.GetComponent<MPPlayerStatSync>().UpdatePlayerWithStatSync(newPlayer, pPlayer);
            newPlayer.CharacterModel.transform.position = StartingPos;
            activePlayer = newPlayer;
            //            photonGO.GetComponent<PhotonView>().RPC(
            //"CreatePlayerAndUpdate", PhotonTargets.OthersBuffered, new object[] { pPlayer.NickName, (int)playerColor });
        }

        protected override void InitGameStates()
        {
            base.InitGameStates();
            otherPlayerTurnState = new OtherPlayersTurnState();
        }

        public override void FinishTurn()
        {
            base.FinishTurn();
            SendPlayerFinishedTurn();
        }

        public override void StartGame()
        {
            base.StartGame();
            if (PhotonNetwork.isMasterClient)
            {
                SendCurrentPlayerTurn();
            }
        }

        private void SendPlayerFinishedTurn()
        {
            PhotonNetwork.room.SetTurn(currentPlayerTurn++);
            photonView.RPC("PlayerFinishedTurn", PhotonTargets.All, PhotonNetwork.playerName);
        }

        [PunRPC]
        private void PlayerFinishedTurn(string playerName)
        {
            if (PhotonNetwork.isMasterClient)
            {
                turnCounter++;
                PhotonNetwork.room.SetTurn(turnCounter);
                SendCurrentPlayerTurn();
            }
        }

        private void SendCurrentPlayerTurn()
        {
            photonView.RPC("SetCurrentPlayerTurn", PhotonTargets.All, PhotonNetwork.playerList[turnCounter % PhotonNetwork.playerList.Length].NickName);
        }

        [PunRPC]
        private void SetCurrentPlayerTurn(string playerName)
        {
            Debug.Log("Setting turn to: " + playerName);
            if (playerName != PhotonNetwork.playerName)
                SwitchToGameState(otherPlayerTurnState);
            else
            {
                SwitchToGameState(navigatingDungeonState);
            }
        }

        public void SendTrapDisarmMessage(int tileID)
        {
            photonView.RPC("DisarmTrap", PhotonTargets.OthersBuffered, tileID);
        }

        [PunRPC]
        private void DisarmTrap(int id)
        {
            DungeonTileManager.GetTileByID(id).REvent.SetTrapToDisarmed();
        }
    }
}