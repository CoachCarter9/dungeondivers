﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class RoomTileDiceColliders : MonoBehaviour
    {
        [SerializeField]
        private Transform colliderContainer;

        private BoxCollider[] colliders;

        private void Awake()
        {
            GetComponent<RoomTile>().onRoomEventSet += AddMethodsToRoomEvent;
        }

        private void Start()
        {
            colliders = colliderContainer.GetComponentsInChildren<BoxCollider>();
            colliderContainer.gameObject.SetActive(false);
        }

        private void AddMethodsToRoomEvent(RoomEvent rEvent)
        {
            Debug.Log("Add Methods");
            rEvent.onRollDice += DisableColliderNearestCamera;
            rEvent.onRoomEventComplete += Reset;
        }

        private void Reset(bool passed)
        {
            colliderContainer.gameObject.SetActive(false);
            for (int i = 0; i < colliders.Length; i++)
            {
                colliders[i].enabled = true;
            }
        }

        private void DisableColliderNearestCamera()
        {
            //Debug.Log("enabled dice colliders");
            colliderContainer.gameObject.SetActive(true);
            Vector3 cameraPos = Camera.main.transform.position;
            BoxCollider nearestCollider = colliders[0];
            float nearestDistance = Vector3.Distance(Camera.main.transform.position, nearestCollider.transform.position);
            for (int i = 0; i < colliders.Length; i++)
            {
                float colliderDistance = Vector3.Distance(cameraPos, colliders[i].transform.position);
                if (colliderDistance < nearestDistance)
                {
                    nearestCollider = colliders[i];
                    nearestDistance = colliderDistance;
                }
            }

            nearestCollider.enabled = false;
        }
    }
}