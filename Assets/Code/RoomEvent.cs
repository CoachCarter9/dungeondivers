﻿using System;
using System.Collections;
using System.Collections.Generic;
using UISystem;
using UnityEngine;

namespace DungeonDivers
{
    public abstract class RoomEvent
    {
        public RoomEvent()
        {
            eventName = this.GetType().Name;
        }

        private string eventName;

        public string EventName
        {
            get { return eventName; }
        }

        protected bool isArmed = true;

        public bool IsArmed
        {
            get { return isArmed; }
        }

        public void DisarmTrap()
        {
            isArmed = false;
            SendDisarmMessage();
        }

        private void SendDisarmMessage()
        {
            if (PhotonNetwork.connected)
            {
                ((MPGameController)GameController.Instance).SendTrapDisarmMessage(tile.ID);
            }
        }

        public void SetTrapToDisarmed()
        {
            isArmed = false;
        }

        protected bool isBuffRoom = false;

        public bool IsBuffRoom
        {
            get { return isBuffRoom; }
        }

        public enum RoomTrapType
        {
            Ceiling,
            Floor,
            Wall,
            None
        }

        public abstract RoomTrapType TrapType
        { get; }

        public Color StatusColor
        {
            get
            {
                if (IsArmed == true)
                {
                    if (IsBuffRoom)
                    {
                        return Color.green;
                    }
                    else return Color.red;
                }
                else return Color.white;
            }
        }

        public abstract Player.PlayerStat.StatType StatRollType
        { get; }

        protected Player activatingPlayer;

        private List<Player> passedPlayers = new List<Player>();

        protected bool isOneTime = false;
        public RoomTile tile;

        public delegate void OnTrapComplete(bool passed);
        public OnTrapComplete onRoomEventComplete;

        public delegate void OnRollDice();
        public OnRollDice onRollDice;

        private bool passedEvent = true;

        private enum TrapResult
        {
            CriticalFail,
            Fail,
            Pass,
            CriticalPass
        }

        public virtual void ActivateTrap(Player player)
        {
            passedEvent = true;
            activatingPlayer = player;
            if (isArmed == false)
            {
                if (onRoomEventComplete != null)
                    onRoomEventComplete(passedEvent);
                Debug.Log(eventName + " not armed.");
                return;
            }
            GameEventManager.Instance.AddGameEvent(new TriggeredRoomEvent(player, this));
            DDDice.Instance.RollStatDice(tile.transform.position + new Vector3(0, 7, -2), StatRollType, activatingPlayer.GetStatValueOfType(StatRollType));
            if (onRollDice != null)
                onRollDice();
            tile.StartCoroutine(WaitForDiceAndResolve());
        }

        private IEnumerator WaitForDiceAndResolve()
        {
            yield return new WaitForSeconds(2.5f);
            int[] diceValues = Dice.GetDiceValues();
            int dieRoll, bonusRoll;
            GetRollValue(out dieRoll, out bonusRoll);
            GameEventManager.Instance.AddGameEvent(new DiceResolutionEvent(activatingPlayer, dieRoll, bonusRoll));
            HandleTriggerTrapRoll(dieRoll);
            if (isOneTime)
                isArmed = false;
            SendTrapResult(diceValues, activatingPlayer);
            yield return new WaitForSeconds(2);
            if (onRoomEventComplete != null)
                onRoomEventComplete(passedEvent);
        }

        private void GetRollValue(out int dieRoll, out int bonusRoll)
        {
            dieRoll = Dice.GetD3ValueOfD6();
            bonusRoll = 0;
            if (passedPlayers.Contains(activatingPlayer))
            {
                Debug.Log("Player has passed trap. Adding one to die roll");
                bonusRoll += 1;
            }
            dieRoll += bonusRoll;
        }

        private void SendTrapResult(int[] diceValues, Player activatingPlayer)
        {
            if (PhotonNetwork.connected)
            {
                tile.GetComponent<PhotonView>().RPC("DisplayPlayerResults",
    PhotonTargets.Others, new object[] { diceValues, activatingPlayer.CharacterModel.GetComponent<PhotonView>().viewID, isArmed });
            }
        }

        protected virtual void HandleTriggerTrapRoll(int dieRoll)
        {
            TrapResult result = TrapResult.CriticalFail;
            switch (dieRoll)
            {
                case 12:
                case 11:
                case 10:
                case 9:
                case 8:
                case 7:
                case 6:
                    result = TrapResult.CriticalPass;
                    break;

                case 5:
                case 4:
                case 3:
                    result = TrapResult.Pass;
                    break;

                case 2:
                case 1:
                    result = TrapResult.Fail;
                    break;

                case 0:
                    result = TrapResult.CriticalFail;
                    break;

                default:
                    Debug.Log("Trap doesn't have case for: " + dieRoll);
                    break;
            }

            if (FailedTrap(result))
            {
                if (UseDiceItem())
                    result += 1;
            }

            switch (result)
            {
                case TrapResult.CriticalPass:
                    isArmed = false;
                    GameEventManager.Instance.AddGameEvent(new RoomResultsEvent(activatingPlayer, "Critically Passed"));
                    CriticalPass();
                    activatingPlayer.IncreaseScore(DDConsts.DisarmTrapScore);
                    passedPlayers.Add(activatingPlayer);
                    passedEvent = true;
                    break;

                case TrapResult.Pass:
                    GameEventManager.Instance.AddGameEvent(new RoomResultsEvent(activatingPlayer, "Passed"));
                    Pass();
                    passedEvent = true;
                    passedPlayers.Add(activatingPlayer);
                    break;

                case TrapResult.Fail:
                    GameEventManager.Instance.AddGameEvent(new RoomResultsEvent(activatingPlayer, "Failed"));
                    Fail();
                    passedEvent = false;
                    break;

                case TrapResult.CriticalFail:
                    GameEventManager.Instance.AddGameEvent(new RoomResultsEvent(activatingPlayer, "Critically Failed"));
                    passedEvent = false;
                    CriticalFail();
                    break;
            }
        }

        private bool FailedTrap(TrapResult result)
        {
            return result == TrapResult.Fail || result == TrapResult.CriticalFail;
        }

        private bool UseDiceItem()
        {
            for (int i = 0; i < activatingPlayer.PlayerInventory.ItemCount; i++)
            {
                if (activatingPlayer.PlayerInventory.GetItemAtIndex(i).GetType() == typeof(WearableItem))
                {
                    WearableItem wItem = (WearableItem)activatingPlayer.PlayerInventory.GetItemAtIndex(i);
                    if (wItem.FailType == TrapType)
                    {
                        activatingPlayer.PlayerInventory.UseItem(i);
                        return true;
                    }
                }
            }
            return false;
        }

        protected virtual void CriticalPass()
        {
        }

        protected virtual void Pass()
        {
        }

        protected virtual void Fail()
        {
        }

        protected virtual void CriticalFail()
        {
        }
    }

    public static class RoomEventFactory
    {
        static RoomEventFactory()
        {
            CreateTrapList();
        }

        private static Dictionary<string, RoomEvent> trapEvents = new Dictionary<string, RoomEvent>();

        private static void CreateTrapList()
        {
            trapEvents.Add("Pendulum", new Pendulum());
            trapEvents.Add("CeilingPiston", new CeilingPiston());
            trapEvents.Add("ArrowLauncher", new ArrowLauncher());
            trapEvents.Add("SheerRock", new SheerRock());
            trapEvents.Add("RollingBoulder", new RollingBoulder());
            trapEvents.Add("BladeHall", new BladeHall());
            trapEvents.Add("RushingRiver", new RushingRiver());
            trapEvents.Add("RampStairs", new RampStairs());
            trapEvents.Add("ConcealedPit", new ConcealedPit());
            trapEvents.Add("ThePit", new ThePit());
            trapEvents.Add("SpearCeiling", new SpearCeiling());
            trapEvents.Add("FloodedRoom", new FloodedRoom());
            trapEvents.Add("RatsInTheWall", new RatsInTheWall());
            trapEvents.Add("AxDrop", new AxDrop());
            trapEvents.Add("TrapChest", new TrapChest());
            trapEvents.Add("ScaldingFloor", new ScaldingFloor());
            trapEvents.Add("TripRope", new TripRope());
            trapEvents.Add("VentFloor", new VentFloor());
            trapEvents.Add("ArtifactRoom", new ArtifactRoom());
            trapEvents.Add("StorageRoom", new StorageRoom());
            trapEvents.Add("HiddenStash", new HiddenStash());
            trapEvents.Add("SecretLever", new SecretLever());
            trapEvents.Add("Library", new Library());
            trapEvents.Add("HealthFountain", new HealthFountain());
            trapEvents.Add("Armory", new Armory());
            trapEvents.Add("AlchemistsChamber", new AlchemistsChamber());
            trapEvents.Add("TrainingRoom", new TrainingRoom());
            trapEvents.Add("MessHall", new MessHall());
            trapEvents.Add("ExitDungeonEvent", new ExitDungeonEvent());
            trapEvents.Add("NoEvent", new NoEvent());
        }

        public static RoomEvent NewRoomEventByName(string eventName)
        {
            Debug.Log("Creating instance of: " + eventName);
            if (trapEvents.ContainsKey(eventName) == false)
                throw new Exception(eventName + " doesn't exist in the trap list. Please add it");
            return (RoomEvent)Activator.CreateInstance(trapEvents[eventName].GetType());
        }

        public static RoomEvent GetNewRandomTrapEvent()
        {
            List<string> keyList = new List<string>(trapEvents.Keys);
            int randTrap = UnityEngine.Random.Range(0, keyList.Count);
            return NewRoomEventByName(keyList[randTrap]);
        }
    }

    public class FallingNet : RoomEvent
    {
        public FallingNet()
        {
            isOneTime = true;
        }

        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        protected override void CriticalFail()
        {
            //TODO Miss two turns
        }

        protected override void Fail()
        {
            //TODO Miss 1 turns
        }
    }

    public class TarDrop : RoomEvent
    {
        public TarDrop()
        {
            isOneTime = true;
        }

        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        protected override void CriticalFail()
        {
            //TODO Roll to move next round and 1/2 move for three rounds
        }

        protected override void Fail()
        {
            //TODO 1/2 Move for two rounds
        }
    }

    public class Pendulum : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.STR);
        }
    }

    public class CaveIn : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        public CaveIn()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(4);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(6);
            //TODO Path is blocked
        }
    }

    public class ChandelierDrop : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        public ChandelierDrop()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            //TODO Miss Next Turn
        }
    }

    public class CeilingPiston : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(6);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.CON);
        }
    }

    public class ArrowLauncher : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            //TODO Lose D20 Gold
        }
    }

    public class SheerRock : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.INT);
        }
    }

    public class RollingBoulder : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            //TODO Lose D20 Gold
        }
    }

    public class WallPress : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(4);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(8);
            Item lostItem = activatingPlayer.PlayerInventory.GetItemAtIndex(0);
            if (lostItem != null)
                activatingPlayer.PlayerInventory.RemoveItem(lostItem);
        }
    }

    public class BladeHall : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.CON);
        }
    }

    public class CrackedWall : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        public CrackedWall()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            //TODO Miss next turn
        }
    }

    public class FloorSpikes : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(3);
            //TODO 1/2 Movement for 3 rounds
        }
    }

    public class RushingRiver : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(1);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(2);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.DEX);
        }
    }

    public class RampStairs : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(3);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.INT);
        }
    }

    public class VentFloor : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            tile.StartCoroutine(RollForGold());
        }

        private int goldMultipler = 15;

        private IEnumerator RollForGold()
        {
            DDDice.Instance.RollD6(tile.transform, 1);
            yield return new WaitForSeconds(DDConsts.DiceRollWait);
            int rollValue = Dice.GetD3ValueOfD6();
            activatingPlayer.Gold += rollValue * goldMultipler;
        }
    }

    public class ConcealedPit : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.DEX;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        public ConcealedPit()
        {
            isOneTime = false;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            activatingPlayer.Gold -= 50;
        }
    }

    public class ThePit : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.STR;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(activatingPlayer.Health);
        }
    }

    public class BlindingRoom : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        public BlindingRoom()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(1);
            //TODO -1 Dex for next round
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(3);
            //TODO  -2Dex for next round
        }
    }

    public class NeedleRain : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        public NeedleRain()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            //TODO -1H bleed for 3 Rounds
        }
    }

    public class Snare : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        public Snare()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            //End Turn
        }

        protected override void CriticalFail()
        {
            //TODO Roll to escape for three rounds
        }
    }

    public class DrippingBugs : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        public DrippingBugs()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(1);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(2);
            //TODO Roll and move "back" 1D6 Rooms
        }
    }

    public class SpearCeiling : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.DEX);
        }
    }

    public class FloodedRoom : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Ceiling;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.INT);
        }
    }

    public class SecretWall : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            //TODO Attempt again next round - End Turn
        }

        protected override void CriticalFail()
        {
            //TODO Block one exit in room
        }
    }

    public class RatsInTheWall : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.STR);
        }
    }

    public class MirrorRoom : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            //TODO -1D6 Bleed for 3 three rounds
        }
    }

    public class DartLauncher : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            //TODO -1D6 Bleed for 3 three rounds
        }
    }

    public class AxDrop : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.STR);
        }
    }

    public class DragonStatue : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Wall;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(4);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(6);
            RollForItem();
        }

        private void RollForItem()
        {
            DDDice.Instance.RollD6(tile.transform, 3);
            tile.StartCoroutine(WaitAndDetermineResult());
        }

        private IEnumerator WaitAndDetermineResult()
        {
            yield return new WaitForSeconds(2.5f);
            int dieRoll = Dice.Value("d6");
            activatingPlayer.PlayerInventory.RemoveItemAtIndex(dieRoll);
        }
    }

    public class TrapChest : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        public TrapChest()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.CON);
        }
    }

    public class ScaldingFloor : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(4);
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.DEX);
        }
    }

    public class TripRope : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(1);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(3);
            //TODO Lose 1Dx20 Gold
        }
    }

    public class ConveyorBelt : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(1);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(3);
            //TODO Go back 3 spaces
        }
    }

    public class BearTrap : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        public BearTrap()
        {
            isOneTime = true;
        }

        protected override void Fail()
        {
            activatingPlayer.DecreaseStat(Player.PlayerStat.StatType.DEX, 2);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(3);
            //TODO 1/2 movement for 3 rounds
        }
    }

    public class ShroomRoom : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.CON;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.Floor;
            }
        }

        protected override void Fail()
        {
            activatingPlayer.TakeDamage(3);
        }

        protected override void CriticalFail()
        {
            activatingPlayer.TakeDamage(5);
            //TODO player to left controls your actions for this round and next ----- isn't this round over, since you failed a trap?
        }
    }

    public class ArtifactRoom : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public ArtifactRoom()
        {
            isBuffRoom = true;
        }

        protected override void Pass()
        {
            //TODO Get an Artifact
            isArmed = false;
            activatingPlayer.IncreaseScore(DDConsts.ArtifactScore);
        }

        protected override void CriticalPass()
        {
            //TODO Get an Artifact
            isArmed = false;
            activatingPlayer.IncreaseScore(DDConsts.ArtifactScore);
        }
    }

    public class StorageRoom : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public StorageRoom()
        {
            isBuffRoom = true;
        }

        protected override void Pass()
        {
            RollAndAwardNewItem();
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            RollAndAwardNewItem();
            isArmed = false;
        }

        private bool RollAndAwardNewItem()
        {
            Item newItem = RollForItem();
            if (activatingPlayer.PlayerInventory.HasRoomInInventory)
            {
                activatingPlayer.PlayerInventory.AddNewItem(newItem);
                return true;
            }
            else return false;
        }

        private Item RollForItem()
        {
            int roll = UnityEngine.Random.Range(1, 6);
            switch (roll)
            {
                case 3:
                    return GameController.Instance.ItemFactory.GetItem("Apple");
                //TODO x2 Bandages

                case 4:
                    return GameController.Instance.ItemFactory.GetItem("Apple");
                //TODO Minor Healing Potion

                case 5:
                    return GameController.Instance.ItemFactory.GetItem("Helmet");

                case 6:
                    return GameController.Instance.ItemFactory.GetItem("Apple");
                //TODO Pick Ax

                default:
                    return GameController.Instance.ItemFactory.GetItem("Apple");
            }
        }
    }

    public class HiddenStash : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public HiddenStash()
        {
            isBuffRoom = true;
            isOneTime = true;
        }

        protected override void Pass()
        {
            //TODO Roll 2D6 result x15 + 20
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            //TODO Roll 2D6 result x15 + 20
            isArmed = false;
        }
    }

    public class SecretLever : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public SecretLever()
        {
            isBuffRoom = true;
            isOneTime = true;
        }

        protected override void Pass()
        {
            //TODO Select the top # Room and place it on the nearest open wall to this room. If no open walls go clockwise until you find the closest one
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            //TODO Select the top # Room and place it on the nearest open wall to this room. If no open walls go clockwise until you find the closest one
            isArmed = false;
        }
    }

    public class Library : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public Library()
        {
            isBuffRoom = true;
        }

        protected override void Pass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.INT);
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.INT);
            isArmed = false;
        }
    }

    public class HealthFountain : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public HealthFountain()
        {
            isOneTime = true;
            isBuffRoom = true;
        }

        private void RollForHealth()
        {
            DDDice.Instance.RollD6(tile.transform, 5);
            tile.StartCoroutine(WaitAndDetermineResult());
        }

        private IEnumerator WaitAndDetermineResult()
        {
            yield return new WaitForSeconds(2.5f);
            int dieRoll = Dice.GetD3ValueOfD6();
            activatingPlayer.Heal(dieRoll);
        }

        protected override void Pass()
        {
            RollForHealth();
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            RollForHealth();
            isArmed = false;
        }
    }

    public class Armory : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public Armory()
        {
            isBuffRoom = true;
        }

        protected override void Pass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.CON);
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.CON);
            isArmed = false;
        }
    }

    public class AlchemistsChamber : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public AlchemistsChamber()
        {
            isBuffRoom = true;
        }

        private void RollForHealth()
        {
            DDDice.Instance.RollD6(tile.transform, 5);
            tile.StartCoroutine(WaitAndDetermineResult());
        }

        private void RollForItem()
        {
            DDDice.Instance.RollD6(tile.transform, 3);
            tile.StartCoroutine(WaitAndDetermineResult());
        }

        private IEnumerator WaitAndDetermineResult()
        {
            yield return new WaitForSeconds(2.5f);
            int dieRoll = Dice.GetD3ValueOfD6();
            switch (dieRoll)
            {
                case 1:
                    activatingPlayer.PlayerInventory.AddNewItem(GameController.Instance.ItemFactory.GetItem("Apple"));
                    break;

                case 2:
                    activatingPlayer.PlayerInventory.AddNewItem(GameController.Instance.ItemFactory.GetItem("Minor Potion of DEX"));
                    break;

                case 3:
                    activatingPlayer.PlayerInventory.AddNewItem(GameController.Instance.ItemFactory.GetItem("Minor Potion of CON"));
                    break;

                case 4:
                    activatingPlayer.PlayerInventory.AddNewItem(GameController.Instance.ItemFactory.GetItem("Minor Potion of INT"));
                    break;

                case 5:
                    activatingPlayer.PlayerInventory.AddNewItem(GameController.Instance.ItemFactory.GetItem("Minor Potion of STR"));
                    break;

                case 6:
                    activatingPlayer.PlayerInventory.AddNewItem(GameController.Instance.ItemFactory.GetItem("Apple"));
                    //TODO Minor Health Potion
                    break;
            }
        }

        protected override void Pass()
        {
            RollForItem();
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            RollForItem();
            isArmed = false;
        }
    }

    public class TrainingRoom : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public TrainingRoom()
        {
            isBuffRoom = true;
        }

        protected override void Pass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.DEX);
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.DEX);
            isArmed = false;
        }
    }

    public class MessHall : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public MessHall()
        {
            isBuffRoom = true;
        }

        protected override void Pass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.STR);
            isArmed = false;
        }

        protected override void CriticalPass()
        {
            activatingPlayer.IncreaseStat(Player.PlayerStat.StatType.STR);
            isArmed = false;
        }
    }

    public class NoEvent : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        public NoEvent()
        {
            isArmed = false;
        }
    }

    public class ExitDungeonEvent : RoomEvent
    {
        public override Player.PlayerStat.StatType StatRollType
        {
            get
            {
                return Player.PlayerStat.StatType.INT;
            }
        }

        public override RoomTrapType TrapType
        {
            get
            {
                return RoomTrapType.None;
            }
        }

        private UIManager uiMan;
        private ExitDungeonUI exitUI;
        private PlayerManager playerMan;

        public ExitDungeonEvent()
        {
            uiMan = GameObject.Find("UI").GetComponent<UIManager>();
            playerMan = GameObject.Find("PlayerManager").GetComponent<PlayerManager>();
        }

        public override void ActivateTrap(Player player)
        {
            activatingPlayer = player;
            exitUI = (ExitDungeonUI)uiMan.OpenUIImmediately(UISystem.WindowDisplays.ExitDungeonUI);
            exitUI.onContinueExploring += OnPlayerStayInDungeon;
            exitUI.onLeaveDungeon += OnPlayerLeftDungeon;
        }

        public void OnPlayerLeftDungeon()
        {
            activatingPlayer.ExitDungeon();
            playerMan.PlayerExittedDungeon(activatingPlayer);
            exitUI.onLeaveDungeon -= OnPlayerLeftDungeon;
            if (onRoomEventComplete != null)
                onRoomEventComplete(false);
        }

        public void OnPlayerStayInDungeon()
        {
            exitUI.onContinueExploring -= OnPlayerStayInDungeon;
            if (onRoomEventComplete != null)
                onRoomEventComplete(true);
        }
    }
}