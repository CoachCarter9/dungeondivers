﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;

namespace DungeonDivers
{
    public class DungeonPlacementManager : PunBehaviour
    {
        public static DungeonPlacementManager Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject gConGO = GameObject.Find("GameController");
                    instance = gConGO.AddComponent<DungeonPlacementManager>();
                }
                return instance;
            }
        }

        private static DungeonPlacementManager instance;

        public delegate void OnConfirmedTile(RoomTile tile);
        public OnConfirmedTile onConfirmedTile;

        public int TilesPlaced
        {
            get
            {
                return placedTiles.Count;
            }
        }

        public int MinDungeonSize
        {
            get { return 10; }
        }

        private DungeonTileManager dungTileMan;

        private void InitTileOffsets()
        {
            tileOffsets.Add(RoomTile.ConnectorDirection.North, new Vector3(0, 0, tileSize));
            tileOffsets.Add(RoomTile.ConnectorDirection.South, new Vector3(0, 0, -tileSize));
            tileOffsets.Add(RoomTile.ConnectorDirection.East, new Vector3(tileSize, 0, 0));
            tileOffsets.Add(RoomTile.ConnectorDirection.West, new Vector3(-tileSize, 0, 0));
        }

        public static int tileSize = 10;
        private GameObject placedTilesContainer;

        private static Dictionary<Vector3, RoomTile> placedTiles = new Dictionary<Vector3, RoomTile>();
        public static Dictionary<RoomTile.ConnectorDirection, Vector3> tileOffsets = new Dictionary<RoomTile.ConnectorDirection, Vector3>();

        private RoomTile tileToPlace;
        private bool isPlacingTile;
        public bool IsPlacingTile
        {
            get { return isPlacingTile; }
        }

        private List<Vector3> placementLocs = new List<Vector3>();
        public List<Vector3> PlacementLocs
        {
            get { return placementLocs; }
        }

        public enum RotationDirection
        {
            Left, Right
        }

        private PhotonView photonView;

        private void Awake()
        {
            Init();
            photonView = GetComponent<PhotonView>();
            dungTileMan = GetComponent<DungeonTileManager>();
        }

        private void Init()
        {
            instance = this;
            InitTileOffsets();
            placedTilesContainer = GameObject.Find("Dungeon");
        }

        public bool PlaceTile(Vector3 tileLoc, RoomTile tile)
        {
            if (placedTiles.ContainsKey(tileLoc)) return false;
            Debug.Log("Placing Tile: " + tile.ID);
            SetTileInDungeon(tileLoc, tile);
            isPlacingTile = true;
            SendTilePlacement(tile.transform.position, tile.ID);
            return AutoPlaceTileIfPossible();
        }

        private void SetTileInDungeon(Vector3 loc, RoomTile tile)
        {
            tile.transform.position = loc;
            tileToPlace = tile;
            placedTiles.Add(tile.transform.position, tile);
            tile.transform.SetParent(placedTilesContainer.transform);
        }

        private void SendTilePlacement(Vector3 tileLoc, int ID)
        {
            if (PhotonNetwork.connected)
            {
                Debug.Log("Sending tile with ID: " + ID);
                photonView.RPC("PlaceMPTile", PhotonTargets.OthersBuffered, new object[] { tileLoc, ID });
            }
        }

        [PunRPC]
        private void PlaceMPTile(Vector3 tileLoc, int ID)
        {
            RoomTile tileToPlace = DungeonTileManager.GetTileByID(ID);
            PlaceTile(tileLoc, tileToPlace);
            dungTileMan.RemoveTileFromDeck(tileToPlace);
        }

        public void RotatePlacementTile(RotationDirection direction)
        {
            if (tileToPlace != null)
                tileToPlace.RotateToNextValidPosition(direction);
        }

        public bool AutoPlaceTileIfPossible()
        {
            if (tileToPlace.CanAutoPlace())
            {
                ConfirmTilePlacement();
                return true;
            }
            if (IsValidPlacement() == false && tileToPlace != GameController.ExitDungeonTile)
            {
                tileToPlace.RotateToNextValidPosition(RotationDirection.Right);
            }
            return false;
        }

        public bool IsValidPlacement()
        {
            if (tileToPlace == null) return true;
            Vector3 tilePos = tileToPlace.transform.position;
            //Figure out what tiles we're connected to
            //Verify player is in a connecting tile
            return tileToPlace.HasValidConnection();
        }

        public void ConfirmTilePlacement()
        {
            if (tileToPlace == null)
            {
                Debug.Log("Tile to place is null");
                return;
            }
            tileToPlace.gameObject.name = tileToPlace.gameObject.name.Replace("(Clone)", tileToPlace.transform.position.ToString());
            tileToPlace.UpdateNeighborTiles();
            Debug.Log("Confirmed tile: " + tileToPlace.gameObject.name);
            //if (TilesPlaced < 10)
            //{
            //    placementLocs.Remove(tileToPlace.transform.position);
            //    AddAvailableLocsToPlacementLocs(tileToPlace);
            //}
            tileToPlace.transform.SetParent(placedTilesContainer.transform);
            if (onConfirmedTile != null)
                onConfirmedTile(tileToPlace);

            SendTileRotation();

            tileToPlace = null;
            isPlacingTile = false;

            //tile location, room event, tile type, id
        }

        private void SendTileRotation()
        {
            Debug.Log("Trying to send rotation of tile: " + tileToPlace.name);
            if (PhotonNetwork.connected)
                photonView.RPC("RotateMPTile", PhotonTargets.OthersBuffered, new object[] { tileToPlace.ModelRotation, tileToPlace.ID });
        }

        [PunRPC]
        private void RotateMPTile(Vector3 localRotInEulers, int ID)
        {
            RoomTile tileToPlace = DungeonTileManager.GetTileByID(ID);
            tileToPlace.ModelRotation = localRotInEulers;
        }

        public void DebugPlacementLocs()
        {
            for (int i = 0; i < placementLocs.Count; i++)
            {
                Debug.Log(placementLocs[i]);
            }
        }

        private void AddAvailableLocsToPlacementLocs(RoomTile tile)
        {
            CheckLocAndAddIfAvailable(tile, RoomTile.ConnectorDirection.North);
            CheckLocAndAddIfAvailable(tile, RoomTile.ConnectorDirection.South);
            CheckLocAndAddIfAvailable(tile, RoomTile.ConnectorDirection.East);
            CheckLocAndAddIfAvailable(tile, RoomTile.ConnectorDirection.West);
        }

        private void CheckLocAndAddIfAvailable(RoomTile tile, RoomTile.ConnectorDirection connectorDirection)
        {
            Vector3 tilePos = tile.transform.position + tileOffsets[connectorDirection];
            if (tile.connectorStatus[connectorDirection] == RoomTile.ConnectorState.Free)
            {
                if (placementLocs.Contains(tilePos) == false)
                    placementLocs.Add(tilePos);
            }
        }

        public static RoomTile GetTileAt(Vector3 worldPos)
        {
            if (placedTiles.ContainsKey(worldPos))
                return placedTiles[worldPos];
            return null;
        }

        #region Debug

        [SerializeField]
        private bool debugPlacingTile = false;

        private void Update()
        {
            if (debugPlacingTile)
            {
                Debug.Log(tileToPlace.name);
                debugPlacingTile = false;
            }
        }

        #endregion Debug
    }
}