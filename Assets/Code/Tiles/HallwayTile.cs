﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class HallwayTile : RoomTile
    {
        public override DungeonTileManager.DungeonTileType TileType
        {
            get
            {
                return DungeonTileManager.DungeonTileType.Hallway;
            }
        }

        private List<ConnectorDirection> connectedDirections = new List<ConnectorDirection>();

        public override bool CanAutoPlace()
        {
            RotateToAutoPlacement();
            return true;
        }

        public override void InitConnectors()
        {
            connectorStatus.Add(ConnectorDirection.North, ConnectorState.Free);
            connectorStatus.Add(ConnectorDirection.South, ConnectorState.Free);
            connectorStatus.Add(ConnectorDirection.East, ConnectorState.Unavailable);
            connectorStatus.Add(ConnectorDirection.West, ConnectorState.Unavailable);
        }
    }
}