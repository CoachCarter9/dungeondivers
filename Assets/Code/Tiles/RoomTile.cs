﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public abstract class RoomTile : MonoBehaviour
    {
        public enum ConnectorDirection
        {
            North, South, East, West
        }

        public enum ConnectorState
        {
            Unavailable, Occupied, Free
        }

        public Dictionary<ConnectorDirection, ConnectorState> connectorStatus = new Dictionary<ConnectorDirection, ConnectorState>();
        private bool hasPlayerConnection = false;

        public delegate void OnRoomEventSet(RoomEvent rEvent);
        public OnRoomEventSet onRoomEventSet;
        private RoomEvent roomEvent;
        public RoomEvent REvent
        {
            get
            {
                return roomEvent;
            }

            set
            {
                roomEvent = value;
                LoadEventArt();
                if (onRoomEventSet != null)
                    onRoomEventSet(value);
            }
        }

        private void LoadEventArt()
        {
            GameObject eventPrefab = Resources.Load("Traps/" + roomEvent.EventName) as GameObject;
            if (eventPrefab != null)
            {
                GameObject eventArt = Instantiate(eventPrefab, modelContainer.transform, false);
            }
        }

        private int id = 0;
        public int ID
        {
            get { return id; }
        }

        /// <summary>
        /// Should only be run once.
        /// </summary>
        /// <param name="id"></param>
        public void SetID(int id)
        {
            this.id = id;
        }

        [SerializeField]
        private GameObject modelContainer;

        public Vector3 ModelRotation
        {
            get
            {
                return modelContainer.transform.localEulerAngles;
            }
            set
            {
                modelContainer.transform.localEulerAngles = value;
            }
        }

        public bool debugConnectors;
        public bool debugRoomEventName;

        private void Update()
        {
            if (debugConnectors)
            {
                DebugConnectors();
                debugConnectors = false;
            }
            if (debugRoomEventName)
            {
                Debug.Log(REvent.EventName);
                debugRoomEventName = false;
            }
        }

        public bool HasValidConnection()
        {
            hasPlayerConnection = false;
            UpdateConnections();
            return hasPlayerConnection;
        }

        protected void RotateToAutoPlacement()
        {
            for (int i = 0; i < connectorStatus.Count; i++)
            {
                if (HasValidConnection())
                    return;
                else RotateTile(DungeonPlacementManager.RotationDirection.Right);
            }
        }

        protected void UpdateConnections()
        {
            CheckForPlayerInDirection(ConnectorDirection.North);
            CheckForPlayerInDirection(ConnectorDirection.South);
            CheckForPlayerInDirection(ConnectorDirection.West);
            CheckForPlayerInDirection(ConnectorDirection.East);
        }

        private void CheckForPlayerInDirection(ConnectorDirection direction)
        {
            if (connectorStatus[direction] == ConnectorState.Free)
            {
                RoomTile tile = DungeonPlacementManager.GetTileAt(transform.position + DungeonPlacementManager.tileOffsets[direction]);
                if (tile != null)
                {
                    if (tile == GameController.Instance.ActivePlayer.Tile)
                        hasPlayerConnection = true;
                }
            }
        }

        public void UpdateNeighborTiles()
        {
            UpdateNeighborTileConnections(ConnectorDirection.North, ConnectorDirection.South);
            UpdateNeighborTileConnections(ConnectorDirection.South, ConnectorDirection.North);
            UpdateNeighborTileConnections(ConnectorDirection.East, ConnectorDirection.West);
            UpdateNeighborTileConnections(ConnectorDirection.West, ConnectorDirection.East);
        }

        private void UpdateNeighborTileConnections(ConnectorDirection direction, ConnectorDirection oppoDirection)
        {
            RoomTile neighborTile = DungeonPlacementManager.GetTileAt(transform.position + DungeonPlacementManager.tileOffsets[direction]);
            if (neighborTile != null)
            {
                if (neighborTile.connectorStatus[oppoDirection] == ConnectorState.Free)
                    neighborTile.connectorStatus[oppoDirection] = ConnectorState.Occupied;
                if (connectorStatus[direction] == ConnectorState.Free)
                {
                    connectorStatus[direction] = ConnectorState.Occupied;
                }
            }
        }

        private void DebugConnectors()
        {
            Debug.Log("North Connector: " + connectorStatus[ConnectorDirection.North]);
            Debug.Log("South Connector: " + connectorStatus[ConnectorDirection.South]);
            Debug.Log("East Connector: " + connectorStatus[ConnectorDirection.East]);
            Debug.Log("West Connector: " + connectorStatus[ConnectorDirection.West]);
        }

        public void RotateToNextValidPosition(DungeonPlacementManager.RotationDirection tileRot)
        {
            int rotations = 0;
            do
            {
                RotateTile(tileRot);
                HasValidConnection();
                rotations++;
                if (rotations >= 4)
                {
                    Debug.Log("Too many rotations");
                    break;
                }
            }
            while (hasPlayerConnection == false);
        }

        public void RotateTile(DungeonPlacementManager.RotationDirection tileRot)
        {
            Dictionary<ConnectorDirection, ConnectorState> tempState = new Dictionary<ConnectorDirection, ConnectorState>(connectorStatus);
            switch (tileRot)
            {
                case DungeonPlacementManager.RotationDirection.Left:
                    modelContainer.transform.Rotate(Vector3.up, -90);
                    connectorStatus[ConnectorDirection.North] = tempState[ConnectorDirection.East];
                    connectorStatus[ConnectorDirection.West] = tempState[ConnectorDirection.North];
                    connectorStatus[ConnectorDirection.South] = tempState[ConnectorDirection.West];
                    connectorStatus[ConnectorDirection.East] = tempState[ConnectorDirection.South];
                    break;

                case DungeonPlacementManager.RotationDirection.Right:
                    modelContainer.transform.Rotate(Vector3.up, 90);
                    connectorStatus[ConnectorDirection.North] = tempState[ConnectorDirection.West];
                    connectorStatus[ConnectorDirection.West] = tempState[ConnectorDirection.South];
                    connectorStatus[ConnectorDirection.South] = tempState[ConnectorDirection.East];
                    connectorStatus[ConnectorDirection.East] = tempState[ConnectorDirection.North];
                    break;
            }
        }

        public abstract bool CanAutoPlace();

        public abstract void InitConnectors();

        public abstract DungeonTileManager.DungeonTileType TileType
        {
            get;
        }

        public void UpdateTileName()
        {
            string tileName = string.Format("{0}_{1}", id.ToString("00"), TileType.ToString());
            transform.name = tileName;
        }

        [PunRPC]
        protected void DisplayPlayerResults(int[] diceValues, int photonID, bool isArmed)
        {
            int dieRoll = Dice.GetD3ValueOfD6(diceValues);
            Debug.Log("Character Photon ID: " + photonID);
            DiverDiceResult diceResolver = PhotonView.Find(photonID).gameObject.GetComponent<DiverDiceResult>();
            diceResolver.ShowDiceRoll(diceValues);
            if (isArmed == false)
                REvent.DisarmTrap();
        }
    }
}