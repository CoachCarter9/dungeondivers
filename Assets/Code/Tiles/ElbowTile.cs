﻿using System;
using System.Collections;
using UnityEngine;

namespace DungeonDivers
{
    public class ElbowTile : RoomTile
    {
        public override DungeonTileManager.DungeonTileType TileType
        {
            get
            {
                return DungeonTileManager.DungeonTileType.Elbow;
            }
        }

        public override bool CanAutoPlace()
        {
            return false;
        }

        public override void InitConnectors()
        {
            connectorStatus.Add(ConnectorDirection.North, ConnectorState.Unavailable);
            connectorStatus.Add(ConnectorDirection.South, ConnectorState.Free);
            connectorStatus.Add(ConnectorDirection.East, ConnectorState.Unavailable);
            connectorStatus.Add(ConnectorDirection.West, ConnectorState.Free);
        }
    }
}