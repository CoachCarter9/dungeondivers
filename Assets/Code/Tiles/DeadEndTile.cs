﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class DeadEndTile : RoomTile
    {
        public override DungeonTileManager.DungeonTileType TileType
        {
            get
            {
                return DungeonTileManager.DungeonTileType.DeadEnd;
            }
        }

        private List<string> connectionDirections = new List<string>();

        public override bool CanAutoPlace()
        {
            if (gameObject.name == DDConsts.DungeonEntranceTileName)
                return false;
            RotateToAutoPlacement();
            return true;
        }

        public override void InitConnectors()
        {
            connectorStatus.Add(ConnectorDirection.North, ConnectorState.Free);
            connectorStatus.Add(ConnectorDirection.South, ConnectorState.Unavailable);
            connectorStatus.Add(ConnectorDirection.East, ConnectorState.Unavailable);
            connectorStatus.Add(ConnectorDirection.West, ConnectorState.Unavailable);
        }
    }
}