﻿using System;
using System.Collections;
using UnityEngine;

namespace DungeonDivers
{
    public class FourWayTile : RoomTile
    {
        public override DungeonTileManager.DungeonTileType TileType
        {
            get { return DungeonTileManager.DungeonTileType.FourWay; }
        }

        public override bool CanAutoPlace()
        {
            return true;
        }

        public override void InitConnectors()
        {
            connectorStatus.Add(ConnectorDirection.North, ConnectorState.Free);
            connectorStatus.Add(ConnectorDirection.South, ConnectorState.Free);
            connectorStatus.Add(ConnectorDirection.East, ConnectorState.Free);
            connectorStatus.Add(ConnectorDirection.West, ConnectorState.Free);
        }
    }
}