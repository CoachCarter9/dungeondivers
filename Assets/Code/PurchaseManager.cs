﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class PurchaseManager
    {
        public interface iShop
        {
            void PurchaseItem(Item i);

            void RefundItem(Item i);
        }

        public static bool PurchaseItem(Player p, Item item)
        {
            if (p.PlayerInventory.ItemCount >= p.PlayerInventory.MaxItems) return false;
            if (p.Gold >= item.ItemData.GoldValue)
            {
                p.PlayerInventory.AddNewItem((Item)item.Clone());
                p.Gold -= item.ItemData.GoldValue;
                return true;
            }
            return false;
        }
    }
}