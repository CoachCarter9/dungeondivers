﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class Inventory
    {
        public Inventory()
        {
            SetMaxItemCount(99);
        }

        public Inventory(Player owningPlayer)
        {
            this.owningPlayer = owningPlayer;
        }

        private Player owningPlayer;
        private List<Item> items = new List<Item>();
        public int ItemCount { get { return items.Count; } }

        private int maxItems = 0;
        public int MaxItems { get { return maxItems; } }

        public bool HasRoomInInventory
        {
            get { return items.Count < maxItems; }
        }

        public void SetMaxItemCount(int max)
        {
            maxItems = max;
            ReduceItems();
        }

        private void ReduceItems()
        {
            if (items.Count > maxItems)
            {
                items.RemoveRange(maxItems, items.Count);
            }
        }

        public bool AddNewItem(Item itemToAdd)
        {
            if (items.Count < maxItems)
            {
                items.Add(itemToAdd);
                return true;
            }
            else {
                Debug.Log("Player cannot carry any more items");
                return false;
            }
        }

        public Item GetItemAtIndex(int index)
        {
            if (index > items.Count)
            {
                throw new Exception("No item in slot: " + index);
            }
            return items[index];
        }

        public void UseItem(int slot)
        {
            if (slot > items.Count)
            {
                throw new Exception("No item in slot: " + slot);
            }
            Item temp = items[slot];
            GameEventManager.Instance.AddGameEvent(new UsedItemEvent(owningPlayer, temp));
            items.RemoveAt(slot);
            temp.Use(owningPlayer);
        }

        public void RemoveItem(Item i)
        {
            items.Remove(i);
        }

        public void RemoveItemAtIndex(int index)
        {
            if (items[index] != null)
                items.RemoveAt(index);
        }
    }
}