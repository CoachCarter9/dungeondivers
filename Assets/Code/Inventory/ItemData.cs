﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData
{
    public ItemData(string name, Sprite icon, int value)
    {
        this.name = name;
        this.icon = icon;
        this.value = value;
    }

    protected string name = "";
    public string Name { get { return name; } }

    protected Sprite icon;
	public Sprite Icon{ get { return icon; } }

    protected int value = 10;
    public int GoldValue { get { return value; } }
}