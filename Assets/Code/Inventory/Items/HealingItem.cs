﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class HealingItem : Item
    {
        public HealingItem(ItemData itemData, int healAmount) : base(itemData)
        {
            this.healAmount = healAmount;
        }

        private int healAmount = 0;

        public override void Use(Player owningPlayer)
        {
            owningPlayer.Heal(healAmount);
        }
    }
}