﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class WearableItem : Item
    {
        public WearableItem(ItemData itemData, RoomEvent.RoomTrapType failType) : base(itemData)
        {
            this.failType = failType;
        }

        private RoomEvent.RoomTrapType failType;
        public RoomEvent.RoomTrapType FailType { get { return failType; } }

        public override void Use(Player target)
        {
        }
    }
}