﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class StatModifierItem : Item
    {
        public StatModifierItem(ItemData itemData, Player.PlayerStat.StatType statType, int durationInTurns, int statIncrease) : base(itemData)
        {
            this.statType = statType;
            this.durationInTurns = durationInTurns;
            this.statIncrease = statIncrease;
        }

        private Player.PlayerStat.StatType statType;
        public Player.PlayerStat.StatType StatType { get { return statType; } }

        private int durationInTurns = 0;
        private int statIncrease = 0;

        public override void Use(Player target)
        {
            target.AddBuff(new StatModifier(statType, 3, statIncrease));
            PlayerVFXSpawner.SpawnStatUpFXOnPlayer(target, statType);
        }
    }
}