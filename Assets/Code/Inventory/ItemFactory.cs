﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class ItemFactory
    {
        public ItemFactory()
        {
            CreateItems();
        }

        private string spritePath = "ItemSprites/";
        private Dictionary<string, Item> items = new Dictionary<string, Item>();

        private void CreateItems()
        {
            int appleHealAmount = 3;
            int appleValue = 10;
            ItemData appleData = new ItemData("Apple", GetSprite("apple"), appleValue);
            items.Add("Apple", new HealingItem(appleData, appleHealAmount));

            int armorValue = 70;
            ItemData armorData = new ItemData("Armor", GetSprite("armor"), armorValue);
            items.Add("Armor", new WearableItem(armorData, RoomEvent.RoomTrapType.Wall));

            int helmetValue = 40;
            ItemData helmetData = new ItemData("Helmet", GetSprite("helmet"), helmetValue);
            items.Add("Helmet", new WearableItem(helmetData, RoomEvent.RoomTrapType.Ceiling));

            int bootsValue = 55;
            ItemData bootsData = new ItemData("Boots", GetSprite("boots"), bootsValue);
            items.Add("Boots", new WearableItem(bootsData, RoomEvent.RoomTrapType.Floor));

            int statModDurationInTurns = 3;
            int statModIncrease = 1;

            int strPotValue = 75;
            ItemData strPotData = new ItemData("Minor Potion of STR", GetSprite("strengthPotion"), strPotValue);
            items.Add("Minor Potion of STR", new StatModifierItem(strPotData, Player.PlayerStat.StatType.STR, statModDurationInTurns, statModIncrease));

            int dexPotValue = 75;
            ItemData dexPotData = new ItemData("Minor Potion of DEX", GetSprite("dexterityPotion"), dexPotValue);
            items.Add("Minor Potion of DEX", new StatModifierItem(dexPotData, Player.PlayerStat.StatType.STR, statModDurationInTurns, statModIncrease));

            int intPotValue = 75;
            ItemData intPotData = new ItemData("Minor Potion of INT", GetSprite("intelligencePotion"), intPotValue);
            items.Add("Minor Potion of INT", new StatModifierItem(intPotData, Player.PlayerStat.StatType.STR, statModDurationInTurns, statModIncrease));

            int conPotValue = 75;
            ItemData conPotData = new ItemData("Minor Potion of CON", GetSprite("constitutionPotion"), conPotValue);
            items.Add("Minor Potion of CON", new StatModifierItem(conPotData, Player.PlayerStat.StatType.STR, statModDurationInTurns, statModIncrease));
        }

        private Sprite GetSprite(string spriteName)
        {
            return (Sprite)Resources.Load((spritePath + spriteName), typeof(Sprite));
        }

        public Item GetItem(string name)
        {
            if (items.ContainsKey(name) == false)
                throw new System.Exception(name + " doesn't exist. Check your key and try again");
            return items[name];
        }
    }
}