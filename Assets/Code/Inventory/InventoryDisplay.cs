﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class InventoryDisplay : MonoBehaviour
    {
        [SerializeField]
        private Transform itemContainer;

        private InventoryItemDisplay[] itemDisplays;

        private void Awake()
        {
            itemDisplays = itemContainer.GetComponentsInChildren<InventoryItemDisplay>();
        }

        public void ShowInventory(Inventory inventory)
        {
            for (int i = 0; i < itemDisplays.Length; i++)
            {
                if (i >= inventory.ItemCount)
                {
                    itemDisplays[i].gameObject.SetActive(false);
                    continue;
                }

                itemDisplays[i].DisplayItem(inventory.GetItemAtIndex(i));
                itemDisplays[i].gameObject.SetActive(true);
            }
        }
    }
}