﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class InventoryItemDisplay : MonoBehaviour
    {
        [SerializeField]
        private Text nameLabel;

        [SerializeField]
        private Text valueLabel;

        [SerializeField]
        private Image itemSprite;

        private Item currentItem;
        public Item CurrentItem { get { return currentItem; } }

        private PurchaseManager.iShop shop;

        private void Awake()
        {
            shop = GetComponentInParent<PurchaseManager.iShop>();
        }

        public void DisplayItem(Item itemToDisplay)
        {
            currentItem = itemToDisplay;
            UpdateDisplay();
        }

        public void UpdateDisplay()
        {
            UpdateName();
            UpdateValue();
            UpdateSprite();
        }

        private void UpdateName()
        {
            if (nameLabel != null)
                nameLabel.text = currentItem.ItemData.Name;
        }

        private void UpdateValue()
        {
            if (valueLabel != null)
                valueLabel.text = currentItem.ItemData.GoldValue.ToString();
        }

        private void UpdateSprite()
        {
            if (itemSprite != null)
            {
                itemSprite.overrideSprite = currentItem.ItemData.Icon;
            }
        }

        public void BuyItem()
        {
            shop.PurchaseItem(currentItem);
        }

        public void RefundItem()
        {
            shop.RefundItem(currentItem);
        }
    }
}