﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public abstract class Item : ICloneable
    {
        public Item(ItemData itemData)
        {
            this.itemData = itemData;
        }

        private ItemData itemData;
        public ItemData ItemData { get { return itemData; } }

        public abstract void Use(Player target);

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}