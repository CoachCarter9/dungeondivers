﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GamePromptManager
{
    public static GamePromptUI messagePrompt;

    public static void DisplayMessage(string message)
    {
        if (messagePrompt != null)
            messagePrompt.DisplayMessage(message);
    }

    public static void ClearMessage()
    {
        DisplayMessage(string.Empty);
    }
}