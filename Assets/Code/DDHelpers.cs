﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDHelpers
{
    public static bool IsOffline()
    {
        return PhotonNetwork.connected == false || PhotonNetwork.offlineMode;
    }

    public static bool IsOfflineOrMasterClient()
    {
        return IsOffline() || PhotonNetwork.isMasterClient;
    }
}