﻿using KatalystKreations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class Player
    {
        public Player(string name, PlayerColor color, Dictionary<PlayerStat.StatType, PlayerStat> stats)
        {
            this.stats = stats;
            this.health = stats[PlayerStat.StatType.CON].StatValue * 10;
            this.name = name;
            remainingActions = GetStatValueOfType(PlayerStat.StatType.DEX);
            pColor = color;
            inventory = new Inventory(this);
            inventory.SetMaxItemCount(stats[PlayerStat.StatType.STR].StatValue);
            Gold = DDConsts.PlayerStartingGold;
        }

        public PhotonPlayer photonPlayer;

        public MPPlayerStatSync MPStatSync
        {
            set
            {
                mpStatSync = value;
                mpStatSync.PlayerData = this;
                Debug.Log("Player data sent to MP Stat Sync");
            }
        }

        private MPPlayerStatSync mpStatSync;

        private string name;

        public string Name
        {
            get { return name; }
        }

        private Inventory inventory;
        public Inventory PlayerInventory { get { return inventory; } }

        public enum PlayerColor
        {
            Red = 1,
            Green = 2,
            Blue = 3,
            Yellow = 4
        }

        private PlayerColor pColor;

        public PlayerColor PColor
        {
            get
            {
                return pColor;
            }
        }

        private int remainingActions = 0;

        public int RemainingActions
        {
            get { return remainingActions; }
        }

        public void ClearActions()
        {
            remainingActions = 0;
        }

        public void UsedAction()
        {
            remainingActions--;
        }

        public void ResetForNextTurn()
        {
            remainingActions = GetStatValueOfType(PlayerStat.StatType.DEX);
            DecreaseStatBuffs();
        }

        private void DecreaseStatBuffs()
        {
            for (int i = 0; i < buffs.Count; i++)
            {
                buffs[i].TurnsRemaining -= 1;
            }
        }

        public GameObject CharacterModel
        {
            get
            {
                return charModel;
            }
            set
            {
                charModel = value;
                if (charModel.GetComponent<UnitMover>() == null)
                    unitMover = charModel.AddComponent<UnitMover>();
                else unitMover = charModel.GetComponent<UnitMover>();
                charModel.GetComponent<PlayerColorChanger>().SetColor(pColor);
            }
        }

        private GameObject charModel;

        private UnitMover unitMover;

        private int score = 0;

        public int Score
        {
            get { return score; }
        }

        public void IncreaseScore(int amount)
        {
            score += amount;
        }

        private int gold;
        public int Gold
        {
            get
            {
                return gold;
            }
            set
            {
                gold = value;
                if (gold <= 0)
                    gold = 0;
            }
        }

        public void Move(Vector3 tilePosToMoveTo, UnitMover.OnMoveCompleted callback)
        {
            unitMover.Move(tilePosToMoveTo, callback);
        }

        public class PlayerStat
        {
            public PlayerStat(StatType type, int[] tierValues)
            {
                this.type = type;
                this.tierValues = tierValues;
            }

            public enum StatTier
            {
                Strong,
                AboveAvg,
                BelowAvg,
                Weak
            }

            public enum StatType
            {
                STR,
                DEX,
                INT,
                CON
            }

            private StatType type;
            private int[] tierValues;
            private int statLevel = 4;

            public int StatValue
            {
                get
                {
                    return tierValues[statLevel];
                }
            }

            public void IncreaseStat(int amount = 1)
            {
                if (statLevel + amount < tierValues.Length)
                {
                    statLevel += amount;
                }
                else statLevel = tierValues.Length - 1;
            }

            public void DecreaseStat(int amount = 1)
            {
                if (statLevel - amount >= 0)
                {
                    statLevel -= amount;
                }
            }
        }

        private Dictionary<PlayerStat.StatType, PlayerStat> stats;
        private List<StatModifier> buffs = new List<StatModifier>();

        public void AddBuff(StatModifier buff)
        {
            buffs.Add(buff);
            stats[buff.StatType].IncreaseStat(buff.StatIncrease);
            buff.onBuffExpired += RemoveBuff;
        }

        private void RemoveBuff(StatModifier buff)
        {
            buffs.Remove(buff);
            GameEventManager.Instance.AddGameEvent(new StatModifierExpired(this, buff));
            buff.onBuffExpired -= RemoveBuff;
            stats[buff.StatType].DecreaseStat(buff.StatIncrease);
        }

        public RoomTile Tile
        {
            get
            {
                return tile;
            }
            set
            {
                tile = value;
                CharacterModel.transform.position = tile.transform.position;
                //if (mpStatSync != null)
                //    mpStatSync.UpdatePlayerTile();
            }
        }

        private RoomTile tile;

        private int health;

        public int Health
        {
            get { return health; }
        }

        public void TakeDamage(int damage)
        {
            Debug.Log(Name + " took " + damage + " damage");
            GameEventManager.Instance.AddGameEvent(new TookDamageEvent(this, damage));
            health -= damage;
            FloatingText.Spawn(CharacterModel.transform, damage.ToString(), FloatingTextType.Damage);
            if (health <= 0)
            {
                score = 0;
                charModel.SetActive(false);
                if (onPlayerDied != null)
                    onPlayerDied(this);
            }
        }

        public void Heal(int amount)
        {
            health += amount;
            GameController.Instance.UpdateCurrentPlayerDisplay();
            DDFloatText.ShowPlayerHeal(CharacterModel.transform, amount);
        }

        public delegate void OnPlayerDied(Player player);
        public OnPlayerDied onPlayerDied;

        public int GetStatValueOfType(PlayerStat.StatType type)
        {
            return stats[type].StatValue;
        }

        public void IncreaseStat(PlayerStat.StatType type, int amount = 1)
        {
            stats[type].IncreaseStat(amount);
            GameEventManager.Instance.AddGameEvent(new StatChangeEvent(this, amount, type));
            PlayerVFXSpawner.SpawnStatUpFXOnPlayer(this, type);
            DDFloatText.ShowStatIncrease(amount, type);
        }

        public void DecreaseStat(PlayerStat.StatType type, int amount = 1)
        {
            stats[type].DecreaseStat(amount);
            GameEventManager.Instance.AddGameEvent(new StatChangeEvent(this, -amount, type));
            PlayerVFXSpawner.SpawnStatDownFXOnPlayer(this, type);
            FloatingTextType textType = DDConsts.GetFloatingTextTypeForStat(type);
            FloatingText.Spawn(CharacterModel.transform, string.Format("-{0}{1}", amount, type), textType);
            DDFloatText.ShowStatDecrease(amount, type);
        }

        public void ExitDungeon()
        {
            charModel.transform.position = new Vector3(999, 999, 999);
            charModel.SetActive(false);
        }

        public void GenerateStartingInventory()
        {
            inventory.AddNewItem(GameController.Instance.ItemFactory.GetItem("Apple"));
        }
    }
}