﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace DungeonDivers
{
    public class MPController : NetworkBehaviour
    {
        public static MPController Instance
        {
            get
            {
                return instance;
            }
        }

        private static MPController instance;
    }
}