﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using System;

namespace DungeonDivers
{
    public class MPPlayerStatSync : PunBehaviour, IPunObservable
    {
        private Player playerData;
        public Player PlayerData
        {
            set
            {
                playerData = value;
            }
        }

        [SerializeField]
        private UnitMover unitMover;

        [PunRPC]
        private void CreatePlayerAndUpdate(string pPlayerNickName, int playerColor)
        {
            PhotonPlayer pPlayer = GetPhotonPlayerByName(pPlayerNickName);
            Player newPlayer = new Player(pPlayer.NickName, (Player.PlayerColor)playerColor, PlayerManager.Instance.AutoGenerateStats());
            PlayerManager.Instance.AddPlayer(newPlayer);
            playerData = newPlayer;
            UpdatePlayerWithStatSync(newPlayer, pPlayer);
        }

        public void UpdatePlayerWithStatSync(Player player, PhotonPlayer pPlayer)
        {
            player.MPStatSync = this;
            player.photonPlayer = pPlayer;
            player.CharacterModel = gameObject;
        }

        private PhotonPlayer GetPhotonPlayerByName(string nickName)
        {
            for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
            {
                if (PhotonNetwork.playerList[i].NickName == nickName)
                    return PhotonNetwork.playerList[i];
            }
            throw new Exception(string.Format("No player with name: {0} is currently in the room.", nickName));
        }

        //callback for when player is created inside room
        public override void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            base.OnPhotonInstantiate(info);
        }

        //When player leaves the room/disconnects
        public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
        {
            base.OnPhotonPlayerDisconnected(otherPlayer);
        }

        //Send/Receive Info on player changes -- Used for realtime stat updating
        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.isWriting)
            {
                // We own this player: send the others our data
                stream.SendNext(transform.position);
                stream.SendNext(transform.eulerAngles);
            }
            else
            {
                // Network player, receive data
                transform.position = (Vector3)stream.ReceiveNext();
                transform.eulerAngles = (Vector3)stream.ReceiveNext();
            }
        }
    }
}