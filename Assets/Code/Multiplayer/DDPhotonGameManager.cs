﻿using Photon;
using System.Collections;
using System.Collections.Generic;
using UISystem;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DungeonDivers
{
    public class DDPhotonGameManager : PunBehaviour
    {
        public static DDPhotonGameManager Singleton
        {
            get
            {
                return singleton;
            }
        }

        [SerializeField]
        private PhotonLogLevel logLevel;

        public enum PlayModeType
        {
            Local_Game,
            Online_Game,
            Host_Game,
            Join_Game
        }

        private static DDPhotonGameManager singleton = null;

        private delegate void ConnectedToMaster();
        private ConnectedToMaster onConnectedToMaster;

        private string photonRoomName = "";

        private UIManager uiMan;

        private void Awake()
        {
            if (singleton != null)
            {
                Destroy(gameObject);
                return;
            }
            singleton = this;
            PhotonNetwork.logLevel = logLevel;
            PhotonNetwork.autoJoinLobby = false;
            PhotonNetwork.automaticallySyncScene = true;
        }

        public void StartOfflineGame()
        {
            PhotonNetwork.offlineMode = true;
            PhotonNetwork.CreateRoom("Offline Room");
            SceneManager.LoadScene("Dungeon");
        }

        public void FindOnlineGame()
        {
            PhotonNetwork.offlineMode = false;
            if (PhotonNetwork.connected)
            {
                FindRandomRoom();
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings(DDConsts.shortVersion);
                onConnectedToMaster += FindRandomRoom;
            }
        }

        public void HostPrivateGame(string roomName)
        {
            PhotonNetwork.offlineMode = false;
            if (PhotonNetwork.connected)
            {
                CreateHostedRoom();
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings(DDConsts.shortVersion);
                onConnectedToMaster += CreateHostedRoom;
            }
        }

        public void JoinPrivateGame(string roomName)
        {
            PhotonNetwork.offlineMode = false;
            if (PhotonNetwork.connected)
            {
                JoinRoom();
            }
            {
                PhotonNetwork.ConnectUsingSettings(DDConsts.shortVersion);
                onConnectedToMaster += JoinRoom;
            }
        }

        private void FindRandomRoom()
        {
            Debug.Log("Find random room");
            onConnectedToMaster -= FindRandomRoom;
            PhotonNetwork.JoinRandomRoom();
        }

        #region PhotonCallbacks

        public override void OnConnectedToMaster()
        {
            PhotonNetwork.playerName = PlayerPrefs.GetString(DDConsts.PPKEY_Username);
            Debug.Log("Joined Lobby");
            if (onConnectedToMaster != null)
                onConnectedToMaster();
        }

        private void OnPhotonRandomJoinFailed()
        {
            Debug.Log("Can't join random room!");
            CreateRoom(null);
        }

        private void CreateHostedRoom()
        {
            onConnectedToMaster -= CreateHostedRoom;
            CreateRoom(photonRoomName);
            photonRoomName = null;
        }

        private void CreateRoom(string roomName)
        {
            PhotonNetwork.CreateRoom(roomName, new RoomOptions()
            {
                MaxPlayers = DDConsts.maxPlayersPerGame
            }, null);
        }

        private void JoinRoom()
        {
            onConnectedToMaster -= JoinRoom;
            PhotonNetwork.JoinRoom(photonRoomName);
            photonRoomName = string.Empty;
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("Joined room");
        }

        #endregion PhotonCallbacks

        #region Debug

        private void OnGUI()
        {
            GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
        }

        #endregion Debug
    }
}