﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class StatModifier
    {
        public StatModifier(Player.PlayerStat.StatType statType, int turnsRemaining, int statValueModifier)
        {
            this.statType = statType;
            this.turnsRemaining = turnsRemaining;
            this.statValueModifier = statValueModifier;
        }

        private Player.PlayerStat.StatType statType;
        public Player.PlayerStat.StatType StatType
        {
            get
            {
                return statType;
            }

            set
            {
                statType = value;
            }
        }

        private int turnsRemaining = 0;

        public int TurnsRemaining
        {
            get
            {
                return turnsRemaining;
            }

            set
            {
                turnsRemaining = value;
                if (turnsRemaining == 0)
                {
                    if (onBuffExpired != null)
                        onBuffExpired(this);
                }
            }
        }

        public delegate void OnBuffExpired(StatModifier buff);
        public OnBuffExpired onBuffExpired;
        private int statValueModifier;
        public int StatIncrease { get { return statValueModifier; } }
    }
}