﻿using System;
using System.Collections;
using System.Collections.Generic;
using UISystem;
using UnityEngine;

namespace DungeonDivers
{
    public class GameEventManager
    {
        public static GameEventManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameEventManager();
                }
                return instance;
            }
        }

        private static GameEventManager instance;

        private GameEventUI gameEventUI;

        public GameEventUI GameEventUI
        {
            set
            {
                gameEventUI = value;
            }
        }

        private List<GameEvent> gameEvents = new List<GameEvent>();

        public void AddGameEvent(GameEvent e)
        {
            gameEvents.Add(e);
            if (gameEventUI != null)
                gameEventUI.DisplayNewEvent(e);
        }
    }

    public abstract class GameEvent
    {
        public GameEvent(Player player, object relevantObject)
        {
            this.player = player;
        }

        private Player player;

        public Player EffectingPlayer
        {
            get { return player; }
        }

        public abstract string EventString
        {
            get;
        }
    }

    public class TookDamageEvent : GameEvent
    {
        public TookDamageEvent(Player player, int eventObject) : base(player, eventObject)
        {
            damageAmount = eventObject;
        }

        private int damageAmount = 0;

        public override string EventString
        {
            get
            {
                string eventString = string.Format("{0} took {1} damage", EffectingPlayer.Name, damageAmount);
                return eventString;
            }
        }
    }

    public class ReceivedItem : GameEvent
    {
        public ReceivedItem(Player player, object eventObject) : base(player, eventObject)
        {
            //damageAmount = (int)eventObject;
        }

        //private int damageAmount = 0;
        public override string EventString
        {
            get
            {
                string eventString = string.Format("{0} received {1} item", EffectingPlayer.Name, "a new");
                return eventString;
            }
        }
    }

    public class UsedItemEvent : GameEvent
    {
        public UsedItemEvent(Player player, object eventObject) : base(player, eventObject)
        {
            itemUsed = (Item)eventObject;
        }

        private Item itemUsed;

        public override string EventString
        {
            get
            {
                string eventString = string.Format("{0} used {1} item", EffectingPlayer.Name, (itemUsed.ItemData.Name));
                return eventString;
            }
        }
    }

    public class StatModifierExpired : GameEvent
    {
        public StatModifierExpired(Player player, object eventObject) : base(player, eventObject)
        {
            modifier = (StatModifier)eventObject;
        }

        private StatModifier modifier;

        public override string EventString
        {
            get
            {
                string eventString = string.Format("{0} expired on {1} item", (modifier.StatType.ToString()), EffectingPlayer.Name);
                return eventString;
            }
        }
    }

    public class TriggeredRoomEvent : GameEvent
    {
        public TriggeredRoomEvent(Player player, object eventObject) : base(player, eventObject)
        {
            rEvent = (RoomEvent)eventObject;
        }

        private RoomEvent rEvent;

        public override string EventString
        {
            get
            {
                string eventString = string.Format("{0} triggered the {1} event", EffectingPlayer.Name, rEvent.EventName);
                return eventString;
            }
        }
    }

    public class RoomResultsEvent : GameEvent
    {
        public RoomResultsEvent(Player player, object eventObject) : base(player, eventObject)
        {
            eventResult = (string)eventObject;
        }

        private string eventResult;

        public override string EventString
        {
            get
            {
                string eventString = string.Format("{0} {1}", EffectingPlayer.Name, eventResult);
                return eventString;
            }
        }
    }

    public class DiceResolutionEvent : GameEvent
    {
        public DiceResolutionEvent(Player player, object eventObject, int bonusRoll) : base(player, eventObject)
        {
            diceRoll = (int)eventObject;
            this.bonusRoll = bonusRoll;
        }

        private int diceRoll;
        private int bonusRoll = 0;

        public override string EventString
        {
            get
            {
                string eventString = string.Format("{0} rolled a {1}", EffectingPlayer.Name, diceRoll);
                if (bonusRoll > 0) eventString += string.Format(" ({0} + {1})", (diceRoll - bonusRoll), bonusRoll);
                return eventString;
            }
        }
    }

    public class StatChangeEvent : GameEvent
    {
        public StatChangeEvent(Player player, int changeAmount, Player.PlayerStat.StatType statType) : base(player, changeAmount)
        {
            this.changeAmount = changeAmount;
            this.statType = statType;
        }

        private int changeAmount;
        private Player.PlayerStat.StatType statType;

        public override string EventString
        {
            get
            {
                string changeString = "gained";
                if (changeAmount < 0) changeString = "lost";
                string eventString = string.Format("{0} {1} {2} {3} points", EffectingPlayer.Name, changeString, Mathf.Abs(changeAmount), statType.ToString());
                return eventString;
            }
        }
    }
}