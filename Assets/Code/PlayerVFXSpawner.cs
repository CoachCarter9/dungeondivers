﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class PlayerVFXSpawner : ScriptableObject
    {
        private void Awake()
        {
            GameObject statLoad = (GameObject)Resources.Load("VFX_StatUp");
            statUpVFX = Instantiate(statLoad, new Vector3(999, 999, 999), Quaternion.identity);
            statLoad = (GameObject)Resources.Load("VFX_StatDown");
            statDownVFX = Instantiate(statLoad, new Vector3(999, 999, 999), Quaternion.identity);
        }

        private static GameObject statUpVFX;
        private static GameObject statDownVFX;

        public static void SpawnStatUpFXOnPlayer(Player player, Player.PlayerStat.StatType statType)
        {
            PlayStatVFXOnPlayer(player, statType, statUpVFX);
        }

        public static void SpawnStatDownFXOnPlayer(Player player, Player.PlayerStat.StatType statType)
        {
            PlayStatVFXOnPlayer(player, statType, statDownVFX);
        }

        private static void PlayStatVFXOnPlayer(Player player, Player.PlayerStat.StatType statType, GameObject vfx)
        {
            vfx.SetActive(false);
            vfx.transform.position = player.CharacterModel.transform.position;
            Color statColor = DDConsts.GetColorForStat(statType);
            ChangeVFXColor(statColor, vfx.transform);
            vfx.SetActive(true);
        }

        private static void ChangeVFXColor(Color newColor, Transform vfx)
        {
            foreach (Transform t in vfx)
            {
                foreach (Transform c in t)
                {
                    ChangeVFXColor(newColor, t);
                }
                ParticleSystem p = t.GetComponent<ParticleSystem>();
                ParticleSystem.MainModule sColor = p.main;
                sColor.startColor = newColor;
            }
        }
    }
}