﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PCInputController : InputController
{
    private LayerMask layerMask;

    private List<KeyCode> keysToListenFor = new List<KeyCode>();

    private void Awake()
    {
        layerMask = ~(1 << LayerMask.NameToLayer("mobaCameraBoundaryLayer"));
        InitKeyListeners();
    }

    private void InitKeyListeners()
    {
        keysToListenFor.Add(KeyCode.W);
        keysToListenFor.Add(KeyCode.A);
        keysToListenFor.Add(KeyCode.S);
        keysToListenFor.Add(KeyCode.D);
        keysToListenFor.Add(KeyCode.Q);
        keysToListenFor.Add(KeyCode.E);
        keysToListenFor.Add(KeyCode.Alpha1);
        keysToListenFor.Add(KeyCode.Alpha2);
        keysToListenFor.Add(KeyCode.Alpha3);
        keysToListenFor.Add(KeyCode.Alpha4);
        keysToListenFor.Add(KeyCode.Alpha5);
        keysToListenFor.Add(KeyCode.Alpha6);
        keysToListenFor.Add(KeyCode.Keypad1);
        keysToListenFor.Add(KeyCode.Keypad2);
        keysToListenFor.Add(KeyCode.Keypad3);
        keysToListenFor.Add(KeyCode.Keypad4);
        keysToListenFor.Add(KeyCode.Keypad5);
        keysToListenFor.Add(KeyCode.Keypad6);
        keysToListenFor.Add(KeyCode.Space);
        keysToListenFor.Add(KeyCode.Return);
        keysToListenFor.Add(KeyCode.KeypadEnter);
    }

    // Update is called once per frame
    private void Update()
    {
        pressedKeys.Clear();
        //if ((Input.GetMouseButtonDown(0)) && EventSystem.current.IsPointerOverGameObject() == false)
        //{
        //    // check if clicked on a tile and make it selected
        //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;
        //    if (Physics.Raycast(ray, out hit, 1000, layerMask))
        //    {
        //        //Debug.Log(hit.collider.gameObject.name);
        //        // should be a tile since I have nothing else in scene that has a collider
        //        // I remove the "Tile " bit from name to get the tile number (node index)
        //        if (onSelectionPressed != null)
        //            onSelectionPressed(hit.collider.gameObject);
        //    }
        //}
        //else
        if (Input.anyKeyDown)
        {
            AddPressedKeyToList();
            NotifyKeyObservers();
        }
    }

    private void AddPressedKeyToList()
    {
        foreach (KeyCode key in keysToListenFor)
        {
            if (Input.GetKeyDown(key))
            {
                pressedKeys.Add(key);
            }
        }
    }
}