﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public static class PlayerStatFactory
    {
        static PlayerStatFactory()
        {
            CreateStatTierValueDic();
        }

        private static Dictionary<Player.PlayerStat.StatTier, int[]> tierValues = new Dictionary<Player.PlayerStat.StatTier, int[]>();

        private static void CreateStatTierValueDic()
        {
            int[] strongTier = new int[9]
            {
            2,2,3,3,4,4,5,5,6
            };
            tierValues.Add(Player.PlayerStat.StatTier.Strong, strongTier);

            int[] aboveTier = new int[9]
            {
            1,2,3,3,3,4,4,5,5
            };
            tierValues.Add(Player.PlayerStat.StatTier.AboveAvg, aboveTier);

            int[] belowTier = new int[9]
            {
            1,2,2,3,3,3,4,4,5
            };
            tierValues.Add(Player.PlayerStat.StatTier.BelowAvg, belowTier);

            int[] weakTier = new int[9]
            {
            1,1,2,2,3,3,4,4,4
            };
            tierValues.Add(Player.PlayerStat.StatTier.Weak, weakTier);
        }

        public static Player.PlayerStat CreateStat(Player.PlayerStat.StatType type, Player.PlayerStat.StatTier tier)
        {
            return new Player.PlayerStat(type, tierValues[tier]);
        }
    }
}