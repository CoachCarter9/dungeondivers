﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class DDDice : MonoBehaviour
    {
        public static DDDice Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject temp = GameObject.Find("DiceRoller");
                    if (temp != null)
                    {
                        instance = temp.GetComponent<DDDice>();
                        if (temp == null)
                            instance = temp.AddComponent<DDDice>();
                    }
                    else
                    {
                        GameObject newGO = new GameObject("DiceRoller");
                        instance = newGO.AddComponent<DDDice>();
                    }
                }
                return instance;
            }
        }

        private static DDDice instance;

        [SerializeField]
        private GameObject dieSpawnPoint;

        [SerializeField]
        private Transform objectToParentTo;

        [SerializeField]
        private float xForceVar = 5;

        [SerializeField]
        private float zForceVar = 5;

        private Vector3 force
        {
            get
            {
                return new Vector3(xForceVar, 0, zForceVar);
            }
        }

        [SerializeField]
        private bool useDebugRoll = false;

        private void Awake()
        {
            PositionToObject();
        }

        private void PositionToObject()
        {
            Quaternion initRot = transform.rotation;
            Vector3 pos = transform.position;
            transform.SetParent(objectToParentTo, true);
            transform.position = pos;
            transform.rotation = initRot;
        }

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.R) && useDebugRoll)
            {
                RollD6();
            }
        }

        public void RollD6(int dieToRoll = 1)
        {
            RollD6(dieSpawnPoint.transform, dieToRoll);
        }

        public void RollD6(Transform spawnPoint, int dieToRoll = 1)
        {
            Dice.Clear();
            Dice.Roll("d6", "d6-black-dots", spawnPoint.position, force);
        }

        public void RollStatDice(Vector3 spawnPoint, Player.PlayerStat.StatType type, int numDiceToRoll)
        {
            string diceMaterial = "DD-normal";
            switch (type)
            {
                case Player.PlayerStat.StatType.STR:
                    diceMaterial = "DD-red";
                    break;

                case Player.PlayerStat.StatType.DEX:
                    diceMaterial = "DD-green";
                    break;

                case Player.PlayerStat.StatType.INT:
                    diceMaterial = "DD-blue";
                    break;

                case Player.PlayerStat.StatType.CON:
                    diceMaterial = "DD-yellow";
                    break;
            }
            RollEventDie(spawnPoint, numDiceToRoll, diceMaterial);
        }

        private void RollEventDie(Vector3 spawnPoint, int numDiceToRoll, string mat)
        {
            Dice.Roll(numDiceToRoll + "d6", "d6-" + mat, spawnPoint, force);
        }

        public DiceResolution GetDiceResolution(string dieType = "d6")
        {
            DiceResolution dRes = new DiceResolution();
            List<int> diceValues = new List<int>();

            string dMat = "";
            // loop all dice
            for (int d = 0; d < Dice.AllDice.Count; d++)
            {
                RollingDie rDie = (RollingDie)Dice.AllDice[d];
                dMat = rDie.mat;
                // check the type
                if (rDie.name == dieType || dieType == "")
                    diceValues.Add(rDie.die.value);
            }
            dRes.diceValues = diceValues;
            dRes.material = dMat;
            return dRes;
        }

        public struct DiceResolution
        {
            public string material;
            public List<int> diceValues;
        }

        private ArrayList numbers;

        //private Vector3 Force()
        //{
        //    Vector3 rollTarget = Vector3.zero + new Vector3(2 + 7 * UnityEngine.Random.value, .5F + 4 * Random.value, -2 - 3 * Random.value);
        //    return Vector3.Lerp(dieSpawnPoint.transform.position, rollTarget, 1).normalized * (-35 - Random.value * 20);
        //}
    }
}