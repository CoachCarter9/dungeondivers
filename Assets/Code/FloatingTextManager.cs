﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace KatalystKreations
{
    /// <summary>
    /// You can add as many Types as you'd like here and define their properties in the inspector.
    /// There must at least be a Default value in this enum.
    /// </summary>
    public enum FloatingTextType
    {
        Default = 0, //Do not remove this value.
        Damage,
        Heal,
        STR,
        DEX,
        INT,
        CON,
        Score,
        Gold
    }

    /// <summary>
    /// To use this script, create a canvas in the scene if you don't have one.
    /// This script will configue that canvas itself so if you already have one
    /// and don't want its settings changed, create a new canvas for this.
    /// Place this script on the canvas you are using.
    /// Whenever you want to create floating text, just call FloatingText.Spawn in code
    /// and fill out the desired parameters.
    /// NOTE: You'll need to add "using KatalystKreations;" to the top of a script you want to use this in.
    /// </summary>
    [RequireComponent(typeof(Canvas))]
    public class FloatingTextManager : MonoBehaviour
    {
        public static FloatingTextManager Instance;
        public int sortOrder = 100;

        [Tooltip("This contains all the information of the different structs that make up the Types. This is filled out in the inspector.")]
        public FloatingTextData[] floatingTextData;

        [SerializeField]
        private int poolSize = 5;

#if UNITY_EDITOR

        [Header("For Debugging")]
        [Tooltip("Turn this on and use the 1-4 (non-numpad) keys to display debugging text in the middle of the screen.")]
        public bool debugMode;

        [Tooltip("Used when debugging to track an object. Supply this for the follow tests to work.")]
        public Transform TESTOBJECT;

#endif
        private Canvas canvas;
        private List<CanvasGroup> inactiveText = new List<CanvasGroup>();
        private Dictionary<FloatingTextType, FloatingTextData> floatingData;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Debug.LogError("More than one FloatingTextManager detected, this should not happen. Delete one and try again.");
        }

        private void Start()
        {
            canvas = GetComponent<Canvas>();
            var scaler = canvas.GetComponent<CanvasScaler>();
            scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            scaler.referenceResolution = new Vector2(1920, 1080);
            scaler.matchWidthOrHeight = 0.5f;
            canvas.worldCamera = Camera.main;
            canvas.pixelPerfect = true;
            canvas.sortingOrder = sortOrder;
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            floatingData = PopulateDictionary();

            for (int i = 0; i < poolSize; i++)
            {
                CreateNewText();
            }
        }

#if UNITY_EDITOR

        private void Update()
        {
            if (debugMode == true)
            {
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    FloatingText.Spawn(Input.mousePosition, "Normal Damage!", FloatingTextType.Damage);
                }
                if (Input.GetKeyDown(KeyCode.Alpha3))
                {
                    FloatingText.Spawn(Input.mousePosition, "Normal Healing!", FloatingTextType.Heal);
                }
            }
        }

#endif

        public void SpawnFloatingText(Vector2 _location, string _text, FloatingTextType _type = FloatingTextType.Default)
        {
#if UNITY_EDITOR
            if (debugMode == true)
                Debug.Log("Spawning new FloatingText at " + _location);
#endif
            if (floatingData.ContainsKey(_type) == false)
            {
                Debug.LogError("No FloatingText of that Type found.");
                return;
            }
            var floatingText = FindAvailableText();
            floatingText.transform.localScale = Vector3.one;
            //Vector2 screenLoc = Camera.main.WorldToScreenPoint(_location);
            floatingText.transform.position = (_location + floatingData[_type].spawnOffset) + new Vector2(Random.Range(-floatingData[_type].staggerAmount, floatingData[_type].staggerAmount), Random.Range(-floatingData[_type].staggerAmount, floatingData[_type].staggerAmount));
            var t = floatingText.GetComponent<Text>();
            t.text = _text;
            t.fontSize = floatingData[_type].fontSize;
            if (floatingData[_type].font != null)
                t.font = floatingData[_type].font;
            t.color = floatingData[_type].color;
            floatingText.alpha = 1f;
            StartCoroutine(TextTimer(floatingText, floatingData[_type].duration));
            if (floatingData[_type].floating == true)
                StartCoroutine(FloatText(floatingText, floatingData[_type].duration, floatingData[_type].movementAmount, floatingData[_type].movementSpeed));
            if (floatingData[_type].shakeText == true)
                StartCoroutine(ShakeText(floatingText, floatingData[_type].duration));
            if (floatingData[_type].fade == true)
                StartCoroutine(FadeText(floatingText, floatingData[_type].duration));
            if (floatingData[_type].pulseSize == true)
                StartCoroutine(PulseScale(floatingText, floatingData[_type].duration));
        }

        public void SpawnFloatingText(Transform _location, string _text, FloatingTextType _type = FloatingTextType.Default)
        {
#if UNITY_EDITOR
            if (debugMode == true)
                Debug.Log("Spawning new FloatingText at " + _location);
#endif
            if (floatingData.ContainsKey(_type) == false)
            {
                Debug.LogError("No FloatingText of that Type found.");
                return;
            }
            var floatingText = FindAvailableText();
            floatingText.transform.localScale = Vector3.one;
            Vector2 screenLoc = Camera.main.WorldToScreenPoint(_location.position);
            floatingText.transform.position = (screenLoc + floatingData[_type].spawnOffset) + new Vector2(Random.Range(-floatingData[_type].staggerAmount, floatingData[_type].staggerAmount), Random.Range(-floatingData[_type].staggerAmount, floatingData[_type].staggerAmount));
            var t = floatingText.GetComponent<Text>();
            t.text = _text;
            t.fontSize = floatingData[_type].fontSize;
            if (floatingData[_type].font != null)
                t.font = floatingData[_type].font;
            t.color = floatingData[_type].color;
            floatingText.alpha = 1f;
            StartCoroutine(TextTimer(floatingText, floatingData[_type].duration));
            if (floatingData[_type].followTarget == true)
                StartCoroutine(FollowTarget(floatingText, _location, floatingData[_type].duration));
            else if (floatingData[_type].floating == true && floatingData[_type].followTarget == false)
                StartCoroutine(FloatText(floatingText, floatingData[_type].duration, floatingData[_type].movementAmount, floatingData[_type].movementSpeed));
            if (floatingData[_type].shakeText == true && floatingData[_type].followTarget == false)
                StartCoroutine(ShakeText(floatingText, floatingData[_type].duration));
            if (floatingData[_type].fade == true)
                StartCoroutine(FadeText(floatingText, floatingData[_type].duration));
            if (floatingData[_type].pulseSize == true)
                StartCoroutine(PulseScale(floatingText, floatingData[_type].duration));
        }

        private CanvasGroup CreateNewText(FloatingTextType type = FloatingTextType.Default)
        {
            GameObject go = new GameObject();
            go.name = "FloatingText " + type;
            go.transform.SetParent(transform);
            inactiveText.Insert(0, go.AddComponent<CanvasGroup>());
            inactiveText[0].alpha = 0f;
            var t = inactiveText[0].gameObject.AddComponent<Text>();
            t.color = floatingData[type].color;
            t.fontSize = floatingData[type].fontSize;
            if (floatingData[type].font != null)
                t.font = floatingData[type].font;
            t.raycastTarget = false;
            t.alignment = TextAnchor.MiddleCenter;
            t.horizontalOverflow = HorizontalWrapMode.Overflow;
            t.verticalOverflow = VerticalWrapMode.Overflow;
            inactiveText[0].blocksRaycasts = false;
            inactiveText[0].interactable = false;
            return inactiveText[0];
        }

        private CanvasGroup FindAvailableText()
        {
            CanvasGroup group = null;
            foreach (var t in inactiveText)
            {
                if (t.alpha == 0)
                {
                    group = t;
                    break;
                }
            }
            if (group != null)
                return group;
            else
                return CreateNewText();
        }

        private IEnumerator FloatText(CanvasGroup _text, float _duration, float _amount, float _speed)
        {
            var countdown = _duration;

            while (countdown > 0)
            {
                countdown -= Time.deltaTime;
                _text.transform.Translate(Vector2.up * (Time.deltaTime * _speed) * _amount);
                yield return new WaitForEndOfFrame();
            }
            yield break;
        }

        private IEnumerator TextTimer(CanvasGroup _text, float _duration)
        {
            yield return new WaitForSeconds(_duration);
#if UNITY_EDITOR
            if (debugMode == true)
                Debug.Log("Hiding recently spawned text after " + _duration.ToString() + " seconds elapsed.");
#endif
            _text.alpha = 0f;
        }

        private IEnumerator FadeText(CanvasGroup _text, float _duration)
        {
            while (_text.alpha > 0)
            {
                _text.alpha -= Time.deltaTime / _duration;
                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator FollowTarget(CanvasGroup _text, Transform _target, float _duration)
        {
            var countdown = _duration;
            while (countdown > 0)
            {
                countdown -= Time.deltaTime;
                if (_target != null)
                    _text.transform.position = Camera.main.WorldToScreenPoint(_target.transform.position);
                else
                    yield break;
                yield return null;
            }
        }

        private Dictionary<FloatingTextType, FloatingTextData> PopulateDictionary()
        {
            Dictionary<FloatingTextType, FloatingTextData> data = new Dictionary<FloatingTextType, FloatingTextData>();
            foreach (var fData in floatingTextData)
            {
                data.Add(fData.type, fData);
            }
            return data;
        }

        private IEnumerator PulseScale(CanvasGroup _text, float _duration)
        {
            var originalScale = _text.transform.localScale;
            float countdown = _duration / 8;
            while (countdown > 0)
            {
                countdown -= Time.deltaTime;
                _text.transform.localScale = Vector3.Lerp(_text.transform.localScale, originalScale * 2, Time.deltaTime * 8);
                yield return null;
            }
            countdown = _duration / 8;
            while (countdown > 0)
            {
                countdown -= Time.deltaTime;
                _text.transform.localScale = Vector3.Lerp(_text.transform.localScale, originalScale, Time.deltaTime * 8);
                yield return null;
            }
            _text.transform.localScale = originalScale;
        }

        private IEnumerator ShakeText(CanvasGroup _text, float _duration)
        {
            var countdown = _duration;
            Quaternion originalRotation = _text.transform.rotation;
            while (countdown > 0)
            {
                countdown -= Time.deltaTime;
                float z = Random.value * 20 - (20 / 2);
                _text.transform.eulerAngles = new Vector3(originalRotation.x, originalRotation.y, originalRotation.z + z);
                yield return null;
            }

            _text.transform.rotation = originalRotation;
        }
    }

    public static class FloatingText
    {
        /// <summary>
        /// Spawn in some Floating Text at a Vector2 location.
        /// </summary>
        /// <param name="location">The Vector2 location you want the text to appear on screen.</param>
        /// <param name="text">The words you want it to display.</param>
        /// <param name="type">The type of FloatingText you want as defined in the inspector of FloatingTextData</param>
        public static void Spawn(Vector2 location, string text, FloatingTextType type = FloatingTextType.Default)
        {
            FloatingTextManager.Instance.SpawnFloatingText(location, text, type);
        }

        /// <summary>
        /// Spawn in some Floating Text at a Vector2 location.
        /// </summary>
        /// <param name="location">The Transform where you want the text to appear on screen.</param>
        /// <param name="text">The words you want it to display.</param>
        /// <param name="type">The type of FloatingText you want as defined in the inspector of FloatingTextData</param>
        public static void Spawn(Transform location, string text, FloatingTextType type = FloatingTextType.Default)
        {
            FloatingTextManager.Instance.SpawnFloatingText(location, text, type);
        }
    }

    [System.Serializable]
    public struct FloatingTextData
    {
        public string name;
        public FloatingTextType type;
        public Color color;
        public Font font;
        public int fontSize;
        public float duration;
        public float movementAmount;
        public float movementSpeed;
        public float staggerAmount;
        public Vector2 spawnOffset;
        public bool fade;
        public bool floating;
        public bool pulseSize;
        public bool shakeText;
        public bool followTarget;
    }

    [System.Serializable]
    public struct TextTypeData
    {
        public FloatingTextType type;
        public Color color;
        public Font font;
        public int fontSize;
    }
}