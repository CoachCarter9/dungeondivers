﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoDrawTest : MonoBehaviour
{
    [SerializeField]
    private float range;

    [SerializeField]
    private Transform objectToDrawFrom;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, range);
        Gizmos.color = Color.red;
        Debug.Log(objectToDrawFrom.forward * range);
        Gizmos.DrawLine(objectToDrawFrom.position, objectToDrawFrom.position + (objectToDrawFrom.forward * range));
    }
}