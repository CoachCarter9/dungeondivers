﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraOpacity : MonoBehaviour
{
    [SerializeField]
    private Camera gameCamera;

    [SerializeField]
    private GameObject player;

    private Shader shaderDifuse;
    private Shader shaderTransparent;
    public float targetAlpha;
    public float time;
    private GameObject o;
    public bool mustFadeBack = false;

    // Use this for initialization
    private void Start()
    {
        //player = GameObject.FindGameObjectWithTag("Player");
        shaderDifuse = Shader.Find("Standard");
        shaderTransparent = Shader.Find("Transparent/Diffuse");

        Moba_Camera cameraController = GetComponent<Moba_Camera>();
        player = cameraController.settings.lockTargetTransform.gameObject;
        cameraController.onTargetChanged += UpdateTarget;
    }

    private void UpdateTarget(Transform t)
    {
        player = t.gameObject;
    }

    // Update is called once per frame
    private void Update()
    {
        if (player == null) return;
        RaycastHit hit;
        if (Physics.Raycast(gameCamera.transform.position, (player.transform.position - gameCamera.transform.position), out hit, 30))
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("CanHide"))
            {
                mustFadeBack = true;

                if (hit.collider.gameObject != o && o != null)
                {
                    FadeUp(o);
                }

                o = hit.collider.gameObject;

                if (o.GetComponent<Renderer>().material.shader != shaderTransparent)
                {
                    o.GetComponent<Renderer>().material.shader = shaderTransparent;
                    Color k = o.GetComponent<Renderer>().material.color;
                    k.a = 0.5f;
                    o.GetComponent<Renderer>().material.color = k;
                }

                FadeDown(o);
            }
            else
            {
                //Debug.Log("Hitting: " + hit.collider.gameObject.name + " which is not set to fading");
                if (mustFadeBack)
                {
                    mustFadeBack = false;
                    FadeUp(o);
                }
            }
        }
        //else Debug.Log("I didn't hit anything");
    }

    private void FadeUp(GameObject f)
    {
        //iTween.Stop(f);
        if (f == null)
        {
            //Debug.Log("Fade up is null");
            return;
        }
        Hashtable args = new Hashtable();
        args.Add("alpha", 1);
        args.Add("time", time);
        args.Add("oncomplete", "SetDiffuseShading");
        args.Add("oncompletetarget", gameObject);
        args.Add("oncompleteparams", f);
        iTween.FadeTo(f, args);
    }

    private void FadeDown(GameObject f)
    {
        //iTween.Stop(f);
        Hashtable args = new Hashtable();
        args.Add("alpha", 1);
        args.Add("time", time);
        iTween.FadeTo(f, iTween.Hash("alpha", targetAlpha, "time", time));
    }

    private void SetDiffuseShading(GameObject f)
    {
        if (f.GetComponent<Renderer>().material.color.a == 1)
        {
            f.GetComponent<Renderer>().material.shader = shaderDifuse;
        }
    }

    private void OnDrawGizmos()
    {
        if (player == null) return;
        Gizmos.color = Color.red;
        Gizmos.DrawRay(gameCamera.transform.position, player.transform.position - gameCamera.transform.position);
    }
}