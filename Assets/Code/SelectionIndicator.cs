﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class SelectionIndicator : MonoBehaviour
    {
        [SerializeField]
        private MeshRenderer mRend;

        public SelectionLine line;

        public List<Vector3> LinePositions
        {
            get
            {
                return line.LinePositions;
            }
        }

        public void SetToColor(Color newColor)
        {
            mRend.material.color = newColor;
        }

        public void ResetToPos(Vector3 pos)
        {
            if (DDHelpers.IsOffline())
            {
                SetToPosition(pos);
            }
            else
            {
                GetComponent<PhotonView>().RPC(
                    "SetToPosition",
                    PhotonTargets.All,
                    pos);
            }
        }

        [PunRPC]
        private void SetToPosition(Vector3 pos)
        {
            Debug.Log("Set To Position");
            transform.position = pos;
            line.ResetLineToPos(pos);
        }

        public void MoveToLocation(Vector3 locToCheck)
        {
            line.MoveToLocation(locToCheck);
        }
    }
}