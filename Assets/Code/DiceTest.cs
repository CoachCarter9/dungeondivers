﻿using System.Collections;
using UnityEngine;

public class DiceTest : MonoBehaviour
{
    private string randomColor = "black-dots";

    [SerializeField]
    private GameObject spawnPoint;

    public bool rolledDice = false;

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Dice.Clear();
            Dice.Roll("1d6", "d6-" + randomColor, spawnPoint.transform.position, Force());
            rolledDice = true;
            StartCoroutine(WaitForDice());
        }
    }

    private Vector3 Force()
    {
        Vector3 rollTarget = Vector3.zero + new Vector3(2 + 7 * Random.value, .5F + 4 * Random.value, -2 - 3 * Random.value);
        return Vector3.Lerp(transform.position, rollTarget, 1).normalized * (-35 - Random.value * 20);
    }

    private IEnumerator WaitForDice()
    {
        float timeSinceStop = 0f;
        do
        {
            yield return new WaitForSeconds(.1f);
            if (Dice.hasMovingDice == false)
                timeSinceStop += Time.deltaTime;
            else timeSinceStop = 0;
        }
        while (Dice.hasMovingDice && Dice.Value("d6") == 0 && timeSinceStop >= .25f);
        yield return new WaitForSeconds(1);
        while (Dice.Value("d6") == 0)
        {
            yield return new WaitForSeconds(.1f);
        }
        Debug.Log("Value: " + Dice.Value("d6"));
        //rolledDice = false;
    }
}