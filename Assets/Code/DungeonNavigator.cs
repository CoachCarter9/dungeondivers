﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class DungeonNavigator : MonoBehaviour
    {
        private RoomTile currentTile;

        [SerializeField]
        private SelectionIndicator selectionIndicator;

        [SerializeField]
        private Text roomEventLabel;

        public Vector3 selectionPosition
        {
            get { return selectionIndicator.transform.position; }
        }

        public List<Vector3> MovePath
        {
            get { return selectionIndicator.LinePositions; }
        }

        public void ResetSelectionToPosition(Vector3 pos)
        {
            selectionIndicator.ResetToPos(pos);
        }

        public bool canMove = true;
        private List<Vector3> moveLocations = new List<Vector3>();

        // Use this for initialization
        private void Start()
        {
            currentTile = DungeonPlacementManager.GetTileAt(GameController.StartingPos);
            DungeonPlacementManager.Instance.onConfirmedTile += SetSelectionToTile;
        }

        public void HandleLocInDirection(RoomTile.ConnectorDirection direction)
        {
            if (moveLocations.Contains(selectionIndicator.transform.position + DungeonPlacementManager.tileOffsets[direction]) == false)
            {
                throw new Exception("Move locations does not contain position: " + selectionIndicator.transform.position + DungeonPlacementManager.tileOffsets[direction]);
            }
            if (currentTile != null)
            {
                if (currentTile.connectorStatus[direction] == RoomTile.ConnectorState.Unavailable)
                {
                    Debug.Log("Direction is unavailable: " + direction.ToString());
                    return;
                }
            }
            Vector3 locToCheck = selectionIndicator.transform.position + DungeonPlacementManager.tileOffsets[direction];
            selectionIndicator.MoveToLocation(locToCheck);
            RoomTile tile = DungeonPlacementManager.GetTileAt(locToCheck);
            if (tile != null && tile.connectorStatus[GetOppositeDirection(direction)] != RoomTile.ConnectorState.Unavailable)
            {
                SetSelectionToTile(tile);
            }
            else if (currentTile != null)
            {
                if (currentTile.connectorStatus[direction] == RoomTile.ConnectorState.Free)
                    MoveToPlacementLoc(locToCheck);
            }
        }

        private bool ConnectOrFreeInDirection(RoomTile tile, RoomTile.ConnectorDirection direction)
        {
            return tile.connectorStatus[direction] == RoomTile.ConnectorState.Free || tile.connectorStatus[direction] == RoomTile.ConnectorState.Occupied;
        }

        private void SetSelectionToTile(RoomTile tile)
        {
            string eventLabelText = tile.REvent.EventName + "\n" + tile.REvent.TrapType.ToString();
            roomEventLabel.text = eventLabelText;
            UpdateIndicatorByEventStatus(tile.REvent);
            selectionIndicator.transform.position = tile.transform.position;
            currentTile = tile;
        }

        private void UpdateIndicatorByEventStatus(RoomEvent roomEvent)
        {
            selectionIndicator.SetToColor(roomEvent.StatusColor);
        }

        private void MoveToPlacementLoc(Vector3 pos)
        {
            selectionIndicator.SetToColor(DDConsts.PlacingTileColor);
            selectionIndicator.transform.position = pos;
            currentTile = null;
        }

        private RoomTile.ConnectorDirection GetOppositeDirection(RoomTile.ConnectorDirection direction)
        {
            switch (direction)
            {
                case RoomTile.ConnectorDirection.North:
                    return RoomTile.ConnectorDirection.South;

                case RoomTile.ConnectorDirection.South:
                    return RoomTile.ConnectorDirection.North;

                case RoomTile.ConnectorDirection.West:
                    return RoomTile.ConnectorDirection.East;

                case RoomTile.ConnectorDirection.East:
                    return RoomTile.ConnectorDirection.West;

                default:
                    return RoomTile.ConnectorDirection.North;
            }
        }

        public void ReselectTile()
        {
            if (currentTile != null)
                SetSelectionToTile(currentTile);
        }

        public void SelectTile(RoomTile tileToSelect)
        {
            SetSelectionToTile(tileToSelect);
        }

        public void SelectPlayerTile(Player activePlayer)
        {
            if (activePlayer.Tile == null)
                activePlayer.Tile = DungeonPlacementManager.GetTileAt(activePlayer.CharacterModel.transform.position);
            SetSelectionToTile(activePlayer.Tile);
        }

        public void UpdateMovableTileList(RoomTile tile, int maxMoves)
        {
            moveLocations.Clear();
            List<RoomTile> openList = new List<RoomTile>();
            List<RoomTile> closedList = new List<RoomTile>();
            Dictionary<Vector3, int> tileCosts = new Dictionary<Vector3, int>();
            Debug.Log(tile);
            tileCosts.Add(tile.transform.position, 0);
            openList.Add(tile);
            while (openList.Count > 0)
            {
                if (tileCosts.ContainsKey(openList[0].transform.position) == false)
                {
                    Debug.Log(tileCosts.Count);
                    Debug.Log(openList.Count);
                }
                int cost = tileCosts[openList[0].transform.position];
                if (moveLocations.Contains(openList[0].transform.position) == false)
                    moveLocations.Add(openList[0].transform.position);
                for (int i = 0; i < openList[0].connectorStatus.Count; i++)
                {
                    foreach (KeyValuePair<RoomTile.ConnectorDirection, RoomTile.ConnectorState> connector in openList[0].connectorStatus)
                    {
                        Vector3 newLoc = openList[0].transform.position + DungeonPlacementManager.tileOffsets[connector.Key];
                        if (connector.Value == RoomTile.ConnectorState.Free && cost < maxMoves)
                        {
                            moveLocations.Add(newLoc);
                            UpdateCostForLocation(tileCosts, cost, newLoc);
                        }
                        else if (connector.Value == RoomTile.ConnectorState.Occupied && cost < maxMoves)
                        {
                            RoomTile nextTile = DungeonPlacementManager.GetTileAt(newLoc);
                            if (openList.Contains(nextTile) == false && closedList.Contains(nextTile) == false)
                            {
                                UpdateCostForLocation(tileCosts, cost, newLoc);
                                openList.Add(nextTile);
                            }
                        }
                    }
                }
                closedList.Add(openList[0]);
                openList.Remove(openList[0]);
            }
            DebugMoveLocations();
        }

        private static void UpdateCostForLocation(Dictionary<Vector3, int> tileCosts, int cost, Vector3 newLoc)
        {
            if (tileCosts.ContainsKey(newLoc))
            {
                if (tileCosts[newLoc] > cost + 1)
                {
                    tileCosts[newLoc] = cost + 1;
                }
            }
            else tileCosts.Add(newLoc, cost + 1);
        }

        private void DebugMoveLocations()
        {
            Debug.Log("Possible move locations: " + moveLocations.Count);
            for (int i = 0; i < moveLocations.Count; i++)
            {
                //Debug.Log(moveLocations[i]);
            }
        }
    }
}