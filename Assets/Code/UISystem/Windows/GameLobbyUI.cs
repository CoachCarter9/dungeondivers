using Photon;
using System;
using System.Collections.Generic;
using UISystem;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class GameLobbyUI : ADisplayWindow
    {
        private Dictionary<Text, bool> usedSlots = new Dictionary<Text, bool>();

        [Tooltip("Parent GameObject to name displays")]
        [SerializeField]
        private Transform playerDisplayContainer;

        private Text[] playerNameDisplays;

        [SerializeField]
        private GameObject startGameBtn;

        private void Awake()
        {
            playerNameDisplays = playerDisplayContainer.GetComponentsInChildren<Text>();
            RefreshRoomInfo();
            //AutoGenerate number of slots based on max players per room
        }

        private void RefreshRoomInfo()
        {
            startGameBtn.SetActive(PhotonNetwork.isMasterClient);
            UpdatePlayerNames();
        }

        protected override void OpenParams()
        {
            UpdatePlayerNames();
        }

        private void UpdatePlayerNames()
        {
            usedSlots.Clear();
            PhotonPlayer[] playersInRoom = PhotonNetwork.playerList;
            for (int i = 0; i < playerNameDisplays.Length; i++)
            {
                if (i < playersInRoom.Length)
                {
                    playerNameDisplays[i].text = playersInRoom[i].NickName;
                    usedSlots.Add(playerNameDisplays[i], true);
                }
                else
                {
                    usedSlots.Add(playerNameDisplays[i], false);
                    playerNameDisplays[i].text = string.Empty;
                }
            }
        }

        private void OnPhotonPlayerConnected(PhotonPlayer player)
        {
            Debug.Log(player.NickName + " joined the room");
            UpdateNextPlayerSlot(player.NickName);
        }

        private void UpdateNextPlayerSlot(string playerName)
        {
            Text playerSlot = GetAvailableNameSlot();
            playerSlot.text = playerName;
            usedSlots[playerSlot] = true;
        }

        private Text GetAvailableNameSlot()
        {
            foreach (KeyValuePair<Text, bool> slot in usedSlots)
            {
                if (usedSlots[slot.Key] == false)
                    return slot.Key;
            }
            throw new Exception("Too many players for this game room. Check photon settings for max players");
        }

        private void OnPhotonPlayerDisconnected(PhotonPlayer other)
        {
            Text playerSlot = GetSlotForPlayer(other.NickName);
            playerSlot.text = string.Empty;
            usedSlots[playerSlot] = false;
            RefreshRoomInfo();
        }

        private Text GetSlotForPlayer(string playerName)
        {
            for (int i = 0; i < playerNameDisplays.Length; i++)
            {
                if (playerNameDisplays[i].text == playerName)
                    return playerNameDisplays[i];
            }
            throw new Exception(string.Format("No text field contains {0}. Was {0} ever really in the game?", playerName));
        }

        public void ReturnToMainMenu()
        {
            PhotonNetwork.LeaveRoom();
            startGameBtn.SetActive(false);
            Close();
            uiMan.OpenUIThroughDisplayQueue(WindowDisplays.MainMenuUI);
        }

        public void StartGame()
        {
            PhotonNetwork.room.IsOpen = false;
            PhotonNetwork.LoadLevel(DDConsts.MPLevel);
        }
    }
}