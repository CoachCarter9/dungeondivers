using System;
using UISystem;
using UnityEngine;

namespace DungeonDivers
{
    public class GameTypesUI : ADisplayWindow
    {
        public delegate void OnPlayModeSelected(DDPhotonGameManager.PlayModeType playMode);
        public OnPlayModeSelected onPlayModeSelected;

        public void OnSelectedLocal()
        {
            SelectPlayMode(DDPhotonGameManager.PlayModeType.Local_Game);
        }

        public void OnSelectedOnline()
        {
            SelectPlayMode(DDPhotonGameManager.PlayModeType.Online_Game);
        }

        public void OnSelectedHostGame()
        {
            SelectPlayMode(DDPhotonGameManager.PlayModeType.Host_Game);
        }

        public void OnSelectedJoinPrivate()
        {
            SelectPlayMode(DDPhotonGameManager.PlayModeType.Join_Game);
        }

        private void SelectPlayMode(DDPhotonGameManager.PlayModeType modeType)
        {
            if (onPlayModeSelected != null)
                onPlayModeSelected(modeType);
            Close();
        }
    }
}