using System;
using UISystem;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class PlayerInfoUI : ADisplayWindow
    {
        [SerializeField]
        private Text scoreLabel;

        [SerializeField]
        private Text actionsLabel;

        [SerializeField]
        private Text healthLabel;

        [SerializeField]
        private Text strengthLabel;

        [SerializeField]
        private Text dexLabel;

        [SerializeField]
        private Text intellectLabel;

        [SerializeField]
        private Text constitutionLabel;

        [SerializeField]
        private Text goldLabel;

        [SerializeField]
        private InventoryDisplay inventoryDisplay;

        public void UpdateWithPlayer(Player player)
        {
            scoreLabel.text = player.Score.ToString();
            actionsLabel.text = player.RemainingActions.ToString();
            healthLabel.text = player.Health.ToString();
            strengthLabel.text = player.GetStatValueOfType(Player.PlayerStat.StatType.STR).ToString();
            dexLabel.text = player.GetStatValueOfType(Player.PlayerStat.StatType.DEX).ToString();
            intellectLabel.text = player.GetStatValueOfType(Player.PlayerStat.StatType.INT).ToString();
            constitutionLabel.text = player.GetStatValueOfType(Player.PlayerStat.StatType.CON).ToString();
            goldLabel.text = player.Gold.ToString();
            inventoryDisplay.ShowInventory(player.PlayerInventory);
        }
    }
}