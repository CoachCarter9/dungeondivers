using System;
using UISystem;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class InputStringUI : ADisplayWindow
    {
        [Tooltip("Text field for displaying window context")]
        [SerializeField]
        private Text contextString;

        public delegate void OnFinishedInput(string input);
        public OnFinishedInput onFinishedInput;

        [SerializeField]
        private InputField inputText;

        public void SetContextString(string message)
        {
            contextString.text = message;
        }

        public void Continue()
        {
            if (onFinishedInput != null)
                onFinishedInput(inputText.text);
            Close();
        }
    }
}