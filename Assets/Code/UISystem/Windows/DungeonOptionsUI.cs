using System;
using UISystem;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UISystem
{
    public class DungeonOptionsUI : ADisplayWindow
    {
        public void QuitGame()
        {
            if (PhotonNetwork.connected)
            {
                QuitMultiplayer();
            }
            else
            {
                ReturnToMainMenu();
            }
        }

        private void QuitMultiplayer()
        {
            PhotonNetwork.LeaveRoom();
        }

        private void OnLeftRoom()
        {
            ReturnToMainMenu();
        }

        private void ReturnToMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}