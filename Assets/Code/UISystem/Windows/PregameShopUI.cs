using System;
using System.Collections.Generic;
using UISystem;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class PregameShopUI : ADisplayWindow, PurchaseManager.iShop
    {
        private ItemFactory itemFactory;

        [SerializeField]
        private InventoryDisplay shopInvDisplay;

        [SerializeField]
        private InventoryDisplay playerInvDisplay;

        [SerializeField]
        private PlayerColorChanger pColorChanger;

        [SerializeField]
        private Text goldLabel;

        private Inventory shopInventory;

        private int currentPlayer = 0;

        private void Start()
        {
            shopInventory = new Inventory();
            itemFactory = new ItemFactory();
            shopInventory.AddNewItem(itemFactory.GetItem("Apple"));
            shopInventory.AddNewItem(itemFactory.GetItem("Armor"));
            shopInventory.AddNewItem(itemFactory.GetItem("Helmet"));
            shopInventory.AddNewItem(itemFactory.GetItem("Boots"));
            shopInventory.AddNewItem(itemFactory.GetItem("Minor Potion of STR"));
            shopInventory.AddNewItem(itemFactory.GetItem("Minor Potion of DEX"));
            shopInventory.AddNewItem(itemFactory.GetItem("Minor Potion of INT"));
            shopInventory.AddNewItem(itemFactory.GetItem("Minor Potion of CON"));

            shopInvDisplay.ShowInventory(shopInventory);
            ShowCurrentPlayerInfo();
        }

        public void Continue()
        {
            currentPlayer++;
            if (currentPlayer >= PlayerManager.Instance.NumOfActivePlayers)
            {
                StartGame();
            }
            else ShowCurrentPlayerInfo();
        }

        private void ShowCurrentPlayerInfo()
        {
            pColorChanger.SetColor(PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayer).PColor);
            UpdatePlayerGold();
            ShowCurrentPlayerSlots();
        }

        private void UpdatePlayerGold()
        {
            goldLabel.text = PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayer).Gold.ToString();
        }

        private void ShowCurrentPlayerSlots()
        {
            Inventory playerInventory = PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayer).PlayerInventory;
            for (int i = 0; i < playerInvDisplay.transform.childCount; i++)
            {
                if (i >= playerInventory.MaxItems)
                    playerInvDisplay.transform.GetChild(i).gameObject.SetActive(false);
                else playerInvDisplay.transform.GetChild(i).gameObject.SetActive(true);
            }
            playerInvDisplay.ShowInventory(playerInventory);
        }

        private void StartGame()
        {
            Close();
            uiMan.transform.Find("CharacterCreationUI").gameObject.SetActive(false);
            uiMan.transform.Find("DungeonUI").gameObject.SetActive(true);
            GameController.Instance.StartGame();
        }

        public void PurchaseItem(Item i)
        {
            if (PurchaseManager.PurchaseItem(PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayer), i))
                ShowCurrentPlayerInfo();
        }

        public void RefundItem(Item i)
        {
            Inventory playerInventory = PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayer).PlayerInventory;
            playerInventory.RemoveItem(i);
            PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayer).Gold += i.ItemData.GoldValue;
            ShowCurrentPlayerInfo();
        }
    }
}