using System;
using UISystem;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class GameEventUI : ADisplayWindow
    {
        [SerializeField]
        private Text eventText;

        [SerializeField]
        private bool clearOnNewMessage = false;

        private void Awake()
        {
            GameEventManager.Instance.GameEventUI = this;
            eventText.text = string.Empty;
        }

        public void DisplayNewEvent(GameEvent e)
        {
            if (clearOnNewMessage) eventText.text = string.Empty;
            string currentText = eventText.text;
            //currentText = currentText.Insert(0, e.EventString + "\n");
            currentText += "\n" + e.EventString;
            eventText.text = currentText;
        }
    }
}