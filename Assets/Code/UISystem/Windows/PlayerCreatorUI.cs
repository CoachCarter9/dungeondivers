using System;
using System.Collections;
using System.Collections.Generic;
using UISystem;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class PlayerCreatorUI : ADisplayWindow
    {
        [SerializeField]
        private StatDragManager statDragMan;

        [SerializeField]
        private PlayerModelSelector modelSelector;

        [SerializeField]
        private Text errorPrompt;

        [SerializeField]
        private GameObject createPlayerBtn;

        [SerializeField]
        private GameObject buyBtn;

        private int playersCreated = 0;
        private int maxPlayers = 4;

        private void Awake()
        {
            statDragMan.onStatError += DisplayErrorMessage;
            statDragMan.onStatDropped += CheckForCorrectStats;
#if UNITY_EDITOR
            createPlayerBtn.SetActive(true);
#endif
        }

        private void CheckForCorrectStats()
        {
            StartCoroutine(WaitAndCheckStats());
        }

        private IEnumerator WaitAndCheckStats()
        {
            yield return new WaitForEndOfFrame();
            if (statDragMan.StatsAreCorrect())
            {
                createPlayerBtn.SetActive(true);
            }
        }

        public void CreatePlayer()
        {
            playersCreated++;
            if (playersCreated > maxPlayers)
            {
                DisplayErrorMessage("MAX PLAYERS REACHED");
                return;
            }
            buyBtn.SetActive(true);

            Dictionary<Player.PlayerStat.StatType, Player.PlayerStat> playerStats = statDragMan.GetStatInfo();
            Player newPlayer = new Player("Player " + playersCreated, modelSelector.CurrentMaterial, playerStats);
            PlayerManager.Instance.AddPlayer(newPlayer);
            if (PlayerManager.Instance.NumOfActivePlayers >= maxPlayers)
                GoToShop();
            else ResetForNextPlayer();
        }

        private void ResetForNextPlayer()
        {
            createPlayerBtn.SetActive(false);
            statDragMan.Reset();
            modelSelector.UsedCurrentMaterial();
        }

        public void GoToShop()
        {
            Close();
            uiMan.OpenUIThroughDisplayQueue(WindowDisplays.PregameShopUI);
        }

        private void DisplayErrorMessage(string message)
        {
            Debug.Log("Show Error: " + message);
            int errorDisplayTime = 3;
            errorPrompt.text = message;
            errorPrompt.color = new Color(errorPrompt.color.r, errorPrompt.color.g, errorPrompt.color.b, 1);
            StartCoroutine(WaitAndFadeOutMessage(errorDisplayTime));
        }

        private IEnumerator WaitAndFadeOutMessage(int waitTime)
        {
            float alpha = 1;
            yield return new WaitForSeconds(waitTime);
            Debug.Log("FadeOut: " + errorPrompt.color.a);
            while (errorPrompt.color.a > 0)
            {
                alpha -= (Time.deltaTime);
                errorPrompt.color = new Color(errorPrompt.color.r, errorPrompt.color.g, errorPrompt.color.b, alpha);
                yield return null;
            }
        }
    }
}