﻿using UnityEngine;
using UnityEngine.UI;

namespace UISystem
{
    public class ErrorWindowUI : ADisplayWindow
    {
        [SerializeField]
        private Text errorLabel;

        [SerializeField]
        private AudioSource errorAudio;

        public void ShowError(string errorMessage)
        {
            errorLabel.text = errorMessage;
        }

        private void OnEnable()
        {
            errorAudio.Play();
        }
    }
}