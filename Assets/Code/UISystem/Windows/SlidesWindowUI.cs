﻿using UnityEngine;
using UnityEngine.UI;

public class SlidesWindowUI : MonoBehaviour
{
    [SerializeField]
    private GameObject slideStatusParent;

    private Image[] slideStatusDisplays;

    [SerializeField]
    private GameObject slidesParent;

    private GameObject[] slides;
    private int currentSlideNumber = 0;

    [SerializeField]
    private GameObject nextBtn;

    [SerializeField]
    private GameObject prevBtn;

    [SerializeField]
    private GameObject slideStatusTemp;

    [SerializeField]
    private Transform slideStatusContainer;

    private bool hasInitSlides = false;

    public delegate void FinishedSlides();
    public FinishedSlides onFinishedSlides;

    public void NextSlide()
    {
        if (currentSlideNumber < slides.Length - 1)
        {
            MarkSlideAsDisabled(currentSlideNumber);
            ShowSlide(currentSlideNumber + 1);
        }
        else
        {
            FinishSlides();
        }
    }

    public void PrevSlide()
    {
        if (currentSlideNumber == 0) return;
        MarkSlideAsDisabled(currentSlideNumber);
        ShowSlide(currentSlideNumber - 1);
    }

    private void ShowSlide(int slideNumber)
    {
        if (slideNumber == slides.Length - 1) nextBtn.SetActive(false);
        else nextBtn.SetActive(true);
        if (slideNumber == 0) prevBtn.SetActive(false);
        else prevBtn.SetActive(true);

        slides[slideNumber].SetActive(true);
        slideStatusDisplays[slideNumber].color = Color.black;
        currentSlideNumber = slideNumber;
    }

    private void MarkSlideAsDisabled(int slideNumber)
    {
        Debug.Log("ResettingSlide" + slideNumber);
        slides[slideNumber].SetActive(false);
        slideStatusDisplays[slideNumber].color = Color.gray;
    }

    public void ResetToBeginning()
    {
        if (!hasInitSlides) InitSlides();
        if (currentSlideNumber != 0)
        {
            MarkSlideAsDisabled(currentSlideNumber);
        }
        ShowSlide(0);
        this.gameObject.SetActive(true);
    }

    private void InitSlides()
    {
        slides = new GameObject[slidesParent.transform.childCount];
        slideStatusDisplays = new Image[slides.Length];
        for (int i = 0; i < slidesParent.transform.childCount; i++)
        {
            slides[i] = slidesParent.transform.GetChild(i).gameObject;
        }
        for (int j = 0; j < slides.Length; j++)
        {
            GameObject newStatusDisplay = UISystem.UIManager.CreateNewItem(slideStatusTemp, slideStatusContainer);
            slideStatusDisplays[j] = newStatusDisplay.GetComponent<Image>();
            newStatusDisplay.SetActive(true);
        }
        hasInitSlides = true;
    }

    public void FinishSlides()
    {
        if (onFinishedSlides != null)
            onFinishedSlides();
    }
}