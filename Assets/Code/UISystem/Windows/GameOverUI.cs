using System;
using System.Collections.Generic;
using UISystem;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class GameOverUI : ADisplayWindow
    {
        [SerializeField]
        private Text winningText;

        public void SetWinners(List<Player> winningPlayers)
        {
            winningText.text = "WINNER:\n";
            if (winningPlayers.Count > 1)
                winningText.text = "WINNERS:\n";

            for (int i = 0; i < winningPlayers.Count; i++)
            {
                winningText.text += winningPlayers[i].Name + "\n";
            }
        }

        public void RestartGame()
        {
            SceneManager.LoadScene("CharacterCreation");
        }

        public void GoToMainMenu()
        {
            SceneManager.LoadScene("CharacterCreation");
        }
    }
}