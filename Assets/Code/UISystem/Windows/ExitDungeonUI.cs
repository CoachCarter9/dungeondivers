using System;
using UISystem;
using UnityEngine;

namespace UISystem
{
    public class ExitDungeonUI : ADisplayWindow
    {
        public delegate void OnContinueExploring();
        public OnContinueExploring onContinueExploring;

        public delegate void OnLeaveDungeon();
        public OnLeaveDungeon onLeaveDungeon;

        public void OnContinueSelected()
        {
            Debug.Log("Continue Exploring");
            if (onContinueExploring != null)
                onContinueExploring();
            Close();
        }

        public void OnExitSelected()
        {
            if (onLeaveDungeon != null)
                onLeaveDungeon();
            Close();
        }
    }
}