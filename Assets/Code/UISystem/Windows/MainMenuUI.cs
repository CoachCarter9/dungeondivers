﻿using System;
using System.Collections;
using System.Collections.Generic;
using UISystem;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonDivers
{
    public class MainMenuUI : ADisplayWindow
    {
        [HideInInspector]
        public static DDPhotonGameManager.PlayModeType playMode = DDPhotonGameManager.PlayModeType.Local_Game;

        [SerializeField]
        private Text playModeText;

        private DDPhotonGameManager photonManager;

        private InputStringUI inputNameUI;

        private void Start()
        {
            photonManager = GameObject.Find("PhotonManager").GetComponent<DDPhotonGameManager>();
            UpdatePlayMode(DDPhotonGameManager.PlayModeType.Local_Game);
        }

        public void OpenGameTypes()
        {
            GameTypesUI gameTypeUI = (GameTypesUI)uiMan.OpenUIImmediately(WindowDisplays.GameTypesUI);
            gameTypeUI.onPlayModeSelected += UpdatePlayMode;
        }

        private void UpdatePlayMode(DDPhotonGameManager.PlayModeType playMode)
        {
            playModeText.text = playMode.ToString().Replace("_", " ");
            MainMenuUI.playMode = playMode;
        }

        public void StartGame()
        {
            switch (playMode)
            {
                case DDPhotonGameManager.PlayModeType.Local_Game:
                    photonManager.StartOfflineGame();
                    break;

                case DDPhotonGameManager.PlayModeType.Online_Game:
                    photonManager.FindOnlineGame();
                    break;

                case DDPhotonGameManager.PlayModeType.Host_Game:
                    inputNameUI = (InputStringUI)uiMan.OpenUIImmediately(WindowDisplays.InputStringUI);
                    inputNameUI.SetContextString("Name your game lobby");
                    inputNameUI.onFinishedInput += OnNamedHostRoom;
                    break;

                case DDPhotonGameManager.PlayModeType.Join_Game:
                    inputNameUI = (InputStringUI)uiMan.OpenUIImmediately(WindowDisplays.InputStringUI);
                    inputNameUI.SetContextString("What room would you like to join?");
                    inputNameUI.onFinishedInput += OnInputJoinRoom;
                    break;
            }
            Close();
        }

        private void OnNamedHostRoom(string roomName)
        {
            inputNameUI.onFinishedInput -= OnNamedHostRoom;
            photonManager.HostPrivateGame(roomName);
        }

        private void OnInputJoinRoom(string roomName)
        {
            inputNameUI.onFinishedInput -= OnInputJoinRoom;
            photonManager.JoinPrivateGame(roomName);
        }
    }
}