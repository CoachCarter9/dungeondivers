using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UISystem
{
    public class UIManager : MonoBehaviour
    {
        public interface IWindow
        {
            void Open();

            void Close();
        }

        public string openOnStart = string.Empty;

        protected virtual void Start()
        {
            if (openOnStart != string.Empty)
                OpenUIThroughDisplayQueue(openOnStart);
        }

        private bool showingUI = false;

        public bool IsShowingUI()
        {
            return showingUI;
        }

        public void ShowError(string errorMessage)
        {
            ErrorWindowUI errorWindow = (ErrorWindowUI)OpenUIImmediately("ErrorWindow");
            errorWindow.ShowError(errorMessage);
        }

        protected List<ADisplayWindow> loadedWindows = new List<ADisplayWindow>();
        private List<ADisplayWindow> displayQueue = new List<ADisplayWindow>();
        private List<ADisplayWindow> openWindows = new List<ADisplayWindow>();

        public delegate void OnFinishedDestroyingWindows();
        public OnFinishedDestroyingWindows onFinishedWindowDestroy;

        public ADisplayWindow ReopenPreviousWindow(string uiPrefabName)
        {
            ADisplayWindow myWindow = GetWindow(uiPrefabName);
            return myWindow;
        }

        public ADisplayWindow OpenUIThroughDisplayQueue(string uiPrefabName)
        {
            ADisplayWindow myWindow = GetWindow(uiPrefabName);
            Resources.UnloadUnusedAssets();
            AddToDisplayQueueAndShowNext(myWindow);
            return myWindow;
        }

        public ADisplayWindow OpenUIImmediately(string uiPrefabName)
        {
            ADisplayWindow myWindow = GetWindow(uiPrefabName);
            OpenDisplayWindow(myWindow);
            return myWindow;
        }

        public ADisplayWindow CloseAllUIAndOpen(string uiPrefabName)
        {
            CloseAllWindows();
            ADisplayWindow myWindow = GetWindow(uiPrefabName);
            OpenDisplayWindow(myWindow);
            return myWindow;
        }

        private void CloseAllWindows()
        {
            for (int i = 0; i < loadedWindows.Count; i++)
            {
                if (loadedWindows[i].IsOpen())
                    loadedWindows[i].Close();
            }
        }

        public ADisplayWindow GetWindow(string uiPrefabName)
        {
            ADisplayWindow myWindow = GetExistingWindow(uiPrefabName);
            if (myWindow == null)
                myWindow = LoadNewWindow(uiPrefabName);
            return myWindow;
        }

        private ADisplayWindow GetExistingWindow(string uiPrefabName)
        {
            ADisplayWindow myWindow = null;
            for (int i = 0; i < loadedWindows.Count; i++)
            {
                if (loadedWindows[i] == null) continue;
                if (loadedWindows[i].gameObject.name == uiPrefabName)
                {
                    myWindow = loadedWindows[i];
                }
            }
            return myWindow;
        }

        private ADisplayWindow LoadNewWindow(string uiPrefabName)
        {
            ADisplayWindow myWindow;
            GameObject newWindowLoad = Resources.Load("Displays/" + uiPrefabName) as GameObject;
            if (newWindowLoad == null)
            {
                Debug.LogError(uiPrefabName + " Doesn't exist");
            }
            GameObject newWindow = Instantiate(newWindowLoad, transform.position, Quaternion.identity) as GameObject;
            newWindow.transform.parent = transform;
            newWindow.transform.localScale = Vector3.one;
            newWindow.transform.localPosition = Vector3.zero;
            RectTransform rectT = newWindow.GetComponent<RectTransform>();
            rectT.offsetMin = Vector2.zero;
            rectT.offsetMax = Vector2.zero;
            newWindow.name = uiPrefabName;
            myWindow = newWindow.GetComponent<ADisplayWindow>();
            loadedWindows.Add(myWindow);
            myWindow.SetUIManager(this);
            return myWindow;
        }

        private void AddToDisplayQueueAndShowNext(ADisplayWindow window)
        {
            if (displayQueue.Contains(window))
            {
                displayQueue.Remove(window);
                displayQueue.Insert(0, window);
            }
            else
            {
                displayQueue.Add(window);
            }
            if (!showingUI)
                ShowNextInDisplayQueue();
        }

        private void ShowNextInDisplayQueue()
        {
            CheckIfShowingUI();
            if (displayQueue.Count > 0)
            {
                for (int i = 0; i < displayQueue.Count; i++)
                {
                    if (displayQueue[i] != null)
                    {
                        OpenDisplayWindow(displayQueue[i]);
                        break;
                    }
                }
            }
        }

        private void OpenDisplayWindow(ADisplayWindow window)
        {
            window.Open();
            AddToOpenWindowsAndRemoveFromQueue(window);
        }

        private void AddToOpenWindowsAndRemoveFromQueue(ADisplayWindow window)
        {
            openWindows.Add(window);
            if (!window.excludeFromShowingCheck) showingUI = true;
            displayQueue.Remove(window);
        }

        public void DisableLoadedWindows()
        {
            for (int i = 0; i < loadedWindows.Count; i++)
            {
                loadedWindows[i].gameObject.SetActive(false);
            }
        }

        public void EnableOpenedWindows()
        {
            for (int i = 0; i < openWindows.Count; i++)
            {
                openWindows[i].gameObject.SetActive(true);
            }
        }

        public ADisplayWindow ToggleUI(string windowName)
        {
            for (int i = 0; i < loadedWindows.Count; i++)
            {
                if (loadedWindows[i].transform.name == windowName)
                {
                    return loadedWindows[i].GetComponent<ADisplayWindow>().ToggleUI();
                }
            }
            return OpenUIImmediately(windowName);
        }

        public void ClosedWindow(ADisplayWindow window)
        {
            openWindows.Remove(window);
            CheckIfShowingUI();
            Resources.UnloadUnusedAssets();
            ShowNextInDisplayQueue();
        }

        public static GameObject CreateNewItem(GameObject objectToCreate, Transform objectParent)
        {
            GameObject newItemGO = Instantiate(objectToCreate) as GameObject;
            newItemGO.transform.parent = objectParent.transform;
            newItemGO.transform.localScale = Vector3.one;
            newItemGO.transform.localPosition = Vector3.zero;
            newItemGO.name = objectToCreate.name;
            return newItemGO;
        }

        public void CloseAllOpenedWindows()
        {
            for (int i = 0; i < loadedWindows.Count; i++)
            {
                if (loadedWindows[i] != null)
                    if (loadedWindows[i].IsOpen())
                        loadedWindows[i].Close();
            }
        }

        private void CheckIfShowingUI()
        {
            foreach (ADisplayWindow windowToCheck in loadedWindows)
            {
                if (windowToCheck == null)
                    continue;
                if (windowToCheck.excludeFromShowingCheck) continue;
                if (windowToCheck.IsOpen())
                {
                    showingUI = true;
                    return;
                }
            }
            showingUI = false;
        }

        public bool IsWindowInQueue(string windowName)
        {
            for (int i = 0; i < displayQueue.Count; i++)
            {
                if (displayQueue[i].name == windowName)
                    return true;
            }
            return false;
        }

        public void DestroyedWindow(ADisplayWindow window)
        {
            loadedWindows.Remove(window);
        }

        public void DestroyLoadedWindows()
        {
            for (int i = 0; i < loadedWindows.Count; i++)
            {
                Destroy(loadedWindows[i].gameObject);
            }
            if (onFinishedWindowDestroy != null)
            {
                onFinishedWindowDestroy();
                onFinishedWindowDestroy = null;
            }
        }
    }
}