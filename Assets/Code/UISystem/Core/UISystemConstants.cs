﻿using System.Collections;
using UnityEngine;

namespace UISystem
{
    public class UISystemConstants
    {
        public const string UIPrefabPath = "/UI/Resources/Displays/";
        public const string UISystemKeysFileName = "UISystemKeys_{0}.cs";
        public const string StaticWindowKeyFormat = "public static string {0} = \"{1}\";";
        public const string RootDir = "/Code/UISystem/";
        public const string WindowsDir = "Windows/";
        public const string Namespace = "UISystem."; // if you change this, change this in the below strings as well

        public const string WindowKeyClassHeader = @"using UnityEngine;
using System;

namespace UISystem
{
    public partial class WindowDisplays
    {";

        public const string NewWindowClassTemplate = @"using UnityEngine;
using System;
using UISystem;

    public class ";

        public const string NewWindowClassSuffix = @": ADisplayWindow
    {
    }";

        public static int IndentLevel1 = 4;
        public static int IndentLevel2 = IndentLevel1 * 2;
        public static int IndentLevel3 = IndentLevel1 * 3;
        public static int IndentLevel4 = IndentLevel1 * 4;
    }
}