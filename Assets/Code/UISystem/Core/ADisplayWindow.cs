﻿using System;
using System.Collections;
using UnityEngine;

namespace UISystem
{
    public class ADisplayWindow : MonoBehaviour, UIManager.IWindow
    {
        protected UIManager uiMan;
        protected GameObject closeConfirmPopup;

        protected bool isOpen = false;

        public bool IsOpen()
        {
            return isOpen;
        }

        public bool excludeFromShowingCheck = false;

        public virtual void SetUIManager(UIManager uiM)
        {
            uiMan = uiM;
        }

        public virtual void Open()
        {
            InitAndEnableWindow();
        }

        private void InitAndEnableWindow()
        {
            this.gameObject.SetActive(true);
            OpenParams();
            isOpen = true;
        }

        protected virtual void OpenParams()
        {
        }

        protected virtual void CloseParams()
        {
        }

        public virtual void Close()
        {
            if (this == null)
                return;
            else {
                CloseAndNotifyUIMan();
            }
        }

        private void CloseAndNotifyUIMan()
        {
            CloseParams();
            isOpen = false;
            uiMan.ClosedWindow(this);
            this.gameObject.SetActive(false);
        }

        public virtual ADisplayWindow ToggleUI()
        {
            if (isOpen)
                Close();
            else {
                uiMan.OpenUIImmediately(this.name);
            }
            return this;
        }

        public void ConfirmClose()
        {
            if (closeConfirmPopup != null)
            {
                if (closeConfirmPopup.activeSelf)
                    Close();
                else closeConfirmPopup.SetActive(true);
            }
            else
                Close();
        }

        public void ClosePopup()
        {
            if (closeConfirmPopup != null)
                closeConfirmPopup.SetActive(false);
        }

        protected virtual void OnDestroy()
        {
            if (uiMan != null)
                uiMan.DestroyedWindow(this);
        }
    }
}