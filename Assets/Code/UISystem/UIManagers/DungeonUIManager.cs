﻿using System;
using System.Collections;
using System.Collections.Generic;
using UISystem;
using UnityEngine;

namespace DungeonDivers
{
    public class DungeonUIManager : UIManager, InputController.KeyObserver
    {
        public void RetrieveKeyList(List<KeyCode> keys)
        {
            if (keys.Contains(KeyCode.Escape))
            {
                OpenUIImmediately(WindowDisplays.DungeonOptionsUI);
            }
        }
    }
}