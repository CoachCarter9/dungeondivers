using System;
using UISystem;
using UnityEngine;

namespace DungeonDivers
{
    public class MainMenuUIManager : UIManager
    {
        private InputStringUI nameInputUI;

        protected override void Start()
        {
            base.Start();
            if (PlayerPrefs.HasKey(DDConsts.PPKEY_Username) == false)
            {
                nameInputUI = (InputStringUI)OpenUIImmediately(WindowDisplays.InputStringUI);
                nameInputUI.SetContextString("Input your name below");
                nameInputUI.onFinishedInput += SaveUsername;
            }
        }

        private void SaveUsername(string input)
        {
            PlayerPrefs.SetString(DDConsts.PPKEY_Username, input);
            PlayerPrefs.Save();
        }

        private void GoToGameLobby()
        {
            CloseAllOpenedWindows();
            OpenUIThroughDisplayQueue(WindowDisplays.GameLobbyUI);
        }

        public void OnJoinedRoom()
        {
            GoToGameLobby();
        }
    }
}