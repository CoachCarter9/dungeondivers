﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace UISystem
{
    public class UISystemOptions
    {
        [MenuItem("UISystem/Generate Display Windows List")]
        public static void GenStaticUIKeysClass()
        {
            string fileName = string.Format(UISystemConstants.UISystemKeysFileName, "DisplayWindows");
            Dictionary<string, string> uiWindows = GetUIWindowNames();
            StringBuilder sb = new StringBuilder();
            sb.Append(UISystemConstants.WindowKeyClassHeader);

            foreach (var windowInfo in uiWindows)
            {
                sb.Append("\n");
                sb.Append("".PadLeft(UISystemConstants.IndentLevel2));
                sb.AppendFormat(UISystemConstants.StaticWindowKeyFormat, windowInfo.Key, windowInfo.Value);
            }

            sb.Append("\n");
            sb.Append("}".PadLeft(UISystemConstants.IndentLevel1 + 1));
            sb.Append("\n");
            sb.Append("}");
            sb.Append("\n");

            WriteFile(sb, fileName);
        }

        [MenuItem("UISystem/List Display Window Names")]
        private static void DebugWindowNames()
        {
            Dictionary<string, string> uiWindows = GetUIWindowNames();
            string debugString = "";
            foreach (var windowInfo in uiWindows)
            {
                debugString += windowInfo.Value + "\n";
            }
            Debug.Log(debugString);
        }

        public static void WriteFile(StringBuilder sb, string fileName)
        {
            string fullPath = string.Empty;
            Debug.Log(fileName);
            var results = AssetDatabase.FindAssets(Path.GetFileNameWithoutExtension(fileName) + " t:Script");
            if (results != null && results.Length > 0)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(results[0]);
                Debug.Log(assetPath);
                fullPath = Path.Combine(Environment.CurrentDirectory, assetPath);
                Debug.Log("Overwriting file at: " + fullPath);
            }
            else
            {
                fullPath = Application.dataPath + UISystemConstants.RootDir + fileName;
                Debug.Log("Created file at: " + fullPath);
            }

            File.WriteAllText(fullPath, sb.ToString());
            AssetDatabase.Refresh();

            Debug.Log("Done Generating: " + fileName);
        }

        private static Dictionary<string, string> GetUIWindowNames()
        {
            Dictionary<string, string> uiWindows = new Dictionary<string, string>();
            foreach (string file in Directory.GetFiles(Application.dataPath + UISystemConstants.UIPrefabPath, "*.prefab"))
            {
                string windowName = Path.GetFileNameWithoutExtension(file);
                uiWindows.Add(FormatWindowKey(windowName), windowName);
            }
            return uiWindows;
        }

        public static string FormatWindowKey(string fileName)
        {
            fileName = fileName.Replace(" ", "_");
            return fileName;
        }

        [MenuItem("UISystem/Clear Console %&c")] // CMD + ALT + C
        private static void ClearConsole()
        {
            // This simply does "LogEntries.Clear()" the long way:
            var logEntries = System.Type.GetType("UnityEditorInternal.LogEntries,UnityEditor.dll");
            var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            clearMethod.Invoke(null, null);
        }
    }
}