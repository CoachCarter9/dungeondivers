using System;
using UnityEditor;
using UnityEngine;

namespace UISystem
{
    [InitializeOnLoadAttribute]
    public class UISystemMenuWindow : EditorWindow
    {
        private string windowName;
        private GameObject lastCreatedWindow;
        private string lastCreatedFileName;
        private string className;

        private Transform windowParent;

        private bool createClassFile = true;

        [MenuItem("UISystem/Window Manager")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(UISystemMenuWindow));
        }

        private void OnGUI()
        {
            GUILayout.Label("New Window", EditorStyles.boldLabel);

            windowName = EditorGUILayout.TextField("Window Name", windowName);
            createClassFile = EditorGUILayout.BeginToggleGroup("Create Class File", createClassFile);
            GUILayout.Label("WARNING: Creating a new class file will overwrite existing file if one exists. Only use for new windows");
            EditorGUILayout.EndToggleGroup();
            windowParent = (Transform)EditorGUILayout.ObjectField("Window Parent: ", windowParent, typeof(Transform), true);
            if (GUILayout.Button("Create New Window"))
            {
                CreateNewWindow();
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            className = EditorGUILayout.TextField("Class Name", className);
            if (GUILayout.Button("Create New Class"))
            {
                if (className == null) return;
                CreateClassFile(className);
            }

            GUILayout.Label("Running this before Unity has finished compiling will not yield intended results." + lastCreatedFileName);
            GUILayout.Label("Last Created File Name: " + lastCreatedFileName, EditorStyles.boldLabel);
            if (GUILayout.Button("Add Created Class File to Last Window"))
            {
                AddClassFile();
            }
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            if (GUILayout.Button("Generate Display Windows List"))
            {
                UISystemOptions.GenStaticUIKeysClass();
            }
        }

        private void AddClassFile()
        {
            string typeName = lastCreatedFileName.Replace(".cs", "");
            string qualifiedNameForScript = UISystemConstants.Namespace + typeName + ",Assembly-CSharp";
            var myClassType = Type.GetType(qualifiedNameForScript);
            Debug.Log(qualifiedNameForScript);
            if (myClassType != null)
            {
                if (lastCreatedWindow.GetComponent(myClassType) == null)
                    lastCreatedWindow.AddComponent(myClassType);
            }
            else Debug.Log("Class Type is Null");
        }

        private void CreateNewWindow()
        {
            lastCreatedWindow = new GameObject();
            lastCreatedWindow.name = windowName;
            lastCreatedWindow.layer = LayerMask.NameToLayer("UI");
            if (windowParent != null)
            {
                ParentWindowToObject(windowParent);
            }
            else
            {
                UIManager uiManager = FindObjectOfType<UIManager>();
                if (uiManager != null)
                {
                    ParentWindowToObject(uiManager.transform);
                }
            }
            CreateClassFile(windowName);
        }

        private void ParentWindowToObject(Transform objectToParentTo)
        {
            lastCreatedWindow.transform.parent = objectToParentTo;
            lastCreatedWindow.transform.localScale = Vector3.one;
            lastCreatedWindow.transform.localPosition = Vector3.zero;
        }

        private void CreateClassFile(string className)
        {
            string formattedKey = UISystemOptions.FormatWindowKey(className);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            string fileString = UISystemConstants.NewWindowClassTemplate + formattedKey + UISystemConstants.NewWindowClassSuffix;
            sb.Append(fileString);

            lastCreatedFileName = formattedKey + ".cs";
            if (createClassFile)
            {
                UISystemOptions.WriteFile(sb, UISystemConstants.WindowsDir + lastCreatedFileName);
            }
        }
    }
}