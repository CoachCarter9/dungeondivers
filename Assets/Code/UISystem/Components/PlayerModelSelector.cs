﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class PlayerModelSelector : MonoBehaviour
    {
        [SerializeField]
        private PlayerColorChanger pColorChanger;

        [SerializeField]
        private List<Player.PlayerColor> playerMaterials = new List<Player.PlayerColor>();

        private int currentMaterial = 0;
        public Player.PlayerColor CurrentMaterial { get { return playerMaterials[currentMaterial]; } }

        public void UsedCurrentMaterial()
        {
            playerMaterials.Remove(playerMaterials[currentMaterial]);
            currentMaterial = 0;
            AssignMatToModel();
        }

        public void AssignNextMat()
        {
            if (currentMaterial == playerMaterials.Count - 1) currentMaterial = 0;
            else currentMaterial++;
            AssignMatToModel();
        }

        public void AssignPreviousMat()
        {
            currentMaterial--;
            if ((int)currentMaterial <= 0) currentMaterial = playerMaterials.Count - 1;
            AssignMatToModel();
        }

        private void AssignMatToModel()
        {
            AssignMatToModel(playerMaterials[currentMaterial]);
        }

        private void AssignMatToModel(Player.PlayerColor matToAssign)
        {
            pColorChanger.SetColor(matToAssign);
        }
    }
}