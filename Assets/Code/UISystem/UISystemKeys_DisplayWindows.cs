using UnityEngine;
using System;

namespace UISystem
{
    public partial class WindowDisplays
    {
        public static string DungeonOptionsUI = "DungeonOptionsUI";
        public static string ExitDungeonUI = "ExitDungeonUI";
        public static string GameLobbyUI = "GameLobbyUI";
        public static string GameOverUI = "GameOverUI";
        public static string GameTypesUI = "GameTypesUI";
        public static string HeroSelectUI = "HeroSelectUI";
        public static string InputStringUI = "InputStringUI";
        public static string MainMenuUI = "MainMenuUI";
        public static string PlayerCreatorUI = "PlayerCreatorUI";
        public static string PregameShopUI = "PregameShopUI";
    }
}