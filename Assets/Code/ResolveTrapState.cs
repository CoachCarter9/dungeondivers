﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class ResolveTrapState : GameController.GameState
    {
        public delegate void OnTrapFinished(bool passed);
        public OnTrapFinished onTrapFinished;

        public ResolveTrapState(DungeonNavigator dungeonNavigator)
        {
            this.dungeonNavigator = dungeonNavigator;
        }

        public void InitState()
        {
            GameController.Instance.ActivePlayer.UsedAction();
            TriggerRoomEvent(GameController.Instance.ActivePlayer.Tile);
        }

        private DungeonNavigator dungeonNavigator;

        private void TriggerRoomEvent(RoomTile tile)
        {
            dungeonNavigator.canMove = false;
            tile.REvent.onRoomEventComplete += FinishedTrap;
            tile.REvent.ActivateTrap(GameController.Instance.ActivePlayer);
        }

        private void FinishedTrap(bool passed)
        {
            Dice.Clear();
            if (onTrapFinished != null)
                onTrapFinished(passed);
            GameController.Instance.UpdateCurrentPlayerDisplay();
        }

        public void PressedKey(List<KeyCode> keys)
        {
        }
    }
}