﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class HotseatGameController : GameController
    {
        [SerializeField]
        private GameObject gameCamera;

        private void Start()
        {
            Debug.Log("Hotseat Start");
            PhotonNetwork.offlineMode = true;
            PhotonNetwork.Disconnect();
        }

        public override void StartGame()
        {
            gameCamera.SetActive(true);
            base.StartGame();
        }

        protected override void InitPlayers()
        {
            PlayerManager.Instance.onPlayerExitedDungeon += CheckForWinner;
            LoadCharacters();
            activePlayer = PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayerTurn);
        }

        private void LoadCharacters()
        {
            if (PlayerManager.Instance.Players.Count == 0)
            {
                PlayerManager.Instance.AutoGenerateCharacter();
            }
            for (int i = 0; i < PlayerManager.Instance.Players.Count; i++)
            {
                PlayerManager.Instance.LoadPlayerModelForPlayer(PlayerManager.Instance.Players[i], "DungeonDiver");
                PlayerManager.Instance.Players[i].Tile = DungeonPlacementManager.GetTileAt(StartingPos);
            }
        }

        protected void CheckForWinner(Player player)
        {
            if (PlayerManager.Instance.NumOfActivePlayers > 0)
                FinishTurn();
            else DetermineWinner();
        }
    }
}