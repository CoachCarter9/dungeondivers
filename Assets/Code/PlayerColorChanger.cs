﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class PlayerColorChanger : MonoBehaviour
    {
        [Serializable]
        private class UnitColor
        {
            public Player.PlayerColor colorName;
            public ColorObject[] objsToColor;
        }

        [Serializable]
        private class ColorObject
        {
            [SerializeField]
            private Material colorMat;

            [SerializeField]
            private Renderer renderer;

            public void SetObjectToMat()
            {
                //TODO MEMORY CLEANUP
                renderer.material = colorMat;
            }
        }

        [SerializeField]
        private UnitColor[] unitColors;

        private Dictionary<Player.PlayerColor, UnitColor> colorDic = new Dictionary<Player.PlayerColor, UnitColor>();

        private void Awake()
        {
            PopulateColorDic();
        }

        private void PopulateColorDic()
        {
            for (int i = 0; i < unitColors.Length; i++)
            {
                colorDic.Add(unitColors[i].colorName, unitColors[i]);
            }
        }

        public void SetColor(Player.PlayerColor playerColor)
        {
            if (colorDic.ContainsKey(playerColor) == false)
                Debug.LogError(gameObject.name + " is missing a color setup for " + playerColor.ToString());

            UnitColor uc = colorDic[playerColor];
            for (int i = 0; i < uc.objsToColor.Length; i++)
            {
                uc.objsToColor[i].SetObjectToMat();
            }
        }
    }
}