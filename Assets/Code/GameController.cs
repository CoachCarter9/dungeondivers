﻿using System;
using System.Collections;
using System.Collections.Generic;
using UISystem;
using UnityEngine;

namespace DungeonDivers
{
    public abstract class GameController : MonoBehaviour, InputController.KeyObserver
    {
        public static GameController Instance
        {
            get { return instance; }
        }

        private static GameController instance;

        public interface GameState
        {
            void PressedKey(List<KeyCode> keys);

            void InitState();
        }

        protected NavigatingDungeonState navigatingDungeonState;
        public NavigatingDungeonState NavigatingDungeonState { get { return navigatingDungeonState; } }

        protected PlacingTileState placingTileState;
        public PlacingTileState PlacingTileState { get { return placingTileState; } }

        protected MovingPlayerState movingPlayerState;
        public MovingPlayerState MovingPlayerState { get { return movingPlayerState; } }

        protected ResolveTrapState resolveTrapState;
        public ResolveTrapState ResolveTrapState { get { return resolveTrapState; } }

        public void SwitchToGameState(GameState state)
        {
            //Debug.Log("Switching to " + state.GetType().ToString());
            currentState = state;
            currentState.InitState();
        }

        protected GameState currentState;

        protected DungeonTileManager dungeonTileMan;
        protected DungeonNavigator dungeonNavigator;

        private InputController inputCon;

        [SerializeField]
        protected PlayerInfoUI playerInfoUI;

        protected Player activePlayer;
        public Player ActivePlayer { get { return activePlayer; } }

        protected int currentPlayerTurn = 0;

        [SerializeField]
        private bool debugGameState = false;

        public static Vector3 StartingPos = Vector3.zero;

        public ItemFactory ItemFactory { get { return itemFactory; } }

        protected ItemFactory itemFactory;

        public static RoomTile ExitDungeonTile;

        private void Update()
        {
            if (debugGameState)
            {
                Debug.Log(currentState.GetType().ToString());
                debugGameState = false;
            }
        }

        private void Awake()
        {
            instance = this;
            InitGameValues();
        }

        private void InitGameValues()
        {
            StartingPos = new Vector3(0, 0, DungeonPlacementManager.tileSize);
#if UNITY_STANDALONE
            inputCon = gameObject.AddComponent<PCInputController>();
            inputCon.AddKeyObserver(this);
#endif
            itemFactory = new ItemFactory();
            ScriptableObject.CreateInstance<PlayerVFXSpawner>();
            dungeonTileMan = GetComponent<DungeonTileManager>();
            dungeonNavigator = GetComponent<DungeonNavigator>();
            InitGameStates();
        }

        public virtual void StartGame()
        {
            if (DDHelpers.IsOfflineOrMasterClient())
            {
                ScriptableObject.CreateInstance<DDFloatText>();
                dungeonTileMan.CreateTileList();
                InitDungeon();
                InitPlayers();
                if (activePlayer.Tile == null) activePlayer.Tile = DungeonPlacementManager.GetTileAt(StartingPos);
                dungeonNavigator.UpdateMovableTileList(activePlayer.Tile, activePlayer.GetStatValueOfType(Player.PlayerStat.StatType.DEX));
                UpdateCurrentPlayerDisplay();
            }
        }

        protected virtual void LoadCharacter(Player player, string prefab)
        {
            PlayerManager.Instance.LoadPlayerModelForPlayer(player, "DungeonDiver");
            player.Tile = DungeonPlacementManager.GetTileAt(StartingPos);
        }

        protected virtual void InitGameStates()
        {
            navigatingDungeonState = new NavigatingDungeonState(dungeonNavigator);
            placingTileState = new PlacingTileState(dungeonTileMan);
            movingPlayerState = new MovingPlayerState(dungeonNavigator);
            resolveTrapState = new ResolveTrapState(dungeonNavigator);
            resolveTrapState.onTrapFinished += FinishedTrap;
            currentState = navigatingDungeonState;
        }

        protected void FinishedTrap(bool passed)
        {
            if (passed == false || ActivePlayer.RemainingActions <= 0)
            {
                FinishTurn();
            }
            else if (movingPlayerState.HasFinishedPath == false)
            {
                SwitchToGameState(movingPlayerState);
            }
            else SwitchToGameState(navigatingDungeonState);
        }

        protected virtual void InitPlayers()
        {
            activePlayer = PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayerTurn);
        }

        protected void InitDungeon()
        {
            SpawnEntranceTiles();
            dungeonNavigator.ResetSelectionToPosition(StartingPos);
        }

        protected void SpawnEntranceTiles()
        {
            RoomTile entranceTile = dungeonTileMan.GetEntranceTile();
            ExitDungeonTile = entranceTile;
            DungeonPlacementManager.Instance.PlaceTile(Vector3.zero, entranceTile);
            DungeonPlacementManager.Instance.ConfirmTilePlacement();

            RoomTile firstTile = dungeonTileMan.GetEntranceConnector();
            DungeonPlacementManager.Instance.PlaceTile(new Vector3(0, 0, DungeonPlacementManager.tileSize), firstTile);
            DungeonPlacementManager.Instance.ConfirmTilePlacement();
        }

        public void RetrieveKeyList(List<KeyCode> keys)
        {
            currentState.PressedKey(keys);
        }

        public virtual void FinishTurn()
        {
            if (currentState != resolveTrapState) return;
            MoveToNextPlayer();
        }

        protected void MoveToNextPlayer()
        {
            currentPlayerTurn++;
            if (currentPlayerTurn >= PlayerManager.Instance.NumOfActivePlayers)
                currentPlayerTurn = 0;

            try
            {
                activePlayer = PlayerManager.Instance.GetActivePlayerAtIndex(currentPlayerTurn);
            }
            catch (Exception e)
            {
                //Move to all players lost screen
                //TODO this will have to change with MP
            }
            ResetCurrentPlayer();
        }

        protected void ResetCurrentPlayer()
        {
            activePlayer.ResetForNextTurn();
            playerInfoUI.UpdateWithPlayer(activePlayer);
            SwitchToGameState(navigatingDungeonState);
        }

        public void UpdateCurrentPlayerDisplay()
        {
            playerInfoUI.UpdateWithPlayer(activePlayer);
        }

        protected void DetermineWinner()
        {
            List<Player> winningPlayers = new List<Player>();
            winningPlayers.Add(PlayerManager.Instance.Players[0]);
            for (int i = 1; i < PlayerManager.Instance.Players.Count; i++)
            {
                if (PlayerManager.Instance.Players[i].Score > winningPlayers[0].Score)
                {
                    winningPlayers.Clear();
                    winningPlayers.Add(PlayerManager.Instance.Players[i]);
                }
                else if (PlayerManager.Instance.Players[i].Score == winningPlayers[0].Score)
                {
                    winningPlayers.Add(PlayerManager.Instance.Players[i]);
                }
            }

            ((GameOverUI)GameObject.Find("UI").GetComponent<UIManager>().OpenUIImmediately(WindowDisplays.GameOverUI)).SetWinners(winningPlayers);
        }
    }

    public class TurnData
    {
        private List<PlayerAction> actionsDuringTurn = new List<PlayerAction>();

        public void AddNewAction(PlayerAction action)
        {
            actionsDuringTurn.Add(action);
        }
    }

    public class PlayerAction
    {
        private Vector3 moveLoc;
        private bool RoomEventPassed;
        //private DiceResolution diceResolution;
    }
}