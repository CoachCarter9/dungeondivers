﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFader : MonoBehaviour
{
    private Transform target;

    [SerializeField]
    private Camera gameCamera;

    private void Start()
    {
        Moba_Camera cameraController = GetComponent<Moba_Camera>();
        target = cameraController.settings.lockTargetTransform;
        //cameraController.onTargetChanged += UpdateTarget;
    }

    private void UpdateTarget(Transform t)
    {
        target = t;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }

        Debug.DrawRay(gameCamera.transform.position, (target.transform.position - gameCamera.transform.position) * 30, Color.red, 5);
        RaycastHit hit;

        //if (Physics.Raycast(gameCamera.transform.position, (target.transform.position - gameCamera.transform.position), out hit, 30))
        //{
        if (Physics.Linecast(gameCamera.transform.position, target.transform.position, out hit))
        {
            Debug.Log("!!!!!!!!!! - I hit: " + hit.collider.gameObject + "- !!!!!!!!!!");
        }
        else Debug.Log("I didn't hit anything");

        //if (hit.collider.gameObject != null && hit.collider.gameObject != target.gameObject)
        //{
        //}
    }
}