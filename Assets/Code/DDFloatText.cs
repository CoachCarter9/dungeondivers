﻿using KatalystKreations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class DDFloatText : ScriptableObject
    {
        private void Awake()
        {
            strStatUI = GameObject.Find("Strength_DisplayLabel").transform;
            dexStatUI = GameObject.Find("Dexterity_DisplayLabel").transform;
            intStatUI = GameObject.Find("Intellect_DisplayLabel").transform;
            conStatUI = GameObject.Find("Constitution_DisplayLabel").transform;
        }

        private static Transform strStatUI;
        private static Transform dexStatUI;
        private static Transform intStatUI;
        private static Transform conStatUI;

        public static void ShowPlayerDamage(Transform playerModel, int amount)
        {
            FloatingText.Spawn(playerModel, amount.ToString(), FloatingTextType.Damage);
        }

        public static void ShowPlayerHeal(Transform playerModel, int amount)
        {
            FloatingText.Spawn(playerModel, amount.ToString(), FloatingTextType.Heal);
        }

        public static void ShowStatIncrease(int amount, Player.PlayerStat.StatType type)
        {
            FloatingTextType textType;
            Transform statDisplayUI;
            SetFloatTextToStat(type, out textType, out statDisplayUI);

            FloatingText.Spawn(statDisplayUI.transform.position, string.Format("+{0}", amount), textType);
            FloatingText.Spawn(GameController.Instance.ActivePlayer.CharacterModel.transform.position, string.Format("+{0}", amount));
            //FloatingText.Spawn(statDisplayUI.transform.position, string.Format("+{0}{1}", amount, type), textType);
        }

        public static void ShowStatDecrease(int amount, Player.PlayerStat.StatType type)
        {
            FloatingTextType textType;
            Transform statDisplayUI;
            SetFloatTextToStat(type, out textType, out statDisplayUI);

            FloatingText.Spawn(statDisplayUI.transform.position, string.Format("-{0}", amount), textType);
            //FloatingText.Spawn(GameController.Instance.ActivePlayer.CharacterModel.transform.position, string.Format("-{0}", amount), playerTextType);
            //FloatingText.Spawn(statDisplayUI.transform.position, string.Format("-{0}{1}", amount, type), textType);
        }

        private static void SetFloatTextToStat(Player.PlayerStat.StatType type, out FloatingTextType textType, out Transform statDisplayUI)
        {
            textType = DDConsts.GetFloatingTextTypeForStat(type);
            statDisplayUI = conStatUI;
            switch (type)
            {
                case Player.PlayerStat.StatType.STR:
                    statDisplayUI = strStatUI;
                    break;

                case Player.PlayerStat.StatType.DEX:
                    statDisplayUI = dexStatUI;
                    break;

                case Player.PlayerStat.StatType.INT:
                    statDisplayUI = intStatUI;
                    break;

                case Player.PlayerStat.StatType.CON:
                    statDisplayUI = conStatUI;
                    break;
            }
        }
    }
}