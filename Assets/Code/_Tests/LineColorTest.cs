﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineColorTest : MonoBehaviour
{
    [SerializeField]
    private LineRenderer line;

    public bool addColor;
    public bool removeColor;
    public bool resetColors;

    private Color[] colors = new Color[4]
    {
        Color.white,
        Color.green,
        Color.red,
        Color.yellow
    };

    private void Update()
    {
        if (addColor)
        {
            AddRandomColor();
            addColor = false;
        }

        if (removeColor)
        {
            RemoveLastColor();
            removeColor = false;
        }

        if (resetColors)
        {
            ResetColorGradient();
            resetColors = false;
        }
    }

    private void RemoveLastColor()
    {
        List<GradientColorKey> lineColors = new List<GradientColorKey>(line.colorGradient.colorKeys);
        lineColors.RemoveAt(lineColors.Count - 1);
        SetColorGradient(lineColors.ToArray());
    }

    private void AddRandomColor()
    {
        Color newColor = colors[UnityEngine.Random.Range(0, colors.Length)];
        List<GradientColorKey> lineColors = new List<GradientColorKey>(line.colorGradient.colorKeys);
        lineColors.Add(new GradientColorKey(newColor, 1));
        lineColors = SmoothColors(lineColors);
        SetColorGradient(lineColors.ToArray());
    }

    private List<GradientColorKey> SmoothColors(List<GradientColorKey> colorKeys)
    {
        List<GradientColorKey> smoothedColors = new List<GradientColorKey>();
        float timingInterval = 1f / colorKeys.Count;
        for (int i = 0; i < colorKeys.Count; i++)
        {
            smoothedColors.Add(new GradientColorKey(colorKeys[i].color, (i + 1) * timingInterval));
            Debug.Log("Added: " + smoothedColors[i].color + " at time " + ((i + 1) * timingInterval));
        }
        return smoothedColors;
    }

    private void SetColorGradient(GradientColorKey[] colorKeys)
    {
        float alpha = 1.0f;
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            colorKeys,
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
            );
        gradient.mode = line.colorGradient.mode;
        line.colorGradient = gradient;
        Debug.Log(line.colorGradient.colorKeys.Length);

        //float alpha = 1.0f;
        //Gradient gradient = new Gradient();
        //gradient.SetKeys(
        //    new GradientColorKey[] { new GradientColorKey(Color.green, 0.0f), new GradientColorKey(Color.red, 1.0f) },
        //    new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
        //    );
        //line.colorGradient = gradient;
    }

    private void ResetColorGradient()
    {
        List<GradientColorKey> lineColors = new List<GradientColorKey>();
        lineColors.Add(new GradientColorKey(Color.white, 0));
        SetColorGradient(lineColors.ToArray());
    }
}