﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonMPTBTest : MonoBehaviour, IPunTurnManagerCallbacks
{
    private PunTurnManager turnManager;

    private void Start()
    {
        turnManager = gameObject.AddComponent<PunTurnManager>();
        turnManager.TurnDuration = Mathf.Infinity;
    }

    private void OnGUI()
    {
        GUILayout.Label("Turn: " + turnManager.Turn);
    }

    public void EndTurn()
    {
    }

    public void OnTurnBegins(int turn)
    {
        throw new NotImplementedException();
    }

    public void OnTurnCompleted(int turn)
    {
        throw new NotImplementedException();
    }

    public void OnPlayerMove(PhotonPlayer player, int turn, object move)
    {
        throw new NotImplementedException();
    }

    public void OnPlayerFinished(PhotonPlayer player, int turn, object move)
    {
        Debug.Log(string.Format("{0} finished turn {1}", player.NickName, turn));
    }

    public void OnTurnTimeEnds(int turn)
    {
        throw new NotImplementedException();
    }
}