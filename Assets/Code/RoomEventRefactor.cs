﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class RoomEventRefactor
    {
        public RoomEventRefactor(RoomEvent.RoomTrapType trapType, Player.PlayerStat.StatType statType, bool isOneTime)
        {
        }

        public delegate void OnTrapFail();
        public OnTrapFail onTrapFail;

        public delegate void OnTrapCriticalFail();
        public OnTrapCriticalFail onTrapCriticalFail;

        public delegate void OnTrapPass();
        public OnTrapPass onTrapPass;

        public delegate void OnTrapCriticalPass();
        public OnTrapCriticalPass onTrapCriticalPass;

        public delegate void OnTrapComplete(bool passed);
        public OnTrapComplete onRoomEventComplete;
    }
}