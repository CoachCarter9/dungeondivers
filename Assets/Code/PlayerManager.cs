﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class PlayerManager : MonoBehaviour
    {
        public static PlayerManager Instance
        {
            get
            {
                if (instance == null)
                {
                    throw new Exception("No Player Manager exists. Therefore no players exist? What are you trying to do here!?");
                }
                return instance;
            }
        }

        private static PlayerManager instance;

        private List<Player> players = new List<Player>();
        public List<Player> Players
        {
            get { return players; }
        }

        private List<Player> activePlayers = new List<Player>();
        public int NumOfActivePlayers { get { return activePlayers.Count; } }

        //public List<Player> ActivePlayers { get { return activePlayers; } }

        private List<Player> exitedPlayers = new List<Player>();

        public void PlayerExittedDungeon(Player leavingPlayer)
        {
            activePlayers.Remove(leavingPlayer);
            exitedPlayers.Add(leavingPlayer);
            if (onPlayerExitedDungeon != null)
                onPlayerExitedDungeon(leavingPlayer);
        }

        public delegate void OnPlayerExitedDungeon(Player player);
        public OnPlayerExitedDungeon onPlayerExitedDungeon;

        public Player GetActivePlayerAtIndex(int index)
        {
            if (activePlayers.Count == 0)
                throw new Exception("There are no more active players in the game");
            return activePlayers[index];
        }

        public void AutoGenerateCharacter()
        {
            Dictionary<Player.PlayerStat.StatType, Player.PlayerStat> newStats = new Dictionary<Player.PlayerStat.StatType, Player.PlayerStat>();
            newStats.Add(Player.PlayerStat.StatType.STR, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.STR, Player.PlayerStat.StatTier.Strong));
            newStats.Add(Player.PlayerStat.StatType.DEX, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.DEX, Player.PlayerStat.StatTier.AboveAvg));
            newStats.Add(Player.PlayerStat.StatType.INT, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.INT, Player.PlayerStat.StatTier.BelowAvg));
            newStats.Add(Player.PlayerStat.StatType.CON, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.CON, Player.PlayerStat.StatTier.Weak));

            AutoGeneratePlayer("AutoPlayer", Player.PlayerColor.Red, newStats);
            AutoGeneratePlayer("AutoPlayer 2", Player.PlayerColor.Green, newStats);
        }

        public Dictionary<Player.PlayerStat.StatType, Player.PlayerStat> AutoGenerateStats()
        {
            Dictionary<Player.PlayerStat.StatType, Player.PlayerStat> newStats = new Dictionary<Player.PlayerStat.StatType, Player.PlayerStat>();
            newStats.Add(Player.PlayerStat.StatType.STR, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.STR, Player.PlayerStat.StatTier.Strong));
            newStats.Add(Player.PlayerStat.StatType.DEX, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.DEX, Player.PlayerStat.StatTier.AboveAvg));
            newStats.Add(Player.PlayerStat.StatType.INT, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.INT, Player.PlayerStat.StatTier.BelowAvg));
            newStats.Add(Player.PlayerStat.StatType.CON, PlayerStatFactory.CreateStat(Player.PlayerStat.StatType.CON, Player.PlayerStat.StatTier.Weak));

            return newStats;
        }

        private void AutoGeneratePlayer(string name, Player.PlayerColor playerColor, Dictionary<Player.PlayerStat.StatType, Player.PlayerStat> playerStats)
        {
            Player newPlayer = new Player(name, playerColor, playerStats);
            AddPlayer(newPlayer);
            newPlayer.GenerateStartingInventory();
        }

        private void Awake()
        {
            GameObject playerMan = GameObject.Find("PlayerManager");
            if (playerMan != gameObject)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(this);
            }
        }

        public void AddPlayer(Player player)
        {
            Debug.Log("Adding Player: " + player.Name);
            players.Add(player);
            activePlayers.Add(player);
            player.onPlayerDied += PlayerDied;
        }

        public void PlayerDied(Player player)
        {
            activePlayers.Remove(player);
            PlayerExittedDungeon(player);
        }

        private bool PlayersRemaining
        {
            get
            {
                if (players.Count > 0)
                    return true;
                else return false;
            }
        }

        public void LoadPlayerModelForPlayer(Player player, string modelName)
        {
            GameObject charLoad = Resources.Load(modelName) as GameObject;
            player.CharacterModel = Instantiate(charLoad, new Vector3(999, 999, 999), Quaternion.identity);
        }

        #region Debug

        public bool increaseStat = false;
        public bool decreaseStat = false;
        public bool takeDamage = false;

        public Player.PlayerStat.StatType statToChange;

        [SerializeField]
        private PlayerInfoUI playerInfoUI;

        [SerializeField]
        private bool debugPlayers;

        [SerializeField]
        private bool debugPhotonPlayers;

        private void Update()
        {
            if (increaseStat)
            {
                GameController.Instance.ActivePlayer.IncreaseStat(statToChange);
                increaseStat = false;
                playerInfoUI.UpdateWithPlayer(GameController.Instance.ActivePlayer);
            }

            if (decreaseStat)
            {
                GameController.Instance.ActivePlayer.DecreaseStat(statToChange);
                decreaseStat = false;
                playerInfoUI.UpdateWithPlayer(GameController.Instance.ActivePlayer);
            }

            if (takeDamage)
            {
                GameController.Instance.ActivePlayer.TakeDamage(5);
                takeDamage = false;
                playerInfoUI.UpdateWithPlayer(GameController.Instance.ActivePlayer);
            }

            if (debugPlayers)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    Debug.Log("Color: " + players[i].PColor.ToString() + " | " + players[i].Name);
                }
                debugPlayers = false;
            }

            if (debugPhotonPlayers)
            {
                for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
                {
                    Debug.Log(PhotonNetwork.playerList[i].NickName);
                }
                debugPhotonPlayers = false;
            }
        }

        #endregion Debug
    }
}