﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;

namespace DungeonDivers
{
    public class MPPhotonDebugConnection : PunBehaviour
    {
        [SerializeField]
        private PhotonLogLevel logLevel;

        [SerializeField]
        private MPGameController mpGameCon;

        private void Start()
        {
            if (PhotonNetwork.connected == false)
            {
                PhotonNetwork.autoJoinLobby = true;
                PhotonNetwork.automaticallySyncScene = true;
                PhotonNetwork.ConnectUsingSettings(DDConsts.shortVersion);
            }
        }

        public override void OnJoinedLobby()
        {
            CreateRoom("");
        }

        private void CreateRoom(string roomName)
        {
            PhotonNetwork.CreateRoom(roomName, new RoomOptions()
            {
                MaxPlayers = DDConsts.maxPlayersPerGame
            }, null);
        }

        public override void OnJoinedRoom()
        {
            mpGameCon.StartGame();
        }

        [PunRPC]
        private void StartGame()
        {
            mpGameCon.StartGame();
        }
    }
}