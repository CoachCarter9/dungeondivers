﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using ExitGames.Client.Photon;
using System;

public class PhotonCallbacks : UnityEngine.MonoBehaviour, IPunCallbacks
{
    public void OnConnectedToMaster()
    {
        throw new NotImplementedException();
    }

    public void OnConnectedToPhoton()
    {
        throw new NotImplementedException();
    }

    public void OnConnectionFail(DisconnectCause cause)
    {
        throw new NotImplementedException();
    }

    public void OnCreatedRoom()
    {
        throw new NotImplementedException();
    }

    public void OnCustomAuthenticationFailed(string debugMessage)
    {
        throw new NotImplementedException();
    }

    public void OnCustomAuthenticationResponse(Dictionary<string, object> data)
    {
        throw new NotImplementedException();
    }

    public void OnDisconnectedFromPhoton()
    {
        throw new NotImplementedException();
    }

    public void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        throw new NotImplementedException();
    }

    public void OnJoinedLobby()
    {
        throw new NotImplementedException();
    }

    public void OnJoinedRoom()
    {
        throw new NotImplementedException();
    }

    public void OnLeftLobby()
    {
        throw new NotImplementedException();
    }

    public void OnLeftRoom()
    {
        throw new NotImplementedException();
    }

    public void OnLobbyStatisticsUpdate()
    {
        throw new NotImplementedException();
    }

    public void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        throw new NotImplementedException();
    }

    public void OnOwnershipRequest(object[] viewAndPlayer)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonCreateRoomFailed(object[] codeAndMsg)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonMaxCccuReached()
    {
        throw new NotImplementedException();
    }

    public void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        throw new NotImplementedException();
    }

    public void OnReceivedRoomListUpdate()
    {
        throw new NotImplementedException();
    }

    public void OnUpdatedFriendList()
    {
        throw new NotImplementedException();
    }

    public void OnWebRpcResponse(OperationResponse response)
    {
        throw new NotImplementedException();
    }
}