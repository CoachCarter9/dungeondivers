﻿using System.Collections;
using UnityEngine;

public class UnitMover : MonoBehaviour
{
    public delegate void OnMoveCompleted(Vector3 posMovedTo);

    public OnMoveCompleted onMoveCompleted;

    private Vector3 targetPos = Vector3.zero;

    public bool moving
    {
        set
        {
            Moving = value;
            //Debug.Log("Setting Moving to: " + value);
        }
    }

    private bool Moving = false;
    private float journeyLength;

    [SerializeField]
    private float moveSpeed = 5;

    // Update is called once per frame
    private void Update()
    {
        if (Moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, moveSpeed * Time.deltaTime);
            transform.LookAt(targetPos);
            if (transform.position == targetPos)
            {
                FinishedMoving();
            }
        }
    }

    private void FinishedMoving()
    {
        //Debug.Log("Finished Moving");
        moving = false;
        if (onMoveCompleted != null)
            onMoveCompleted(targetPos);
    }

    public void Move(Vector3 tilePosToMoveTo, OnMoveCompleted callback)
    {
        targetPos = tilePosToMoveTo;
        if (transform.position == tilePosToMoveTo)
        {
            Debug.Log("Position is the same");
            moving = false;
            return;
        }
        this.onMoveCompleted = callback;
        this.moving = true;
        Debug.Log("Moving to: " + tilePosToMoveTo);
        // start moving
        journeyLength = Vector3.Distance(transform.position, tilePosToMoveTo);
    }
}