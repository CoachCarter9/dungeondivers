﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputController : MonoBehaviour
{
    public delegate void OnSelectionPressed(GameObject selection);

    public OnSelectionPressed onSelectionPressed;

    public delegate void OnTerrainHit(Vector3 pos);

    public OnTerrainHit onTerrainHit;

    protected List<KeyCode> pressedKeys = new List<KeyCode>();

    public interface KeyObserver
    {
        void RetrieveKeyList(List<KeyCode> keys);
    }

    private List<KeyObserver> keyObservers = new List<KeyObserver>();

    public void AddKeyObserver(KeyObserver observer)
    {
        keyObservers.Add(observer);
    }

    public void RemoveKeyObserver(KeyObserver observer)
    {
        keyObservers.Remove(observer);
    }

    protected void NotifyKeyObservers()
    {
        for (int i = 0; i < keyObservers.Count; i++)
        {
            keyObservers[i].RetrieveKeyList(pressedKeys);
        }
    }
}