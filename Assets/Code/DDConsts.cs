﻿using KatalystKreations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public static class DDConsts
    {
        public const string PPKEY_Username = "Username";

        public const string DungeonEntranceTileName = "DungeonEntrance";
        public static Color PlacingTileColor
        {
            get { return Color.yellow; }
        }

        public static Color DexterityColor
        {
            get { return Color.green; }
        }

        public static Color StrengthColor
        {
            get { return Color.red; }
        }

        public static Color IntelligenceColor
        {
            get { return Color.blue; }
        }

        public static Color ConstitutionColor
        {
            get { return Color.yellow; }
        }

        public static Color GetColorForStat(Player.PlayerStat.StatType statType)
        {
            switch (statType)
            {
                case Player.PlayerStat.StatType.CON:
                    return ConstitutionColor;

                case Player.PlayerStat.StatType.DEX:
                    return DexterityColor;

                case Player.PlayerStat.StatType.INT:
                    return IntelligenceColor;

                case Player.PlayerStat.StatType.STR:
                    return StrengthColor;

                default:
                    return Color.white;
            }
        }

        public static FloatingTextType GetFloatingTextTypeForStat(Player.PlayerStat.StatType type)
        {
            switch (type)
            {
                case Player.PlayerStat.StatType.STR:
                    return FloatingTextType.STR;

                case Player.PlayerStat.StatType.DEX:
                    return FloatingTextType.DEX;

                case Player.PlayerStat.StatType.INT:
                    return FloatingTextType.INT;

                case Player.PlayerStat.StatType.CON:
                    return FloatingTextType.CON;

                default:
                    return FloatingTextType.Default;
            }
        }

        public const int ExploreTileScore = 2;
        public const int ArtifactScore = 20;
        public const int DisarmTrapScore = 10;
        public const int PlayerStartingGold = 150;
        public const float DiceRollWait = 2.5f;

        public const int maxPlayersPerGame = 4;

        public static VersionNumber versionNum = new VersionNumber(0, 1, 5, 0);
        public static string shortVersion
        {
            get
            {
                return string.Format("{0}.{1}", versionNum.major, versionNum.minor);
            }
        }

        public static string version
        {
            get { return string.Format("{0}.{1}.{2}.{3}", versionNum.major, versionNum.minor, versionNum.release, versionNum.build); }
        }

        public struct VersionNumber
        {
            public VersionNumber(int major, int minor, int release, int build)
            {
                this.major = major;
                this.minor = minor;
                this.release = release;
                this.build = build;
            }

            public int major;

            public int minor;
            public int release;
            public int build;
        }

        public const string MPLevel = "Dungeon_MP";
    }
}