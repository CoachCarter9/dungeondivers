﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class SelectionLine : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer line;

        private int currentIndex = 0;

        private List<Vector3> linePositions = new List<Vector3>();

        [SerializeField]
        private float yOffset = .15f;

        public List<Vector3> LinePositions
        {
            get
            {
                List<Vector3> subOffsetPos = new List<Vector3>(linePositions);
                for (int i = 0; i < subOffsetPos.Count; i++)
                {
                    subOffsetPos[i] += new Vector3(0, -yOffset);
                    Debug.Log(string.Format("SubOffsetPos {0}: {1}", i, subOffsetPos[i]));
                }
                return subOffsetPos;
            }
        }

        private List<Color> lineColors = new List<Color>();

        private void Start()
        {
            if (line == null)
                line = GetComponent<LineRenderer>();
        }

        public void MoveToLocation(Vector3 newPos)
        {
            newPos += new Vector3(0, yOffset);
            if (linePositions.Count >= 2)
            {
                if (newPos == linePositions[linePositions.Count - 2])
                {
                    RemoveLocationFromLine(linePositions[linePositions.Count - 1]);
                }
                else AddLocationToLine(newPos);
            }
            else AddLocationToLine(newPos);
        }

        private void AddLocationToLine(Vector3 newPos)
        {
            RoomTile room = DungeonPlacementManager.GetTileAt(newPos);
            if (room == null)
                AddColorToLine(DDConsts.PlacingTileColor);
            else AddColorToLine(room.REvent.StatusColor);
            linePositions.Add(newPos);
            line.numPositions = linePositions.Count;
            line.SetPositions(linePositions.ToArray());
        }

        private void AddColorToLine(Color newGradientColor)
        {
            return;
            lineColors.Add(newGradientColor);
            SetColorGradient(CreateColorKeys(lineColors));
        }

        private GradientColorKey[] CreateColorKeys()
        {
            return CreateColorKeys(lineColors);
        }

        private GradientColorKey[] CreateColorKeys(List<Color> colorKeys)
        {
            List<GradientColorKey> smoothedColors = new List<GradientColorKey>();
            float timingInterval = 1f / colorKeys.Count;
            for (int i = 0; i < colorKeys.Count; i++)
            {
                smoothedColors.Add(new GradientColorKey(colorKeys[i], i * timingInterval));
            }
            return smoothedColors.ToArray();
        }

        private void RemoveLocationFromLine(Vector3 posToRemove)
        {
            Debug.Log("Removing pos: " + posToRemove);
            RemoveLastColorFromLine();
            linePositions.Remove(posToRemove);
            line.numPositions = linePositions.Count;
            line.SetPositions(linePositions.ToArray());
        }

        private void RemoveLastColorFromLine()
        {
            return;
            lineColors.RemoveAt(lineColors.Count - 1);
            SetColorGradient(CreateColorKeys());
        }

        public void ResetLineToPos(Vector3 pos)
        {
            pos += new Vector3(0, yOffset);
            ResetColorGradient();
            linePositions.Clear();
            AddLocationToLine(pos);
        }

        private void ResetColorGradient()
        {
            lineColors.Clear();
            SetColorGradient(CreateColorKeys());
        }

        private void SetColorGradient(GradientColorKey[] colorKeys)
        {
            float alpha = 1.0f;
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                colorKeys,
                new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                );
            gradient.mode = line.colorGradient.mode;
            line.colorGradient = gradient;
        }
    }
}