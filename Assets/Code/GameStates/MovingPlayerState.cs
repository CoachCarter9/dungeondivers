﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class MovingPlayerState : GameController.GameState
    {
        public MovingPlayerState(DungeonNavigator dungeonNavigator)
        {
            this.dungeonNavigator = dungeonNavigator;
            cameraController = Camera.main.GetComponentInParent<Moba_Camera>();
        }

        private List<Vector3> playerChosenPath = new List<Vector3>();
        private Moba_Camera cameraController;
        private float cameraTransitionRate = .02f;

        public bool HasFinishedPath
        {
            get
            {
                if (playerChosenPath.Count > 0)
                    return false;
                return true;
            }
        }

        private DungeonNavigator dungeonNavigator;

        public void InitState()
        {
            cameraController.GoToTargetView();
            MovePlayerAlongPath();
        }

        private void MovePlayerAlongPath()
        {
            if (playerChosenPath.Count == 0)
            {
                playerChosenPath = dungeonNavigator.MovePath;
            }

            if (playerChosenPath[0] == GameController.Instance.ActivePlayer.Tile.transform.position)
                playerChosenPath.RemoveAt(0);

            if (playerChosenPath.Count > 0)
                MovePlayerToNextTile();
            else GameController.Instance.SwitchToGameState(GameController.Instance.NavigatingDungeonState);
        }

        private void MovePlayerToNextTile()
        {
            MovePlayerToLoc(playerChosenPath[0]);
        }

        private void MovePlayerToLoc(Vector3 movePosition)
        {
            if (DungeonPlacementManager.GetTileAt(movePosition) == null)
            {
                PlacingTileState placingTileState = GameController.Instance.PlacingTileState;
                GameController.Instance.SwitchToGameState(placingTileState);
                placingTileState.PlaceTile(movePosition);
                dungeonNavigator.ResetSelectionToPosition(movePosition);
            }
            else
            {
                cameraController.settings.movement.lockTransitionRate = cameraTransitionRate;
                cameraController.SetTargetTransform(GameController.Instance.ActivePlayer.CharacterModel.transform);
                GameController.Instance.ActivePlayer.Move(movePosition, PlayerMovedToRoom);
            }
        }

        private void PlayerMovedToRoom(Vector3 enteredRoomPos)
        {
            GameController.Instance.ActivePlayer.Tile = DungeonPlacementManager.GetTileAt(enteredRoomPos);
            playerChosenPath.Remove(enteredRoomPos);
            GameController.Instance.SwitchToGameState(GameController.Instance.ResolveTrapState);
        }

        public void PressedKey(List<KeyCode> keys)
        {
        }

        private void DebugMovePath()
        {
            string debugString = "Move Locations: ";
            for (int i = 0; i < dungeonNavigator.MovePath.Count; i++)
            {
                debugString += dungeonNavigator.MovePath[i] + "\n";
            }
            Debug.Log(debugString);
        }
    }
}