﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class NavigatingDungeonState : GameController.GameState
    {
        public NavigatingDungeonState(DungeonNavigator dungeonNav)
        {
            this.dungeonNavigator = dungeonNav;
            cameraController = Camera.main.GetComponentInParent<Moba_Camera>();
            defaultCameraFollow = cameraController.settings.lockTargetTransform;
        }

        private DungeonNavigator dungeonNavigator;
        private Moba_Camera cameraController;
        private Transform defaultCameraFollow;
        private float cameraTransitionRate = .1f;

        public void InitState()
        {
            ResetNavigatorToPlayerPos();
            cameraController.settings.movement.lockTransitionRate = cameraTransitionRate;
            cameraController.SetTargetTransform(defaultCameraFollow);
            cameraController.GoToDungeonView();
        }

        private void ResetNavigatorToPlayerPos()
        {
            dungeonNavigator.canMove = true;
            dungeonNavigator.ReselectTile();
            dungeonNavigator.SelectPlayerTile(GameController.Instance.ActivePlayer);
            dungeonNavigator.ResetSelectionToPosition(GameController.Instance.ActivePlayer.CharacterModel.transform.position);
            dungeonNavigator.UpdateMovableTileList(GameController.Instance.ActivePlayer.Tile, GameController.Instance.ActivePlayer.RemainingActions);
        }

        public void PressedKey(List<KeyCode> keys)
        {
            if (keys.Contains(KeyCode.Space))
            {
                if (PressedOnOpenSpace())
                {
                    MovePlayerAlongPath(dungeonNavigator.selectionPosition);
                    //PlaceTileAtLocation(dungeonNavigator.selectionPosition);
                }
                else
                {
                    if (DungeonPlacementManager.GetTileAt(dungeonNavigator.selectionPosition) == GameController.Instance.ActivePlayer.Tile)
                        GameController.Instance.SwitchToGameState(GameController.Instance.ResolveTrapState);
                    else
                        MovePlayerAlongPath(DungeonPlacementManager.GetTileAt(dungeonNavigator.selectionPosition));
                }
            }

            if (keys.Contains(KeyCode.Return))
                GameController.Instance.FinishTurn();

            if (keys.Contains(KeyCode.W))
            {
                dungeonNavigator.HandleLocInDirection(RoomTile.ConnectorDirection.North);
            }
            if (keys.Contains(KeyCode.A))
            {
                dungeonNavigator.HandleLocInDirection(RoomTile.ConnectorDirection.West);
            }
            if (keys.Contains(KeyCode.S))
            {
                dungeonNavigator.HandleLocInDirection(RoomTile.ConnectorDirection.South);
            }
            if (keys.Contains(KeyCode.D))
            {
                dungeonNavigator.HandleLocInDirection(RoomTile.ConnectorDirection.East);
            }

            if (keys.Contains(KeyCode.Alpha1) || keys.Contains(KeyCode.Keypad1))
            {
                UseInventoryItem(0);
            }
            if (keys.Contains(KeyCode.Alpha2) || keys.Contains(KeyCode.Keypad2))
            {
                UseInventoryItem(1);
            }
            if (keys.Contains(KeyCode.Alpha3) || keys.Contains(KeyCode.Keypad3))
            {
                UseInventoryItem(2);
            }
            if (keys.Contains(KeyCode.Alpha4) || keys.Contains(KeyCode.Keypad4))
            {
                UseInventoryItem(3);
            }
            if (keys.Contains(KeyCode.Alpha5) || keys.Contains(KeyCode.Keypad5))
            {
                UseInventoryItem(4);
            }
            if (keys.Contains(KeyCode.Alpha6) || keys.Contains(KeyCode.Keypad6))
            {
                UseInventoryItem(5);
            }
        }

        private bool UseInventoryItem(int slot)
        {
            Item itemToUse = null;
            try
            {
                itemToUse = GameController.Instance.ActivePlayer.PlayerInventory.GetItemAtIndex(slot);
                if (ItemIsUsable(itemToUse))
                {
                    GameController.Instance.ActivePlayer.PlayerInventory.UseItem(slot);
                    GameController.Instance.ActivePlayer.UsedAction();
                    GameController.Instance.UpdateCurrentPlayerDisplay();
                }
                else Debug.Log("That item can't be used right now");
                return true;
            }
            catch (Exception err)
            {
                Debug.Log(err);
                return false;
            }
        }

        private bool ItemIsUsable(Item itemToUse)
        {
            return itemToUse.GetType() == typeof(HealingItem) || itemToUse.GetType() == typeof(StatModifierItem);
        }

        private bool PressedOnOpenSpace()
        {
            return DungeonPlacementManager.Instance.IsPlacingTile == false && DungeonPlacementManager.GetTileAt(dungeonNavigator.selectionPosition) == null;
        }

        private void PlaceTileAtLocation(Vector3 selectionPosition)
        {
            GameController.Instance.SwitchToGameState(GameController.Instance.PlacingTileState);
            GameController.Instance.PlacingTileState.PlaceTile(selectionPosition);
        }

        private void MovePlayerAlongPath(RoomTile tile)
        {
            MovePlayer();
        }

        private void MovePlayerAlongPath(Vector3 pos)
        {
            MovePlayer();
        }

        private void MovePlayer()
        {
            MovingPlayerState movePlayerState = GameController.Instance.MovingPlayerState;
            GameController.Instance.SwitchToGameState(movePlayerState);
        }
    }
}