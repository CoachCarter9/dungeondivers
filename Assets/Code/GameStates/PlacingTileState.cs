﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DungeonDivers
{
    public class PlacingTileState : GameController.GameState
    {
        public PlacingTileState(DungeonTileManager dungeonTileMan)
        {
            this.dungeonTileMan = dungeonTileMan;
            cameraController = Camera.main.GetComponentInParent<Moba_Camera>();
            defaultCameraFollow = cameraController.settings.lockTargetTransform;
        }

        private bool placingTile = false;

        private DungeonTileManager dungeonTileMan;
        private Moba_Camera cameraController;
        private float cameraTransitionRate = .05f;
        private Transform defaultCameraFollow;

        public void InitState()
        {
            DungeonPlacementManager.Instance.onConfirmedTile += ConfirmedTile;
            GamePromptManager.DisplayMessage("Press 'Return' to confirm tile placement");
            cameraController.settings.movement.lockTransitionRate = cameraTransitionRate;
            cameraController.SetTargetTransform(defaultCameraFollow);
        }

        public void PressedKey(List<KeyCode> keys)
        {
            if (keys.Contains(KeyCode.Return) || keys.Contains(KeyCode.KeypadEnter))
            {
                if (DungeonPlacementManager.Instance.IsValidPlacement())
                {
                    DungeonPlacementManager.Instance.ConfirmTilePlacement();
                }
                else Debug.Log("NOT A VALID TILE PLACEMENT");
            }

            if (keys.Contains(KeyCode.Q))
            {
                DungeonPlacementManager.Instance.RotatePlacementTile(DungeonPlacementManager.RotationDirection.Left);
            }

            if (keys.Contains(KeyCode.E))
            {
                DungeonPlacementManager.Instance.RotatePlacementTile(DungeonPlacementManager.RotationDirection.Right);
            }
        }

        public void PlaceTile(Vector3 posToPlace)
        {
            Debug.Log("Is Placing Tile: " + placingTile);
            //if (placingTile)
            //{
            //    Debug.Log("Currently Placing Tile");
            //    return;
            //}
            //placingTile = true;
            RoomTile nextTile = dungeonTileMan.GetNextTile();
            if (DungeonPlacementManager.Instance.PlaceTile(posToPlace, nextTile))
            {
                GameController.Instance.SwitchToGameState(GameController.Instance.MovingPlayerState);
            }
        }

        private void ConfirmedTile(RoomTile tile)
        {
            Debug.Log("Confirmed Tile. Placing tile is false");
            //placingTile = false;
            GameController.Instance.ActivePlayer.IncreaseScore(DDConsts.ExploreTileScore);
            GameController.Instance.SwitchToGameState(GameController.Instance.MovingPlayerState);
            DungeonPlacementManager.Instance.onConfirmedTile -= ConfirmedTile;
            GamePromptManager.ClearMessage();
        }
    }
}