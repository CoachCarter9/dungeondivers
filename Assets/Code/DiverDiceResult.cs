﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiverDiceResult : MonoBehaviour
{
    [SerializeField]
    private Transform diceContainer;

    [SerializeField]
    private Transform diceStart;

    [SerializeField]
    private float diceAnimTime = .15f;

    [System.Serializable]
    private struct DiceValueRotation
    {
        public int value;
        public Vector3 localRotation;
    }

    [SerializeField]
    private DiceValueRotation[] diceRots;

    [SerializeField]
    private GameObject[] dice;

    [System.Serializable]
    private struct DiceConfig
    {
        public int numberOfDice;
        public Vector3 dicePos;
        public GameObject[] dice;
    }

    [SerializeField]
    private DiceConfig[] configs;

    private Vector3[] diceStartPositions = new Vector3[6];

    private Dictionary<int, DiceConfig> diceConfigs = new Dictionary<int, DiceConfig>();

    private void Awake()
    {
        InitDicePositions();
        CreateDiceDictionary();
        DisableDice();
    }

    private void InitDicePositions()
    {
        for (int i = 0; i < dice.Length; i++)
        {
            diceStartPositions[i] = dice[i].transform.position;
            dice[i].transform.position = diceStart.position;
        }
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.R))
        {
            DebugDiceRoll();
        }
#endif
    }

    private void DebugDiceRoll()
    {
        int diceRoll = UnityEngine.Random.Range(1, 6);
        int[] diceValues = new int[diceRoll];
        for (int i = 0; i < diceRoll; i++)
        {
            diceValues[i] = UnityEngine.Random.Range(1, 6);
        }
        ShowDiceRoll(diceValues);
    }

    private void CreateDiceDictionary()
    {
        for (int i = 0; i < configs.Length; i++)
        {
            if (diceConfigs.ContainsKey(configs[i].numberOfDice) == false)
            {
                diceConfigs.Add(configs[i].numberOfDice, configs[i]);
            }
            else
            {
                throw new Exception("Configuration for: " + configs[i].numberOfDice + " already exists");
            }
        }
    }

    public void ShowDiceRoll(int[] values)
    {
        DiceConfig config = diceConfigs[values.Length];
        for (int i = 0; i < config.dice.Length; i++)
        {
            config.dice[i].SetActive(true);
            iTween.MoveTo(config.dice[i].transform.gameObject, diceStartPositions[(Int32.Parse(config.dice[i].name) - 1)] + config.dicePos, diceAnimTime);
            config.dice[i].transform.localEulerAngles = GetRotationForValue(values[i]);
        }
        StartCoroutine(WaitAndDisableDice());
    }

    private IEnumerator WaitAndDisableDice()
    {
        yield return new WaitForSeconds(2);
        DisableDice();
    }

    private Vector3 GetRotationForValue(int v)
    {
        for (int i = 0; i < diceRots.Length; i++)
        {
            if (diceRots[i].value == v)
                return diceRots[i].localRotation;
        }
        throw new Exception("No rotation set for value: " + v);
    }

    private void DisableDice()
    {
        for (int i = 0; i < dice.Length; i++)
        {
            iTween.MoveTo(dice[i].transform.gameObject, diceStart.position, diceAnimTime);
        }
    }
}