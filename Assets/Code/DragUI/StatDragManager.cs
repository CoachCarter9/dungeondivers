﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DungeonDivers
{
    public class StatDragManager : MonoBehaviour
    {
        [SerializeField]
        private Transform dropAreas;

        private DropSlot[] dropSlots;

        public delegate void OnStatError(string error);
        public OnStatError onStatError;

        public delegate void OnStatDropped();
        public OnStatDropped onStatDropped;

        private Vector3[] dragStartPos = new Vector3[4];
        private DragObject[] dragObjects;

        private void Start()
        {
            InitDragObjects();
            InitDropSlotArray();
        }

        private void InitDropSlotArray()
        {
            dropSlots = new DropSlot[dropAreas.childCount];
            for (int i = 0; i < dropAreas.childCount; i++)
            {
                dropSlots[i] = dropAreas.GetChild(i).GetComponent<DropSlot>();
                dropSlots[i].onDroppedObject += DroppedObjectOnSlot;
            }
        }

        private void DroppedObjectOnSlot(GameObject droppedObject, DropSlot slot)
        {
            DragObject itemToPlace = slot.listItem;
            int i = slot.transform.GetSiblingIndex();
            while (itemToPlace != null && i < dropSlots.Length && droppedObject != itemToPlace.gameObject)
            {
                GameObject placeObject = itemToPlace.gameObject;
                itemToPlace = dropSlots[i].listItem;
                dropSlots[i].SetItemInSlot(placeObject);
                i++;
                if (i == dropSlots.Length && itemToPlace != null)
                    i = 0;
            }
            if (onStatDropped != null)
                onStatDropped();
        }

        private void InitDragObjects()
        {
            dragObjects = GetComponentsInChildren<DragObject>();

            for (int i = 0; i < dragObjects.Length; i++)
            {
                dragStartPos[i] = dragObjects[i].transform.position;
                dragObjects[i].onDragStart += StartedDraggingObject;
            }
        }

        private void StartedDraggingObject(Transform dragTransform)
        {
            dragTransform.SetAsLastSibling();
            dropAreas.SetAsLastSibling();
        }

        public Dictionary<Player.PlayerStat.StatType, Player.PlayerStat> GetStatInfo()
        {
            Dictionary<Player.PlayerStat.StatType, Player.PlayerStat> newStats = new Dictionary<Player.PlayerStat.StatType, Player.PlayerStat>();
            for (int i = 0; i < dropSlots.Length; i++)
            {
                Player.PlayerStat.StatType type = (Player.PlayerStat.StatType)i;
                try
                {
                    type = (Player.PlayerStat.StatType)Enum.Parse(typeof(Player.PlayerStat.StatType), dropSlots[i].listItem.gameObject.name.Substring(0, 3).ToUpper());
                }
                catch
                {
                    if (onStatError != null)
                        onStatError("Please assign all stat values");
#if UNITY_EDITOR
                    PlayerStatFactory.CreateStat(type, (Player.PlayerStat.StatTier)i);
#else
                    throw new Exception("Stat value isn't assigned");
#endif
                }

                newStats.Add(type, PlayerStatFactory.CreateStat(type, (Player.PlayerStat.StatTier)i));
            }
            return newStats;
        }

        public void Reset()
        {
            for (int i = 0; i < dragObjects.Length; i++)
            {
                dragObjects[i].transform.position = dragStartPos[i];
            }
            for (int i = 0; i < dropSlots.Length; i++)
            {
                dropSlots[i].SetItemInSlot(null);
            }
        }

        public bool StatsAreCorrect()
        {
            for (int i = 0; i < dropSlots.Length; i++)
            {
                if (dropSlots[i].listItem == null)
                {
                    Debug.Log("Slot: " + i + " is missing an item");
                    return false;
                }
            }
            Debug.Log("Stats are correct");
            return true;
        }
    }
}