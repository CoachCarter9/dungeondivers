﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler, IDragHandler
{
    [SerializeField]
    private Image containerImage;

    public DragObject listItem;

    public delegate void OnDroppedObject(GameObject droppedObject, DropSlot slot);
    public OnDroppedObject onDroppedObject;

    public void OnDrop(PointerEventData eventData)
    {
        GameObject dragObject = eventData.pointerDrag;
        if (dragObject != null)
        {
            if (onDroppedObject != null)
                onDroppedObject(dragObject, this);
            SetItemInSlot(dragObject);
        }
    }

    public void SetItemInSlot(GameObject item)
    {
        if (item != null)
        {
            item.transform.position = transform.position;
            listItem = item.GetComponent<DragObject>();
        }
        else listItem = null;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            containerImage.color = Color.yellow;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            containerImage.color = Color.white;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (listItem == null)
            return;

        eventData.pointerDrag = listItem.gameObject;
        listItem.SetDraggedPosition(eventData);
    }
}