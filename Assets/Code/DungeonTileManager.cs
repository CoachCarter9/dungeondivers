﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;

namespace DungeonDivers
{
    public class DungeonTileManager : PunBehaviour
    {
        private string[] specialRooms = new string[8]
        {
        "StorageRoom",
        "HiddenStash",
        "Library",
        "HealthFountain",
        "Armory",
        "AlchemistsChamber",
        "TrainingRoom",
        "MessHall"
        };

        public enum DungeonTileType
        {
            FourWay = 0, TJunction, Hallway, DeadEnd, Elbow
        }

        private List<RoomTile> dungeonDeck = new List<RoomTile>();

        public void RemoveTileFromDeck(RoomTile tile)
        {
            Debug.Log("Removing tile with id: " + tile.ID + " from the deck");
            dungeonDeck.Remove(tile);
        }

        private static List<RoomTile> deckByIDs = new List<RoomTile>();
        private GameObject deckContainer;

        private List<DungeonTileType> validNormalTypes;

        private int currentTileID = 0;

        private void Awake()
        {
            InitValidTileTypes();
            deckContainer = GameObject.Find("DungeonDeckTileContainer");
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                OutputCurrentDeck();
            }
        }

        private void OutputCurrentDeck()
        {
            for (int i = 0; i < dungeonDeck.Count; i++)
            {
                //Debug.Log(string.Format(@"ID: {0} | TileIDFromIDDeck: {1}", dungeonDeck[i].ID, deckByIDs[dungeonDeck[i].ID]));
            }
        }

        private void InitValidTileTypes()
        {
            validNormalTypes = new List<DungeonTileType>();
            Array arr = Enum.GetValues(typeof(DungeonTileType));
            for (int i = 0; i < arr.Length; i++)
            {
                DungeonTileType type = (DungeonTileType)arr.GetValue(i);
                if (IsValidType(type))
                    validNormalTypes.Add(type);
            }
        }

        private bool IsValidType(DungeonTileType type)
        {
            switch (type)
            {
                case DungeonTileType.DeadEnd:
                    return false;

                default:
                    return true;
            }
        }

        public void CreateTileList()
        {
            CreateBlankRooms();
            CreateStandardTiles();
            CreateSpecialRooms();
            //ShuffleDungeonDeck(dungeonDeck);
        }

        private void CreateSpecialRooms()
        {
            for (int i = 0; i < PlayerManager.Instance.NumOfActivePlayers - 1; i++)
            {
                CreateSpecialRoom("ArtifactRoom");
            }
            for (int i = 0; i < specialRooms.Length; i++)
            {
                CreateSpecialRoom(specialRooms[i]);
            }
        }

        private void CreateBlankRooms()
        {
            int blankRoomsToCreate = 6;
            string roomEventName = "NoEvent";
            for (int i = 0; i < blankRoomsToCreate; i++)
            {
                RoomTile newRoomTile = SpawnTile();
                AddRoomEventToTile(newRoomTile, roomEventName);
                SendUpdateTileWithRoomEvent(newRoomTile, roomEventName);
            }
        }

        private void CreateStandardTiles()
        {
            int roomsToCreate = 48;
            for (int i = 0; i < roomsToCreate; i++)
            {
                RoomTile newRoomTile = SpawnTile();
                AddRoomEventToTile(newRoomTile, RoomEventFactory.GetNewRandomTrapEvent());
                SendUpdateTileWithRoomEvent(newRoomTile, newRoomTile.REvent.EventName);
            }
        }

        private void CreateSpecialRoom(string roomName)
        {
            RoomTile newRoom = SpawnTile(DungeonTileType.DeadEnd);
            newRoom.name = roomName;
            AddRoomEventToTile(newRoom, RoomEventFactory.NewRoomEventByName(roomName));
            SendUpdateTileWithRoomEvent(newRoom, roomName);
        }

        private void SendUpdateTileWithRoomEvent(RoomTile newRoomTile, string roomEventName)
        {
            if (PhotonNetwork.isMasterClient)
            {
                photonView.RPC("SetRoomEventForTile", PhotonTargets.OthersBuffered, newRoomTile.GetComponent<PhotonView>().viewID, roomEventName);
            }
        }

        [PunRPC]
        private void SetRoomEventForTile(int viewID, string roomEventName)
        {
            RoomTile rTile = PhotonView.Find(viewID).gameObject.GetComponent<RoomTile>();
            AddRoomEventToTile(rTile, roomEventName);
        }

        private void AddRoomEventToTile(RoomTile newRoomTile, string roomEventName)
        {
            AddRoomEventToTile(newRoomTile, RoomEventFactory.NewRoomEventByName(roomEventName));
        }

        private void AddRoomEventToTile(RoomTile newRoomTile, RoomEvent rEvent)
        {
            newRoomTile.REvent = rEvent;
            newRoomTile.REvent.tile = newRoomTile;
        }

        public RoomTile SpawnTile()
        {
            int randTileType = UnityEngine.Random.Range(0, validNormalTypes.Count);
            return SpawnTile(validNormalTypes[randTileType]);
        }

        public RoomTile SpawnTile(DungeonTileType randTileType)
        {
            return SpawnTile(randTileType.ToString());
        }

        private RoomTile SpawnTile(string tileName)
        {
            GameObject tileLoad = (GameObject)Resources.Load("Tiles/" + tileName);
            GameObject newTile = null;
            RoomTile roomTile = null;
            if (!PhotonNetwork.connected)
            {
                newTile = Instantiate(tileLoad, deckContainer.transform.position, Quaternion.identity);
                roomTile = InitTile(newTile);
            }
            else
            {
                newTile = PhotonNetwork.InstantiateSceneObject("Tiles/" + tileName, deckContainer.transform.position, Quaternion.identity, 0, new object[] { });
                photonView.RPC("InitTile", PhotonTargets.OthersBuffered, newTile.GetComponent<PhotonView>().viewID, currentTileID);
                roomTile = InitTile(newTile);
            }
            currentTileID++;
            return roomTile;
        }

        private RoomTile InitTile(GameObject newTile, int tileID = -1)
        {
            if (tileID == -1) tileID = currentTileID;
            RoomTile roomTile = newTile.GetComponent<RoomTile>();
            roomTile.SetID(tileID);
            newTile.transform.SetParent(deckContainer.transform);
            roomTile.UpdateTileName();
            roomTile.InitConnectors();
            dungeonDeck.Add(roomTile);
            AddTileToIDDictionary(roomTile);
            return roomTile;
        }

        [PunRPC]
        private RoomTile InitTile(int photonID, int tileID)
        {
            PhotonView tileView = PhotonView.Find(photonID);
            return InitTile(tileView.gameObject, tileID);
        }

        private void ShuffleDungeonDeck(List<RoomTile> dungeonDeck, int timesToShuffle = 3)
        {
            for (int i = 0; i < timesToShuffle; i++)
            {
                for (int n = dungeonDeck.Count - 1; n > 0; --n)
                {
                    int k = UnityEngine.Random.Range(0, dungeonDeck.Count);
                    RoomTile temp = dungeonDeck[n];
                    dungeonDeck[n] = dungeonDeck[k];
                    dungeonDeck[k] = temp;
                }
            }

            for (int i = 0; i < dungeonDeck.Count; i++)
            {
                SendTileToOtherPlayers(dungeonDeck[i]);
            }
        }

        public RoomTile GetEntranceTile()
        {
            RoomTile roomTile = SpawnTile(DDConsts.DungeonEntranceTileName);
            roomTile.gameObject.name = DDConsts.DungeonEntranceTileName;
            roomTile.REvent = RoomEventFactory.NewRoomEventByName("ExitDungeonEvent");
            dungeonDeck.Remove(roomTile);
            SendUpdateTileWithRoomEvent(roomTile, roomTile.REvent.EventName);
            return roomTile;
        }

        public RoomTile GetEntranceConnector()
        {
            RoomTile firstTile = SpawnTile(DungeonTileType.FourWay);
            firstTile.REvent = RoomEventFactory.NewRoomEventByName("NoEvent");
            firstTile.gameObject.name = "DungeonConnector";
            dungeonDeck.Remove(firstTile);
            SendUpdateTileWithRoomEvent(firstTile, firstTile.REvent.EventName);
            return firstTile;
        }

        private void SendTileToOtherPlayers(RoomTile tile)
        {
            return;
            if (PhotonNetwork.connected && PhotonNetwork.isMasterClient)
            {
                GetComponent<PhotonView>().RPC(
                    "CreateMPTile", PhotonTargets.OthersBuffered,
                    new object[] { tile.ID, tile.TileType.ToString(), tile.transform.position, tile.REvent.ToString(), tile.gameObject.name });
            }
        }

        public RoomTile GetNextTile()
        {
            if (dungeonDeck.Count > 0)
            {
                return GetTopTile();
            }
            else throw new Exception("There are no more rooms to spawn");
        }

        private RoomTile GetTopTile()
        {
            RoomTile nextTile = dungeonDeck[0];
            RemoveTileFromDeck(nextTile);
            //Debug.Log("DungeonTilesRemaining: " + dungeonDeck.Count);
            return nextTile;
        }

        private RoomTile GetNonDeadEndTile()
        {
            List<RoomTile> tempDeck = new List<RoomTile>(dungeonDeck);
            for (int i = 0; i < dungeonDeck.Count; i++)
            {
                if (dungeonDeck[i].GetType() == typeof(DeadEndTile))
                {
                    tempDeck.Remove(dungeonDeck[i]);
                }
            }
            int randTileIndex = UnityEngine.Random.Range(0, tempDeck.Count);
            RoomTile nextTile = tempDeck[randTileIndex];
            RemoveTileFromDeck(nextTile);
            return nextTile;
        }

        public static RoomTile GetTileByID(int id)
        {
            for (int i = 0; i < deckByIDs.Count; i++)
            {
                if (deckByIDs[i].ID == id)
                {
                    return deckByIDs[i];
                }
            }
            throw new Exception("No tile with Id: " + id + " exists.");
        }

        [PunRPC]
        private void CreateMPTile(int id, string tileType, Vector3 position, string eventName, string tileName)
        {
            RoomTile newTile = SpawnTile(tileType);
            newTile.SetID(id);
            AddTileToIDDictionary(newTile);
            newTile.transform.position = position;
            newTile.name = tileName;
            Debug.Log(string.Format("Create Tile - ID: {0}, Event: {1}"));
            RoomEventFactory.NewRoomEventByName(eventName);
            newTile.REvent.tile = newTile;
        }

        private void AddTileToIDDictionary(RoomTile newTile)
        {
            if (deckByIDs.Contains(newTile) == false)
                deckByIDs.Add(newTile);
        }
    }
}